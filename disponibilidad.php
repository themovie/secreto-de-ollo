<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
<script src="include/js/funciones.js"></script>
<title><?php print("hoteles casas rurales en navarra cerca de pamplona dormir alojamiento ".fLiteral(92,$lIdIdioma)); //Disponibilidad ?></title>
</head>

<body class="top" onload="igualaColumnas3();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<!--<div class="span-12 border" id="cuerpo">-->
				<div class="span-13" id="cuerpo">
					<!--<div class="span-12" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower(fLiteral(353,$lIdIdioma)))); //DISPONIBILIDAD ?>
					</div>
					<!--<div class="span-12 calendario">-->
					<div class="span-12 last calendario">
						<?php include("include/modulos/inc_calendario_disponibilidad.php"); ?>
					</div>
				</div>
			</div>
			<!--<div class="span-5 append-1 last" id="columna_dcha">-->
			<div class="span-5 last" id="columna_dcha">
<?php
		// Martin 17/04/08. Seleccionamos la informacion del entorno del establecimiento.
		//Las variables $lIdEstablecimiento y $lIdIdioma se establecen en el script inc_comun.php. 
		$lCadena = "SELECT terminos, cancelacion, ninios".
						", animales, otros".
						" FROM ".__TABLA_POLITICAS__.
						" WHERE idestablecimiento = ".$lIdEstablecimiento.
						" AND ididioma = ".$lIdIdioma;
		$rsPoliticas = fQuery($lCadena);
		if (mysql_num_rows($rsPoliticas) > 0)
		{
?>
				<h4> <?php print(ucfirst(mb_strtolower(fLiteral(379,$lIdIdioma)))); //CONDICIONES ?> </h4>
<?php
			$lPol_terminos = mysql_result($rsPoliticas,0,"terminos");
			$lPol_cancelacion = mysql_result($rsPoliticas,0,"cancelacion");
			$lPol_ninios = mysql_result($rsPoliticas,0,"ninios");
			$lPol_animales = mysql_result($rsPoliticas,0,"animales");
			$lPol_otros = mysql_result($rsPoliticas,0,"otros");
			if ($lPol_terminos != "")
			{
?>
				<h5 class="titulo_condiciones"> <?php print(fLiteral(380,$lIdIdioma)); //T�rminos ?> </h5>
				<p class="agrupacion_elementos">
					<?php print(str_replace("\n","<br />",$lPol_terminos)); ?>
				</p>
<?php
			}
			if ($lPol_cancelacion != "")
			{
?>
				<h5 class="titulo_condiciones"> <?php print(fLiteral(79,$lIdIdioma)); //Pol�tica de cancelaci�n ?> </h5>
				<p class="agrupacion_elementos">
					<?php print(str_replace("\n","<br />",$lPol_cancelacion)); ?>
				</p>
<?php
			}
			if ($lPol_ninios != "")
			{
?>
				<h5 class="titulo_condiciones"> <?php print(fLiteral(80,$lIdIdioma)); //Pol�tica de ni�os ?> </h5>
				<p class="agrupacion_elementos">
					<?php print(str_replace("\n","<br />",$lPol_ninios)); ?>
				</p>
<?php
			}
			if ($lPol_animales != "")
			{
?>
				<h5 class="titulo_condiciones"> <?php print(fLiteral(319,$lIdIdioma)); //Pol�tica de animales ?> </h5>
				<p class="agrupacion_elementos">
					<?php print(str_replace("\n","<br />",$lPol_animales)); ?>
				</p>
<?php
			}
			if ($lPol_otros != "")
			{
?>
				<h5 class="titulo_condiciones"> <?php print(fLiteral(236,$lIdIdioma)); //Otros ?> </h5>
				<p class="agrupacion_elementos">
					<?php print(str_replace("\n","<br />",$lPol_otros)); ?>
				</p>
<?php
			}
		}
?>
			</div>
			<?php include("include/modulos/inc_pie.php"); ?>
		</div>
	</div>
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
