<?php
	if (session_name() != "sesion-id")
	{
		session_name("sesion-id");
	}
	session_start();
	include("config.php");

	include("include/app/inc_funciones.php");
	include("include/app/inc_declaraciones.php");
	include("include/app/inc_conexion.php");
	include("include/app/inc_strings.php");
	include("include/app/inc_idioma.php");


	// Martin 21/05/09. Tratamiento para evitar que nos vengan de urls erroneas desde busquedas de google, por 
	//ejemplo: "http://www.elsecretodeollo.com/elentorno.php?id=50&i=1&idimg_ent=37?id=50&i=1&sesion-id=n0u4anbs9dk7crtt4dosm6a531"
	$lPagina_entrada = $_SERVER["REQUEST_URI"];
	$lNum_cadenas_empiezan_interrogacion = preg_match_all('/(\?.+)(\?.+)/',$lPagina_entrada,$aCoincidencias,PREG_PATTERN_ORDER);
//print("\$lNum_cadenas_empiezan_interrogacion=".$lNum_cadenas_empiezan_interrogacion."<br />");
//print_r($aCoincidencias);print("<br />");
	if ($lNum_cadenas_empiezan_interrogacion > 0) {
		// Martin 06/07/09. Puesto el header status: 304 y el "exit".
		header("Status: 304");
		header("Location: ".$aCoincidencias[1][0]);
		exit;
	}

	// Martin 20/04/09. El idestablecimiento de secreto del ollo sera el 50 en la aplicacion de 
	//reservadealojamientos.
	$_SESSION["idestablecimiento"] = 50;

	if ($_SESSION["idestablecimiento"] == "")
	{
		$_SESSION["idestablecimiento"] = $_GET["id"];
	}
	$lIdEstablecimiento = $_SESSION["idestablecimiento"];

//	if ($_SESSION["ididioma"] == "" || $_SESSION["ididioma"] != $_GET["i"])
//	{
//		$_SESSION["ididioma"] = $_GET["i"];
//	}
//	$lIdIdioma = (($_SESSION["ididioma"]=="") ? "1" :  $_SESSION["ididioma"]);

	if ($_GET["i"] != "")
	{
		$_SESSION["ididioma"] = $_GET["i"];
	}
	elseif ($_SESSION["ididioma"] == "")
	{
		$_SESSION["ididioma"] = 1;
	}
	// Martin 01/06/09. El idioma sera siempre el espanol.
	// 20202220 jit $_SESSION["ididioma"] = 1;
	$lIdIdioma = $_SESSION["ididioma"];

	// Martin 17/04/08. Inicializamos la variable de sesion idtipo a 1. Esta variable indica el 
	//tipo de establecimiento (1 - hotel; 2 - gestion apartamentos). Se usara en el calendario 
	//de disponibilidad (include/modulos/inc_calendario_disponibilidad.php).
	$_SESSION["idtipo"] = 1;

//	$lIdEstablecimiento = $_GET["id"];
//	$lIdIdioma = $_GET["i"];
//	if ($lIdIdioma == "")
//	{
//		$lIdIdioma = 1;
//	}

	$lPaginaActual = $_SERVER["REQUEST_URI"];
	$lPosicionParam = strpos($_SERVER["REQUEST_URI"],"?");
	$lDominioActual = $_SERVER["SERVER_NAME"];
	if ($lPosicionParam !== false)
	{
		$lPaginaActual = substr($lPaginaActual,1,($lPosicionParam-1));
	}
	else 
	{
		$lPaginaActual = substr($lPaginaActual,1);
	}
	$lParametros = "";
	$lParametros .= "?id=".$lIdEstablecimiento;
	if ($lIdIdioma != "")
	{
		$lParametros .= "&i=".$lIdIdioma;
	}
	// Martin 14/04/08, Usamos la variable $lParametrosSID para propagar la variable 
	//de sesion a traves de los URL para aquellos navegadores que no admitan cookies.
	// 20130425 MA. No usaremos mas $lParametrosSID
	//if (SID != "")
	//{	$lParametrosSID = "&".SID; }
	//else
	//{	$lParametrosSID = ""; }
	$lParametrosSID = '';

	// Martin 12/04/10. Si las variables de sesion del idestablecimiento o el ididioma estan vacias o 
	//tienen un valor no numerico entonces interrumpimos la ejecucion del script.
	if ($_SESSION["idestablecimiento"] == "" || is_numeric($_SESSION["idestablecimiento"]) == false) {
		exit("Error");
	}
	elseif ($_SESSION["ididioma"] == "" || is_numeric($_SESSION["ididioma"]) == false) {
		exit("Error");
	}
?>
