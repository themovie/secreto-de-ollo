<?php
	define("_WNG_EXISTE_POSTAL_","Ya existe una postal con ese mismo n�mero. Por favor especifique otro n�mero o bien modifique los datos de una postal existente");
	define("_WNG_FALTAN_CAMPOS_","Han quedado uno o varios campos requeridos sin rellenar. Por favor verifique:");
	define("_WNG_FORMATO_NO_VALIDO1_","El fichero especificado no es de un formato v�lido.");
	define("_WNG_FORMATO_NO_VALIDO2_","S�lamente se pueden incluir im�genes en formato JPEG (extensi�n <em>.jpg</em> o <em>.jpeg</em>) o GIF  (extensi�n <em>.gif</em>)");
	define("_WNG_IMG_GRANDE_","La imagen es demasiado grande");
	define("_WNG_INTRODUCIR_PASSWORD_","Debe introducir la misma contrase�a en las dos cajas de texto");
	define("_WNG_NO_EXISTE1_","Ocurri� un error al intentar cargar la informaci�n. Puede que el registro haya sido eliminado.");
	define("_WNG_NO_EXISTE2_","Pruebe a refrescar el listado para asegurarse de que la informaci�n existe.");
	define("_WNG_PASSWORD_DIFERENTE1_","Las contrase&ntilde;as no son iguales.");
	define("_WNG_PASSWORD_DIFERENTE2_","Por favor aseg�rese de haber introducido la misma contrase�a en ambas cajas de texto.");
	define("_WNG_SELECCIONAR_IMAGEN_","Debe seleccionar un fichero para continuar con el proceso\\n\\nSi no desea realizar ninguna modificaci�n pulse el bot�n \\'Cerrar Ventana\\'");
	define("_WNG_USUARIO_INCORRECTO1_","El usuario especificado no existe o la contrase�a es incorrecta.");
	define("_WNG_USUARIO_INCORRECTO2_","Recuerde que tanto el usuario como la contrase&ntilde;a son sensibles a las may�sculas.");
	define("_WNG_DUPLICAR_LIBRO_","Se crear� un nuevo registro duplicando la informaci�n del registro actual. Se generar� un nuevo ISBN");

	define("_QTN_BORRAR_FICHA_","Si contin�a se eliminar� la ficha permanentemente.\\n(Este proceso no tiene vuelta atr�s)");
	define("_QTN_ELIMINAR_IMAGEN_","Se eliminar� la imagen de la noticia. &iquest;Continuar?");
	define("_QTN_ELIMINAR_DOCUMENTO_","Se eliminar� el documento relacionado con la noticia. &iquest;Continuar?");
	define("_QTN_PERDER_CAMBIOS_","Se perder�n los cambios realizados. �Continuar?");
	define("_QTN_PERDER_DATOS_","Se perder�n los datos introducidos. �Continuar?");

	define("_STR_CAMBIO_PASS_OK1_","El proceso de cambio de contrase�a ha sido ejecutado correctamente.");
	define("_STR_CAMBIO_PASS_OK2_","Ahora se cerrar� la sesi�n y ser� dirigido a la hoja de inicio. Deber� utilizar los nuevos datos para acceder al Administrador.");
	define("_STR_CAMPOS_REQUERIDOS_","Indica los campos requeridos");
	define("_STR_CERRANDO_SESION1_","Cerrando la sesi�n actual.");
	define("_STR_CERRANDO_SESION2_","Espere por favor ...");
	define("_STR_DEMASIADA_ALTURA_","La imagen tiene un valor de altura mayor que el valor de anchura, por lo que se deformar�.");
	define("_STR_DOCUMENTO_MODIFICADO1_","Las modificaciones solicitadas se efectuaron satisfactoriamente.");
	define("_STR_DOCUMENTO_MODIFICADO2_","Puede cerrar esta ventana con tranquilidad.");
	define("_STR_IMAGEN_BORRADA1_","El proceso de borrado ha finalizado satisfactoriamente.");
	define("_STR_IMAGEN_BORRADA2_","Puede continuar modificando la imagen de la noticia o bien cerrar la ventana.");
	define("_STR_DOCUMENTO_BORRADO1_","El proceso de borrado ha finalizado satisfactoriamente.");
	define("_STR_DOCUMENTO_BORRADO2_","Puede continuar modificando el documento de la noticia o bien cerrar la ventana.");
	define("_STR_IMAGEN_GRANDE_","La imagen especificada es algo m�s grande de lo recomendado, a pesar de que sus proporciones parecen apropiadas.");
	define("_STR_IMAGEN_PEQUENIA_","La imagen especificada es algo m�s peque�a de lo recomendado, a pesar de que sus proporciones parecen apropiadas.");
	define("_STR_IMAGEN_DESPROPORCIONADA_","La imagen no tiene las proporciones Anchura/Altura correctamente ajustadas, por lo que se deformar�.");
	define("_STR_IMAGEN_PROCESADA_"," Se ha procesado el alta correctamente, pero ser�a conveniente que compruebe que el resultado es el deseado.");
	define("_STR_IMAGEN_ERROR_","Se ha detectado un error:");
	define("_STR_IMAGEN_MODIFICADA1_","Las modificaciones solicitadas se efectuaron satisfactoriamente.");
	define("_STR_IMAGEN_MODIFICADA2_","Puede cerrar esta ventana con tranquilidad.");
	define("_STR_NOTICIA_SIN_IMAGEN_","La noticia no tiene ninguna imagen relacionada. Puede asignar una utilizando el siguiente cuadro:");
	define("_STR_NOTICIA_SIN_DOCUMENTO_","La noticia no tiene ning�n documento relacionado. Puede asignar uno utilizando el siguiente cuadro:");
	define("_STR_PREVIEW_IMAGEN1_","Esta es la imagen actualmente asociada a la noticia tal y como se ver� en la Web.");
	define("_STR_PREVIEW_IMAGEN2_","Si desea modificar la imagen actual de la noticia, indique por favor el nuevo fichero en el siguiente cuadro:");
	define("_STR_SIN_DATOS1_","No se encontraron registros en la base de datos");
	define("_STR_SIN_DATOS2_","Si tiene aplicado un filtro, pruebe a eliminarlo para un listado completo.");
	define("_STR_SIN_ESPECIFICAR_","Sin Especificar");
	define("_STR_TIP_LISTADO_","Para una b�squeda m�s sencilla, pruebe a filtrar los datos utilizando el men� de opciones de filtro.");
	define("_STR_VERIFICAR_DATOS_","Verifique que los datos sean correctos. En caso de querer modificar algo, utilice el bot�n Anterior.");
	define("_STR_VERIFICAR_MODIFICACION_","Est� a punto de modificar un registro existente. Confirme que los datos son correctos y pulse el bot�n de 'Finalizar'");
	define("_STR_WIZ_IMAGEN_","Si desea a�adir una imagen a la ficha de esta noticia, indique la ruta en el siguiente cuadro, o bien pulse Examinar y seleccione el fichero:");

	define("_CHR_CAMPO_OCULTO_","&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;");
	define("_CHR_CAMPO_REQUERIDO_","&#8226;");
?>