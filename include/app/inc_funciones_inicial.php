<?php

function ObtenerLiteral($pId,$pIdioma,$pLiteral="",$pConexion="")
{	//Mart�n 27/04/07. A�adido el par�metro pConexion porque manejamos 2 BDs, la de FeelFreeRentals (ffr) 
	//y la de Reservas
	$lIdiomaSelect = (($pIdioma!="") ? $pIdioma : "es" );
	$lCadena = "SELECT lit_literal".
				" FROM ".__TABLA_LITERALES__.
				" WHERE lit_id = ".$pId.
				" AND lit_idioma LIKE ".fComillas($lIdiomaSelect);
	$rsLiteral = fQuery($lCadena,$pConexion);
	if (mysql_num_rows($rsLiteral) > 0) 
	{
		$lLiteral = mysql_result($rsLiteral,0,0);
//		$lLiteral = mb_convert_encoding(mysql_result($rsLiteral,0,0),"UTF-8");
	}
	else
	{
		$lLiteral = $pLiteral;
	}
	return($lLiteral);
}

function ObtenerString($pId,$pIdioma,$pString="",$pConexion="")
{	//Mart�n 27/04/07. A�adido el par�metro pConexion porque manejamos 2 BDs, la de FeelFreeRentals (ffr) 
	//y la de Reservas
	$lIdiomaSelect = (($pIdioma!="") ? $pIdioma : "es" );
	$lCadena = "SELECT str_string".
				" FROM ".__TABLA_STRINGS__.
				" WHERE str_id = ".$pId.
				" AND str_idioma LIKE ".fComillas($lIdiomaSelect);
	$rsString = fQuery($lCadena,$pConexion);
	if (mysql_num_rows($rsString) > 0) 
	{
		$lString = mysql_result($rsString,0,0);
//		$lString = mb_convert_encoding(mysql_result($rsString,0,0),"UTF-8");
	}
	else
	{
		$lString = $pString;
	}
	// Mart�n 26/04/07. Si la cadena a devolver no tiene etiquetas <br/> sustituimos los saltos de l�nea 
	//por etiquetas <br/>.
//	if (strpos($lString,"<br/>") === false)
//	{
//		$lString = str_replace(chr(10),"<br/>",$lString);
//	}

	// IR 2007/05/14 (sustituir los saltos de linea por <br> para HTML): 
//	return($lString);
	return str_replace("\n","<br />",$lString);
}




// Image Resize
// imagen origen, X miniatura, Y miniatura, fichero salida
function createthumb($IMAGE_SOURCE,$THUMB_X,$THUMB_Y,$OUTPUT_FILE)
{
	$BACKUP_FILE = $OUTPUT_FILE . "_backup.jpg";
	copy($IMAGE_SOURCE,$BACKUP_FILE);
	$IMAGE_PROPERTIES = getimagesize($BACKUP_FILE);
	if (!$IMAGE_PROPERTIES[2] == 2)
		return(0);
	else
	{
		$SRC_IMAGE = imagecreatefromjpeg($BACKUP_FILE);
		$SRC_X = imagesx($SRC_IMAGE);
		$SRC_Y = imagesy($SRC_IMAGE);
		if (($THUMB_Y == "0") && ($THUMB_X == "0"))
			return(0);
		elseif($THUMB_Y == "0")
		{
			$SCALEX = $THUMB_X/($SRC_X-1);
			$THUMB_Y = $SRC_Y*$SCALEX;
		}
		elseif($THUMB_X == "0")
		{
			$SCALEY = $THUMB_Y/($SRC_Y-1);
			$THUMB_X = $SRC_X*$SCALEY;
		}

		$THUMB_X = (int)($THUMB_X);
				$THUMB_Y = (int)($THUMB_Y);
		$DEST_IMAGE = imagecreatetruecolor($THUMB_X, $THUMB_Y);
		unlink($BACKUP_FILE);
		if (!imagecopyresized($DEST_IMAGE, $SRC_IMAGE, 0, 0, 0, 0, $THUMB_X, $THUMB_Y, $SRC_X, $SRC_Y))
		{
			imagedestroy($SRC_IMAGE);
			imagedestroy($DEST_IMAGE);
			return(0);
		}
		else
		{
			imagedestroy($SRC_IMAGE);
			if (imagejpeg($DEST_IMAGE,$OUTPUT_FILE))
			{
				imagedestroy($DEST_IMAGE);
				return(1);
			}
			imagedestroy($DEST_IMAGE);
		}
	return(0);
	}
}

//function fReferenciaFoto($pFoto)
//{
//	$lReferencia = "AGK".fPonerCeros($pFoto,5);
//	
//	return $lReferencia;
//}

function fProcesarTexto($pTexto)
{
	$lCadena=$pTexto;
//	$lCadena = str_replace("'","\'",$lCadena);
/*
	$lCadena=str_replace('&quot;',"`",$lCadena);
	$lCadena=str_replace('&ldquo;',"",$lCadena);
	$lCadena=str_replace('&rdquo;',"`",$lCadena);
	$lCadena=str_replace("'","\'",$lCadena);
	$lCadena=str_replace("\\","",$lCadena);
*/
	$lCadena=str_replace('"',"`",$lCadena);
	$lCadena=str_replace('',"",$lCadena);
	$lCadena=str_replace('',"`",$lCadena);
	$lCadena=str_replace("'","",$lCadena);
	$lCadena=str_replace("\\","",$lCadena);

	return $lCadena;
}

function fEnviarMail($pNombreRemitente,$pEMailRemitente,$pDestinatario,$pTitulo, $pTexto1, $pTexto2,$pAdjunto)
{
	$lContenido = '';

	$lContenido.= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
	$lContenido.= "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\">";
	$lContenido.= "<head>";
	$lContenido.= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />";
	$lContenido.= "<title>Correo electronico</title>";
	$lContenido.= "</head>";
	$lContenido.= "<body bgcolor=\"#ffffff\">";

	// Contenido 
	$lContenido.=$pTexto1;

	$lContenido.="</body>";
	$lContenido.="</html>";

	$lCuerpoMail = $lContenido;
	$lContenido = $lCuerpoMail;

	$lMensajeFinal = "";
	$lCabeceras = "";

	// Abrimos boundary
	$lBoundary = "----=_NextPart_".md5(uniqid(rand()));

	// Cabeceras comunes
	$lCabeceras.= "MIME-Version: 1.0\n";
	$lCabeceras.= "From: ".$pNombreRemitente." <".$pEMailRemitente.">\n";
	$lCabeceras.= "Reply-To: ".$pEMailRemitente."\n";
	$lCabeceras.="Return-Path: ".$pEMailRemitente."\n";
	$lCabeceras.= "Content-Type: multipart/alternative;\n\tboundary=".'"'.$lBoundary.'"'."\n";
	$lCabeceras.= "X-Mailer: PHP/".phpversion()."\n";
	// Fin cabecera

	// Boundary para text
	$lMensajeFinal.="\nThis is a multi-part message in MIME format.\n\n";
	$lMensajeFinal.= "--".$lBoundary."\n";
	$lMensajeFinal.= "Content-Type: text/plain;\ncharset=\"iso-8859-1\"\n";
	$lMensajeFinal.= "Content-Transfer-Encoding: quoted-printable"."\n";
	$lMensajeFinal.="Content-Encoding: 8BIT\n\n";
	$lMensajeFinal.=$pTexto2."\n\n";
	// Fin texto

	// Boundary para mensaje html
	$lMensajeFinal.= "--".$lBoundary."\n";
	$lMensajeFinal.= "Content-Type: text/html\n";
	$lMensajeFinal.="Content-disposition: inline\n";
	$lMensajeFinal.="Content-encoding: 8BIT\n\n";

	$lMensajeFinal.= $lContenido."\n\n";

	// Cerramos el boundary
	$lMensajeFinal.= "--".$lBoundary."--"."\n";

	$lTitulo = "=?ISO-8859-1?Q?".imap_8bit($pTitulo)."?=";
	mail($pDestinatario, $lTitulo, $lMensajeFinal,$lCabeceras) or die("Ocurri alg n error en el envio");;
}

function fUltimoID($pTabla, $pCampo, $pConexion="")
{	//Mart�n 27/04/07. A�adido el par�metro pConexion porque manejamos 2 BDs, la de FeelFreeRentals (ffr) 
	//y la de Reservas
	$lCadena = "SELECT MAX(".$pCampo.") AS ultimo FROM ".$pTabla;
	$rsSelect = fQuery($lCadena,$pConexion);
	if(mysql_result($rsSelect, 0,"ultimo")==NULL)
		$lUltimo = 0;
	else
		$lUltimo = mysql_result($rsSelect,0,"ultimo");
	return $lUltimo;
}

function fDiaSemana($pDia,$pIdioma)
{
	$laDias[0][0] = "Domingo";
	$laDias[0][1] = "Igandea";
	$laDias[0][2] = "Sunday";
	$laDias[1][0] = "Lunes";
	$laDias[1][1] = "Astelehena";
	$laDias[1][2] = "Monday";
	$laDias[2][0] = "Martes";
	$laDias[2][1] = "Asteartea";
	$laDias[2][2] = "Tuesday";
	$laDias[3][0] = "Mircoles";
	$laDias[3][1] = "Asteazkena";
	$laDias[3][2] = "Wednesday";
	$laDias[4][0] = "Jueves";
	$laDias[4][1] = "Osteguna";
	$laDias[4][2] = "Thursday";
	$laDias[5][0] = "Viernes";
	$laDias[5][1] = "Ostirala";
	$laDias[5][2] = "Friday";
	$laDias[6][0] = "S bado";
	$laDias[6][1] = "Larunbata";
	$laDias[6][2] = "Saturday";
	$laDias[7][0] = "Domingo";
	$laDias[7][1] = "Igandea";
	$laDias[7][2] = "Sunday";

	if($pIdioma=="es")
		$lIdioma = 0;
	if($pIdioma=="eu")
		$lIdioma = 1;
	if($pIdioma=="en")
		$lIdioma = 2;

	$lDia = $laDias[$pDia][$lIdioma];

	return $lDia;
}

//function fNombreMes($pMes, $pIdioma)
//{
//	// Aado 10 y quito 10 para transformar "08" en "8"
//	$pMes+=10;
//	$pMes-=10;
//	// Resto uno por indice en base 0
//	$pMes-=1;
//	$laMeses[0][0] = "enero";
//	$laMeses[0][1] = "urtarrila";
//	$laMeses[0][2] = "january";
//	$laMeses[1][0] = "febrero";
//	$laMeses[1][1] = "otsaila";
//	$laMeses[1][2] = "february";
//	$laMeses[2][0] = "marzo";
//	$laMeses[2][1] = "martxoa";
//	$laMeses[2][2] = "march";
//	$laMeses[3][0] = "abril";
//	$laMeses[3][1] = "apirila";
//	$laMeses[3][2] = "april";
//	$laMeses[4][0] = "mayo";
//	$laMeses[4][1] = "maiatza";
//	$laMeses[4][2] = "may";
//	$laMeses[5][0] = "junio";
//	$laMeses[5][1] = "ekaina";
//	$laMeses[5][2] = "june";
//	$laMeses[6][0] = "julio";
//	$laMeses[6][1] = "uztaila";
//	$laMeses[6][2] = "july";
//	$laMeses[7][0] = "agosto";
//	$laMeses[7][1] = "abuztua";
//	$laMeses[7][2] = "august";
//	$laMeses[8][0] = "septiembre";
//	$laMeses[8][1] = "iraila";
//	$laMeses[8][2] = "september";
//	$laMeses[9][0] = "octubre";
//	$laMeses[9][1] = "urria";
//	$laMeses[9][2] = "october";
//	$laMeses[10][0] = "noviembre";
//	$laMeses[10][1] = "azaroa";
//	$laMeses[10][2] = "november";
//	$laMeses[11][0] = "diciembre";
//	$laMeses[11][1] = "abendua";
//	$laMeses[11][2] = "december";
//
//	if($pIdioma=="es")
//		$lIdioma = 0;
//	if($pIdioma=="eu")
//		$lIdioma = 1;
//	if($pIdioma=="en")
//		$lIdioma = 2;
//
//	$lMes = $laMeses[$pMes][$lIdioma];
//	
//	return $lMes;
//}


function fNombreMes($pMes, $pIdioma)
{
	// Aado 10 y quito 10 para transformar "08" en "8"
	$pMes+=10;
	$pMes-=10;
	// Resto uno por indice en base 0
	$pMes-=1;
	$laMeses[0][0] = "enero";
	$laMeses[0][1] = "urtarrila";
	$laMeses[0][2] = "janvier";
	$laMeses[0][3] = "january";
	$laMeses[0][4] = "enero";
	$laMeses[0][5] = "Januar";
	$laMeses[0][6] = "Gennaio";

	$laMeses[1][0] = "febrero";
	$laMeses[1][1] = "otsaila";
	$laMeses[1][2] = "f�vrier";
	$laMeses[1][3] = "february";
	$laMeses[1][4] = "febrero";
	$laMeses[1][5] = "Februar";
	$laMeses[1][6] = "febbraio";

	$laMeses[2][0] = "marzo";
	$laMeses[2][1] = "martxoa";
	$laMeses[2][2] = "mars";
	$laMeses[2][3] = "march";
	$laMeses[2][4] = "marzo";
	$laMeses[2][5] = "M�rz";
	$laMeses[2][6] = "marzo";

	$laMeses[3][0] = "abril";
	$laMeses[3][1] = "apirila";
	$laMeses[3][2] = "avril";
	$laMeses[3][3] = "april";
	$laMeses[3][4] = "abril";
	$laMeses[3][5] = "April";
	$laMeses[3][6] = "aprile";

	$laMeses[4][0] = "mayo";
	$laMeses[4][1] = "maiatza";
	$laMeses[4][2] = "mai";
	$laMeses[4][3] = "may";
	$laMeses[4][4] = "mayo";
	$laMeses[4][5] = "Mai";
	$laMeses[4][6] = "maggio";

	$laMeses[5][0] = "junio";
	$laMeses[5][1] = "ekaina";
	$laMeses[5][2] = "juin";
	$laMeses[5][3] = "june";
	$laMeses[5][4] = "junio";
	$laMeses[5][5] = "Juni";
	$laMeses[5][6] = "giugno";

	$laMeses[6][0] = "julio";
	$laMeses[6][1] = "uztaila";
	$laMeses[6][2] = "juillet";
	$laMeses[6][3] = "july";
	$laMeses[6][4] = "julio";
	$laMeses[6][5] = "Juli";
	$laMeses[6][6] = "luglio";

	$laMeses[7][0] = "agosto";
	$laMeses[7][1] = "abuztua";
	$laMeses[7][2] = "ao�t";
	$laMeses[7][3] = "august";
	$laMeses[7][4] = "agosto";
	$laMeses[7][5] = "August";
	$laMeses[7][6] = "Agosto";

	$laMeses[8][0] = "septiembre";
	$laMeses[8][1] = "iraila";
	$laMeses[8][2] = "septembre";
	$laMeses[8][3] = "september";
	$laMeses[8][4] = "septiembre";
	$laMeses[8][5] = "September";
	$laMeses[8][6] = "settembre";

	$laMeses[9][0] = "octubre";
	$laMeses[9][1] = "urria";
	$laMeses[9][2] = "octobre";
	$laMeses[9][3] = "october";
	$laMeses[9][4] = "octubre";
	$laMeses[9][5] = "Oktober";
	$laMeses[9][6] = "ottobre";

	$laMeses[10][0] = "noviembre";
	$laMeses[10][1] = "azaroa";
	$laMeses[10][2] = "novembre";
	$laMeses[10][3] = "november";
	$laMeses[10][4] = "noviembre";
	$laMeses[10][5] = "November";
	$laMeses[10][6] = "novembre";

	$laMeses[11][0] = "diciembre";
	$laMeses[11][1] = "abendua";
	$laMeses[11][2] = "d�cembre";
	$laMeses[11][3] = "december";
	$laMeses[11][4] = "diciembre";
	$laMeses[11][5] = "Dezember";
	$laMeses[11][6] = "dicembre";

	//Martin 21/03/07, Adaptado a la nueva gestin de idiomas.
	//if($pIdioma==1) //"c"
	//	$lIdioma = 0;
	//if($pIdioma==2) //"e"
	//	$lIdioma = 1;
	//if($pIdioma==4) //"i"
	//	$lIdioma = 2;

	$lIdioma = $pIdioma - 1; 

	$lMes = $laMeses[$pMes][$lIdioma];
	
	return $lMes;
}


function fMesAnioSiguiente($pMes,$pAnio)
{
	$laMesAnioSig = array();
	if ($pMes == 12)
	{
		$laMesAnioSig["mes"] = 1;
		$laMesAnioSig["anio"] = $pAnio + 1;
	}
	else
	{
		$laMesAnioSig["mes"] = $pMes + 1;
		$laMesAnioSig["anio"] = $pAnio;
	}
	return($laMesAnioSig);
}

function fMesAnioAnterior($pMes,$pAnio)
{
	$laMesAnioAnt = array();
	if ($pMes == 1)
	{
		$laMesAnioAnt["mes"] = 12;
		$laMesAnioAnt["anio"] = $pAnio - 1;
	}
	else
	{
		$laMesAnioAnt["mes"] = $pMes - 1;
		$laMesAnioAnt["anio"] = $pAnio;
	}
	return($laMesAnioAnt);
}

function fPonerCeros($pNumero,$pDigitos)
{
	if($pDigitos<strlen($pNumero))
		return $pNumero;
	$lCadena = $pNumero;
	for($li=0;$li<$pDigitos-strlen($pNumero);$li++)
		$lCadena="0".$lCadena;
	
	return $lCadena;
}

function sBorrarFichero($pFichero)
{
	if(file_exists($pFichero))
		return unlink($pFichero);
	else
	{
//		print($pFichero.": Fichero no encontrado ...");
		return false;
	}
}

function fComillas($pTexto)
{
	return "'".$pTexto."'";
}

function fReportarError($pCadena)
{
	print("<br>Ocurri  un error al procesar el siguiente comando: ".$pCadena."<br><br>Es posible que la hoja no se muestre correctamente");

	$lTexto = "";
	$lTexto.="Error en script: ".$_SERVER["REQUEST_URI"]."\n";
	$lTexto.="Referer: ".$_SERVER["HTTP_REFERER"]."\n";
	$lTexto.="IP: ".$_SERVER["REMOTE_ADDR"]."\n";
	$lTexto.="Navegador: ".$_SERVER["HTTP_USER_AGENT"]."\n";
	$lTexto.="Fecha de la incidencia: ".date("d/m/Y H:i:s",time())."\n";
	$lTexto.="Cadena SQL: ".$pCadena."\n";
	$lTitulo = "Error Script";
	
	//mail("irodriguez@themovie.org",$lTitulo,$lTexto);
	//mail("a.martin@themovie.org",$lTitulo,$lTexto);
}

function fQuery($pCadena,$pConexion="")
{	//Mart�n 27/04/07. A�adido el par�metro pConexion porque manejamos 2 BDs, la de FeelFreeRentals (ffr) 
	//y la de Reservas
	if ($pConexion == "")
	{
		$rsObjeto = mysql_query($pCadena) or fReportarError($pCadena);
	}
	else
	{
		$rsObjeto = mysql_query($pCadena,$pConexion) or fReportarError($pCadena);
	}
	return $rsObjeto;
}

function fComillasLike($pTexto)
{
	return fComillas("%".$pTexto."%");
}

//function fCargarContenido($pContenido,$pIdioma)
//{
//	$lCadena = "SELECT * FROM ".__TABLA_CONTENIDOS__." WHERE idcontenido = ".$pContenido;
//	$rsContenidos = fQuery($lCadena);
//	if(mysql_num_rows($rsContenidos)>0)
//		print(mysql_result($rsContenidos,0,"contenido".$pIdioma));
//	else
//		print("No se pudo cargar el contenido de esta pgina");
//}

//function fLiteral($pLiteral,$pIdioma)
//{
//	$lIdioma = $pIdioma;
//	if($pIdioma=="")
//		$pIdioma = "c";
//	$lCadena = "SELECT literal".$pIdioma." FROM ".__TABLA_LITERALES__." WHERE idliteral = ".$pLiteral;
//	$rsLiterales = fQuery($lCadena);
//	if(mysql_num_rows($rsLiterales)>0)
//		$lLiteral = mysql_result($rsLiterales,0,"literal".$pIdioma);
//	else
//		$lLiteral = "N/D";
//	return $lLiteral;
//}

//function fString($pString,$pIdioma)
//{
//	$lIdioma = $pIdioma;
//	if($pIdioma=="")
//		$pIdioma = "c";
//	$lCadena = "SELECT string".$pIdioma." FROM ".__TABLA_STRINGS__." WHERE idstring = ".$pString;
//	$rsString = fQuery($lCadena);
//	if(mysql_num_rows($rsString)>0)
//		$lString = mysql_result($rsString,0,"string".$pIdioma);
//	else
//		$lString = "N/D";
//	return $lString;
//}

function fDiasMes($pMes,$pAnio)
{
	$lMes = $pMes;
	$lMes+=10;
	$lMes-=10;

	switch ($lMes)
	{
		case 1:
			$lDiasMes = 31;
			break;
		case 2:
			if(gmp_div_r($pAnio,4)==0)
				$lDiasMes = 29;
			else
				$lDiasMes = 28;
			break;
		case 3:
			$lDiasMes = 31;
			break;
		case 4:
			$lDiasMes = 30;
			break;
		case 5:
			$lDiasMes = 31;
			break;
		case 6:
			$lDiasMes = 30;
			break;
		case 7:
			$lDiasMes = 31;
			break;
		case 8:
			$lDiasMes = 31;
			break;
		case 9:
			$lDiasMes = 30;
			break;
		case 10:
			$lDiasMes = 31;
			break;
		case 11:
			$lDiasMes = 30;
			break;
		case 12:
			$lDiasMes = 31;
			break;
	}

	return $lDiasMes;
}

function fSumarUnDia($pDiaenSeg)
{
	//	Mart n 22/03/07. Dada una fecha en formato UNIX calculamos la fecha del da siguiente 
	//en formato UNIX tambi n (a las 12:00:00). 
	//	Sumar directamente los 86400 segundos que tiene un da a la fecha deber a bastar normalmente, 
	//pero cuando entre dos das hay un cambio de hora PHP lo tiene en cuenta y la diferencia 
	//en segundos es (86400+3600) o (86400-3600)

	//Obtenemos una representaci n legible de la fecha
	$laDia = getdate($pDiaenSeg);
	$lDia = $laDia["mday"];
	$lMes = $laDia["mon"];
	$lAnio = $laDia["year"];

	$lHora = $laDia["hours"];
	$lMinuto = $laDia["minutes"];
	$lSegundo = $laDia["seconds"];

	//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
	$lDia += 1;
	if (!checkdate($lMes,$lDia,$lAnio))
	{
		//Si la fecha es incorrecta es que estabamos en el  ltimo da del mes.
		$lDia = 1;
		$lMes += 1;
		if (!checkdate($lMes,$lDia,$lAnio))
		{
			//Si la fecha es incorrecta es que estabamos en el  ltimo mes del ao.
			$lMes = 1;
			$lAnio += 1;
		}
	}
	//Calculamos la marca de tiempo.
	$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
	//$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio.
	//					" ".$lHora.":".$lMinuto.":".$lSegundo.":");
	return ($lDiaSig);
}

function fRestarUnDia($pDiaenSeg)
{
	//	Mart n 22/03/07. Dada una fecha en formato UNIX calculamos la fecha del da anterior 
	//en formato UNIX tambi n (a las 12:00:00). 
	//	Restar directamente los 86400 segundos que tiene un da a la fecha deber a bastar normalmente, 
	//pero cuando entre dos das hay un cambio de hora PHP lo tiene en cuenta y la diferencia 
	//en segundos es (86400+3600) o (86400-3600)

	//Obtenemos una representaci n legible de la fecha
	$laDia = getdate($pDiaenSeg);
	$lDia = $laDia["mday"];
	$lMes = $laDia["mon"];
	$lAnio = $laDia["year"];

	$lHora = $laDia["hours"];
	$lMinuto = $laDia["minutes"];
	$lSegundo = $laDia["seconds"];

	//Calculamos la marca de tiempo de la fecha correspondiente al da anterior.
	$lDia -= 1;
	if (!checkdate($lMes,$lDia,$lAnio))
	{
		//Si la fecha es incorrecta es que estabamos en el primer d a del mes.
		//Probaremos en un bucle con los posibles ltimos d as del mes hasta hallar 
		//la fecha correcta.
		//$lDia = 31;
		$lMes -= 1;
		if ($lMes == 0)
		{
			//Si mes es 0 es que la fecha era 1 de Enero y la fecha anterior entonces 
			//es 31 de Diciembre.
			$lMes = 12;
			$lDia = 31;
			$lAnio--;
		}
		else
		{
			//Vamos probando con los posibles ltimos d as del mes.
			for ($lDia=31;(!checkdate($lMes,$lDia,$lAnio) || $lDia<28);$lDia--);
		}
	}
	//Calculamos la marca de tiempo.
	$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
	//$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio.
	//					" ".$lHora.":".$lMinuto.":".$lSegundo.":");

	return ($lDiaSig);
}

function fSumarMeses($pMesenSeg,$lNumMeses)
{
	//	Martin 22/03/07. Dada una fecha en formato UNIX calculamos la fecha "lNumMeses" 
	//mesee despues en formato UNIX tambien ( del dia 1 a las 12:00:00). 

	//Obtenemos una representaci n legible de la fecha
	$laMes = getdate($pMesenSeg);
	$lDia = $laMes["mday"];
	$lMes = $laMes["mon"];
	$lAnio = $laMes["year"];

	$lHora = $laMes["hours"];
	$lMinuto = $laMes["minutes"];
	$lSegundo = $laMes["seconds"];

	//Calculamos la marca de tiempo.
	$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");

	if ($lNumMeses <= 0)
	{
		$lNumMeses = 0;
	}
	else
	{
		for ($li=0;$li<$lNumMeses;$li++)
		{
			//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
			$lMes += 1;
			if (!checkdate($lMes,$lDia,$lAnio))
			{
				//Si la fecha es incorrecta es que estabamos en el ultimo mes del anio.
				$lMes = 1;
				$lAnio += 1;
			}
			//Calculamos la marca de tiempo.
			$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
		}
	}
	return ($lMesSig);
}

function fRestarMeses($pMesenSeg,$lNumMeses)
{
	//	Martin 22/03/07. Dada una fecha en formato UNIX calculamos la fecha "lNumMeses" 
	//mesee despues en formato UNIX tambien ( del dia 1 a las 12:00:00). 

	//Obtenemos una representaci n legible de la fecha
	$laMes = getdate($pMesenSeg);
	$lDia = $laMes["mday"];
	$lMes = $laMes["mon"];
	$lAnio = $laMes["year"];

	$lHora = $laMes["hours"];
	$lMinuto = $laMes["minutes"];
	$lSegundo = $laMes["seconds"];

	//Calculamos la marca de tiempo.
	$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");

	if ($lNumMeses <= 0)
	{
		$lNumMeses = 0;
	}
	else
	{
		for ($li=0;$li<$lNumMeses;$li++)
		{
			//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
			$lMes -= 1;
			if (!checkdate($lMes,$lDia,$lAnio))
			{
				//Si la fecha es incorrecta es que estabamos en el primer mes del anio.
				$lMes = 12;
				$lAnio -= 1;
			}
			//Calculamos la marca de tiempo.
			$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
		}
	}
	return ($lMesSig);
}

function fSumarUnMes($pMesenSeg)
{
	//	Martin 22/03/07. Dada una fecha en formato UNIX calculamos la fecha del mes siguiente 
	//en formato UNIX tambien ( del dia 1 a las 12:00:00). 

	//Obtenemos una representaci n legible de la fecha
	$laMes = getdate($pMesenSeg);
	$lDia = $laMes["mday"];
	$lMes = $laMes["mon"];
	$lAnio = $laMes["year"];

	$lHora = $laMes["hours"];
	$lMinuto = $laMes["minutes"];
	$lSegundo = $laMes["seconds"];

	//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
	$lMes += 1;
	if (!checkdate($lMes,$lDia,$lAnio))
	{
		//Si la fecha es incorrecta es que estabamos en el ultimo mes del anio.
		$lMes = 1;
		$lAnio += 1;
	}
	//Calculamos la marca de tiempo.
	$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
	//$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio.
	//					" ".$lHora.":".$lMinuto.":".$lSegundo.":");
	return ($lMesSig);
}

function fSesionIniciada()
{
//	if($_SESSION["admin"]&&$_SESSION["userid"]!=0)
	if($_SESSION["iniciada"]=="si")
		return true;
	else
		return false;
}

function fObtenerCampo($pTabla,$pCampo,$pID,$pValor)
{
	if($pTabla!=""&&$pCampo!=""&&pID!=""&&$pValor!="")
	{
		$lCadena = "SELECT ".$pCampo." FROM ".$pTabla." WHERE ".$pID." = ".$pValor;
		$rsCampo = fQuery($lCadena);
		if(mysql_num_rows($rsCampo)>0)
			$lCampo = mysql_result($rsCampo,0,$pCampo);
		else
			$lCampo = "N/D";
	}
	else
		$lCampo = "N/D";

	return $lCampo;
}
function fObtenerCampoTexto($pTabla,$pCampo,$pID,$pValor)
{
	if($pTabla!=""&&$pCampo!=""&&pID!=""&&$pValor!="")
	{
		$lCadena = "SELECT ".$pCampo." FROM ".$pTabla." WHERE ".$pID." LIKE ".fComillas($pValor);
		$rsCampo = fQuery($lCadena);
		if(mysql_num_rows($rsCampo)>0)
			$lCampo = mysql_result($rsCampo,0,$pCampo);
		else
			$lCampo = "N/D";
	}
	else
		$lCampo = "N/D";

	return $lCampo;
}
function fObtenerCampoDoble($pTabla,$pCampo,$pID1, $pID2,$pValor1,$pValor2)
{
	if($pTabla!=""&&$pCampo!=""&&pID1!=""&&$pID2!=""&&$pValor1!=""&&$pValor2!="")
	{
		$lCadena = "SELECT ".$pCampo." FROM ".$pTabla." WHERE ".$pID1." = ".$pValor1." AND ".$pID2." = ".$pValor2;
		$rsCampo = fQuery($lCadena);
		if(mysql_num_rows($rsCampo)>0)
			$lCampo = mysql_result($rsCampo,0,$pCampo);
		else
			$lCampo = "N/D";
	}
	else
		$lCampo = "N/D";

	return $lCampo;
}
function fGenerarRegistro($pUsuario, $pAccion, $pTabla, $pID)
{
	$lUltimo = fUltimoID(__TABLA_REGISTRO__,"idregistro");
	$lUltimo++;

	$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
	$lIP2 = $_SERVER["REMOTE_ADDR"];
	$lNavegador = $_SERVER["HTTP_USER_AGENT"];
	$lFecha = time();

	$lCadena = "INSERT INTO ".__TABLA_REGISTRO__." VALUES(".
			$lUltimo.",".
			$lFecha.",".
			fComillas($lIP1).",".
			fComillas($lIP2).",".
			fComillas($lNavegador).",".
			$pUsuario.",".
			fComillas($pAccion).",".
			fComillas($pTabla).",".
			$pID.
			")";
	fQuery($lCadena);
}

function fEstablecimientoDisponible($pEstablecimiento, $pHabitaciones, $pTipo, $pPersonas, 
			$pFechaLlegada, $pFechaSalida, $pConexionBDReservas)
{
	$lConexionBDReservas = $pConexionBDReservas;
	// Guardo la informacin en la tabla de comprobaciones
	// Para tener constancia de cada intento, sea fallido o no
	$lUltimo = fUltimoID(__TABLA_COMPROBACIONES__,"idcomprobacion",$lConexionBDReservas);
	$lUltimo++;

	$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
	$lIP2 = $_SERVER["REMOTE_ADDR"];
	$lNavegador = $_SERVER["HTTP_USER_AGENT"];

	$lCadena = "INSERT INTO ".__TABLA_COMPROBACIONES__." VALUES(".
				$lUltimo.",".
				time().",".
				fComillas($lIP1).",".
				fComillas($lIP2).",".
				fComillas($lNavegador).",".
				$pEstablecimiento.",".
				$pTipo.",".
				$pHabitaciones.",".
				$pPersonas.",".
				$pFechaLlegada.",".
				$pFechaSalida.
				")";
	fQuery($lCadena,$lConexionBDReservas);
	
	
//echo("En fEstablecimientoDisponible. Mensaje1. <br />");
	//Comprobamos que haya disponibilidad en $pEstablecimiento con los par metros indicados. 
	$lIDEstablecimiento = $pEstablecimiento;
	$lIDTipo = $pTipo;
	$lHabitaciones = $pHabitaciones;
	$lPersonas = $pPersonas;

	$lFechaDesde = $pFechaLlegada;
	$lFechaHasta = $pFechaSalida;
	$lError = 0;

	// Busco si la fecha llegada/salida choca con alguna fecha de cierre.
	//Martn 21/03/07. Se usa la nueva tabla __TABLA_HABITACIONES_CIERRE__ (rsv_habitaciones_cierre).
//	$lCadena = "SELECT * ".
//			" FROM ".__TABLA_PYD_EXCEPCIONES__.
//			" WHERE idestablecimiento = ".$lIDEstablecimiento.
//			" AND (idtipo = ".$lIDTipo." OR idtipo = -1)".
//			" AND (fecha >= ".$lFechaDesde." AND fecha < ".$lFechaHasta.")".
//			" AND estado = 1 ";
	$lCadena = "SELECT * ".
			" FROM ".__TABLA_HABITACIONES_CIERRE__.
			" WHERE (idtipo = ".$lIDTipo." OR idtipo = -1)".
			" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")";
	$rsExcepciones = fQuery($lCadena,$lConexionBDReservas);
	if (mysql_num_rows($rsExcepciones)>0) {
		$lError = 5; // El establecimiento cierra la habitaci n en alguno de los dias indicados 
	}

	if ($lError == 0)
	{
		// Martin 02/07/07. Tenemos en cuenta si para el periodo seleccionado hay alguna fecha bloqueada.
		// Martin 08/11/07. Los bloqueos se tienen que tratar como si fuesen fechas de reserva, pues los propietarios de 
		//algunos apartamentos hacen reservas por su cuenta y en esos casos lo que hacen es bloquear las noches de la 
		//reserva, la fecha de salida no la bloquean. 
		//  Si una fecha bloqueada coincide con la fecha de salida ese d�a se considera reservable. Si la fecha de salida 
		//de una reserva coincide con el primer d�a de una serie de d�as bloqueados la reserva ser� realizable.
		$lCadena = "SELECT *".
				" FROM ".__TABLA_BLOQUEOS__.
				" WHERE idtipo = ".$lIDTipo.
				//" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")".
				" AND (fecha >= ".$lFechaDesde." AND fecha < ".$lFechaHasta.")".
				" AND bloqueado = 1";
		$rsBloqueos = fQuery($lCadena,$lConexionBDReservas);
		if (mysql_num_rows($rsBloqueos) > 0) 
		{
			$lError = 5;
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje2. <br />");
	if ($lError==0) { 
		// Compruebo precio por noche, estancia mnima y m xima, habitaciones disponibles y plazas
		$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__." AS hab_est".
					", ".__TABLA_HABITACIONES_TIPOS__." AS hab_tipo".
					" WHERE hab_est.idtipo = hab_tipo.idtipo".
					" AND hab_tipo.idestablecimiento = ".$lIDEstablecimiento.
					" AND hab_tipo.visible = 1".
					" AND hab_tipo.idtipo = ".$lIDTipo;
		$rsEstancia = fQuery($lCadena,$lConexionBDReservas);
		$lNoches = round(($lFechaHasta - $lFechaDesde) / 86400, 0);
		//if (mysql_num_rows($rsEstancia)==1) { 
		// Martin 17/05/07 Miramos que se haya devuelto m�s de 1 l�nea, y no s�lo 1 como hasta ahora, porque al no tener 
		//en cuenta el idioma se pueden estar devolviendo m�s de una linea con los mismos datos.
		if (mysql_num_rows($rsEstancia)>0) { 
			$lPrecioNoche = mysql_result($rsEstancia,0,"precio");
			$lEstanciaMax = mysql_result($rsEstancia,0,"estancia_max");
			$lEstanciaMin = mysql_result($rsEstancia,0,"estancia_min");
			$lHabitacionesDisponibles = mysql_result($rsEstancia,0,"habitaciones");
			//$lPlazas = mysql_result($rsEstancia,0,"plazas");
			$lPlazas_min = mysql_result($rsEstancia,0,"plazas_min");
			$lPlazas_max = mysql_result($rsEstancia,0,"plazas_max");
		} 
		else { 
			$lPrecioNoche = "N/D";
			$lEstanciaMax = 60;
			$lEstanciaMin = 1;
		}

		if ($lNoches<$lEstanciaMin) { 
			$lError = 15;
		} 
		elseif ($lNoches>$lEstanciaMax) { 
			$lError = 16;
		} 
		else {
//			$lTotal = round($lPrecioNoche * $lNoches, 2) * $lHabitaciones;
			$lPersonas = intval($lPersonas);
			//$lPlazas = intval($lPlazas);
			$lPlazas_min = intval($lPlazas_min);
			$lPlazas_max = intval($lPlazas_max);
			//Martn 13/04/07. Se tienen en cuenta el nuevo campo plazas_max para calcular cuantas 
			//habitaciones son necesarias para alojar a las personas de la reserva.
			if ($lPlazas_max != 0)
			{
				$lNecesarias = $lPersonas / $lPlazas_max;
			}
			else
			{
				$lNecesarias = 999;
			}
			//if ($lPlazas!=0) {
			//	$lNecesarias = $lPersonas / $lPlazas;
			//}
			//else { 
			//	$lNecesarias = 999;
			//}
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje3. <br />");
	if ($lError==0) {
		// Compruebo restricciones de d as min / max, si hay habitaciones suficientes y si hay un precio especial 
		//para algunos de los das de la reserva.
		// Mart n 21/03/07. 
		$lCadena = "SELECT * ".
				" FROM ".__TABLA_TEMPORADAS_FECHAS__." AS fec, ".__TABLA_HABITACIONES_TEMPORADAS__." AS hab".
				" WHERE fec.idestablecimiento = hab.idestablecimiento".
				" AND fec.idtemporada = hab.idtemporada".
				" AND fec.idestablecimiento = ".$lIDEstablecimiento.
				" AND (hab.idtipo = ".$lIDTipo." OR hab.idtipo = -1)".
				" AND (fec.fecha >= ".$lFechaDesde." AND fec.fecha <= ".$lFechaHasta.")".
				" AND hab.borrado = 0". //Martin 23/07/07
				" ORDER BY fec.fecha, hab.idtipo DESC ";
//		$lCadena = "SELECT * ".
//				" FROM ".__TABLA_PYD_EXCEPCIONES__.
//				" WHERE idestablecimiento = ".$lIDEstablecimiento.
//				" AND (idtipo = ".$lIDTipo." OR idtipo = -1)".
//				" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")".
//				" AND estado = 0".
//				" ORDER BY fecha, idtipo DESC ";
		$rsExcepciones = fQuery($lCadena,$lConexionBDReservas);
		if(mysql_num_rows($rsExcepciones)>0) { 
			//Las variables siguientes se usan para tener en cuenta o no las lneas de tipo general (-1) 
			//en funci n de si hay una lnea de tipo espec fico que ya trate el caso.
			$lCompr_est_min_max_general = true;
			$lCompr_habit_general = true;
			$lCompr_precio_general = true;
			for($li=0;$li<mysql_num_rows($rsExcepciones);$li++) { 
				$lIdtipo_actual = mysql_result($rsExcepciones,$li,"idtipo");
				//$lFecha_actual = mysql_result($rsExcepciones,$li,"fecha");
				if ( (mysql_result($rsExcepciones,$li,"estancia_min")!="") &&
					 (mysql_result($rsExcepciones,$li,"estancia_max")!="") &&
					 (mysql_result($rsExcepciones,$li,"fecha")==$lFechaDesde) &&
					 (($lIdtipo_actual>0) || $lCompr_est_min_max_general) ) { 
//echo("En fEstablecimientoDisponible. Mensaje3-1. lIdtipo_actual=".$lIdtipo_actual.". lFechaDesde=".$lFechaDesde." <br />");
					// Compruebo restricciones de das min / max
					if ($lNoches<mysql_result($rsExcepciones,$li,"estancia_min")) { 
						$lError = 15;
						break;
					}
					if ($lNoches>mysql_result($rsExcepciones,$li,"estancia_max")) { 
						$lError = 16;
						break;
					}
					if ($lIdtipo_actual>0) { 
						$lCompr_est_min_max_general = false;
					} 
					else { 
						$lCompr_est_min_max_general = true;
					}
				} 
				else { 
					$lCompr_est_min_max_general = true;
				}
				if ( mysql_result($rsExcepciones,$li,"habitaciones")!="" &&
					 (($lIdtipo_actual>0) || $lCompr_habit_general) ) { 
					// Busco si en las fechas indicadas hay disponibilidad
					if ( mysql_result($rsExcepciones,$li,"habitaciones")<$lNecesarias) { 
						$lError = 3; // No hay suficientes habitaciones diponibles en uno de los dias
						break;
					}
					if ( mysql_result($rsExcepciones,$li,"habitaciones")<$lHabitacionesDisponibles) { 
						$lHabitacionesDisponibles = mysql_result($rsExcepciones,$li,"habitaciones");
					}
					if ($lIdtipo_actual>0) { 
						$lCompr_habit_general = false;
					} 
					else { 
						$lCompr_habit_general = true;
					}
				} 
				else { 
					$lCompr_habit_general = true;
				}
			}
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje4. <br />");
	if ($lError==0) { 
//		// Busco reservas en esas mismas fechas para ese establecimiento/habitacion
//		$lCadena = "SELECT * FROM ".__TABLA_RESERVAS__.
//			" WHERE estado = 0 AND idestablecimiento = ".$lIDEstablecimiento.
//			" AND idtipo = ".$lIDTipo.
//			" AND (fechallegada < ".$pFechaSalida." AND fechasalida > ".$pFechaLlegada.")".
//			" AND borrado = 0".
//			" AND estado = 0";
//		$rsReservas = fQuery($lCadena,$lConexionBDReservas);
//		$lReservadas = 0;
////echo("En fEstablecimientoDisponible. Mensaje4-1. SELECT reservas=".$lCadena." <br />");
//		if(mysql_num_rows($rsReservas)>0)
//		{
//			for($lm=0;$lm<mysql_num_rows($rsReservas);$lm++)
//				$lReservadas+=mysql_result($rsReservas,$lm,"habitaciones");
//			$lHabitacionesDisponibles-=$lReservadas;
//		}

		//Martin 02/07/07. Se mira en las reservas solapadas (teniendo en cuenta las reservas desbloqueadas) 
		//para ver si hay disponibilidad.
		$lMinHabDisponibles = $lHabitacionesDisponibles;
		for ($lj=$pFechaLlegada;$lj<=$pFechaSalida;$lj=fSumarUnDia($lj))
		{
			$lTotalResDia = 0;
			$lTotalResDiaDesbloqueadas = 0;
			if ($lj==$pFechaLlegada) 
			{
				$lWhereFechas = " AND (fechallegada <= ".$lj." AND fechasalida > ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada <= ".$lj." AND res.fechasalida > ".$lj.")".
								//" AND ( blo.fecha = ".$lj."	OR blo.fecha = ".fSumarUnDia($lj)." )"; Comentado el 01/10/2007
								" AND ( blo.fecha = ".$lj." )";
			}
			elseif ($lj==$pFechaSalida) 
			{
				$lWhereFechas = " AND (fechallegada < ".$lj." AND fechasalida >= ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada < ".$lj." AND res.fechasalida >= ".$lj.")".
								" AND ( blo.fecha = ".$lj."	OR blo.fecha = ".fRestarUnDia($lj)." )";
			}
			else 
			{
				$lWhereFechas = " AND (fechallegada <= ".$lj." AND fechasalida >= ".$lj.")";
				// Comentado el 01/10/2007
				//$lWhereFechas2 = " AND (res.fechallegada <= ".$lj." AND res.fechasalida >= ".$lj.")".
				//				" AND blo.fecha = ".$lj;
				$lWhereFechas2 = " AND ( ( (res.fechallegada <= ".$lj." AND res.fechasalida > ".$lj.")".
								"        AND blo.fecha = ".$lj.")".
								"      OR ( (res.fechallegada <= ".$lj." AND res.fechasalida = ".$lj.")".
								"           AND blo.fecha = ".fRestarUnDia($lj).") )";
			}
			$lCadena = "SELECT SUM(habitaciones) hab_total_reservadas".
				" FROM ".__TABLA_RESERVAS__.
				" WHERE idestablecimiento = ".$lIDEstablecimiento.
				" AND idtipo = ".$lIDTipo.
				$lWhereFechas.
				" AND borrado = 0".
				" AND estado = 0";
			$rsTotalHabRes = fQuery($lCadena,$lConexionBDReservas);
			if (mysql_num_rows($rsTotalHabRes) > 0)
			{
				$lTotalResDia = mysql_result($rsTotalHabRes,0,"hab_total_reservadas");
				if (is_null($lTotalResDia))
				{
					$lTotalResDia = 0;
				}
			}
			// Martin 02/07/07. Miramos si hay un desbloqueo. 
			$lCadena = "SELECT SUM(res.habitaciones) hab_total_desbloqueadas".
				" FROM ".__TABLA_RESERVAS__." AS res".
				", ".__TABLA_BLOQUEOS__." AS blo".
				" WHERE res.idestablecimiento = blo.idestablecimiento".
				" AND res.idtipo = blo.idtipo".
				" AND res.idestablecimiento = ".$lIDEstablecimiento.
				" AND res.idtipo = ".$lIDTipo.
				$lWhereFechas2.
				//" AND blo.fecha = ".$lj.
				//" AND ( blo.fecha = ".$lj.
				//"		OR blo.fecha = ".fSumarUnDia($lj)." )".
				" AND blo.bloqueado = 0".
				" AND res.borrado = 0".
				" AND res.estado = 0";
			$rsTotalHabResDesbloqueadas = fQuery($lCadena,$lConexionBDReservas);
			if (mysql_num_rows($rsTotalHabResDesbloqueadas) > 0)
			{
				$lTotalResDiaDesbloqueadas = mysql_result($rsTotalHabResDesbloqueadas,0,"hab_total_desbloqueadas");
				if (is_null($lTotalResDiaDesbloqueadas))
				{
					$lTotalResDiaDesbloqueadas = 0;
				}
			}
			$lTotalResDia = $lTotalResDia - $lTotalResDiaDesbloqueadas;
			if ( ($lHabitacionesDisponibles - $lTotalResDia) < $lMinHabDisponibles)
			{
				$lMinHabDisponibles = $lHabitacionesDisponibles - $lTotalResDia;
			}
		}
		$lHabitacionesDisponibles = $lMinHabDisponibles;

		// Compruebo si las habitaciones solicitadas son suficientes
		// piden mas habitaciones de las que hay disponibles?
		if($lHabitacionesDisponibles < $lHabitaciones)
			$lError = 6;
		// ??hay disponibles menos de las necesarias?
		if($lHabitacionesDisponibles < $lNecesarias)
			$lError = 6;
		// son necesarias ms de las que piden?
		if($lNecesarias > $lHabitaciones)
			$lError = 2;
	}
/*
		// Busco si la fecha llegada/salida choca con alguna fecha de vacaciones.
		$lCadena = "SELECT * FROM ".__TABLA_PYD_VACACIONES__." WHERE idestablecimiento = ".$lIDEstablecimiento.
			" AND fechainicio = ".$lFechaDesde;
		$rsVacaciones = fQuery($lCadena);

		if(mysql_num_rows($rsVacaciones)>0)
			$lError = 5; // El establecimiento cierra la habitaci n en alguno de los dias indicados
*/
//echo("En fEstablecimientoDisponible. Mensaje5. lError=".$lError." <br />");
	return $lError;
}

?>