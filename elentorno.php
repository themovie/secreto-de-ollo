<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");

	// Martin 11/04/08. Las variables $lIdEstablecimiento y $lIdIdioma se establecen en 
	//el script inc_comun.php.
	$lIdimagen_ent_actual = $_GET["idimg_ent"];
	if ($lIdimagen_ent_actual == "")
	{
		$lIdimagen_ent_actual = $_SESSION["idimagen_ent_actual"];
	}
	if ($lIdimagen_ent_actual == "")
	{
		// Martin 07/04/09. Seleccionamos las fotos de tipo "entorno".
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lCadena = " SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							//" AND img.idimagen = ".$lIdimagen_est_act.
							" AND img.idimagen_tipo = 2". //Martin 07/04/09. Nuevo.
							" AND img.idtipo = -1".
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img_trad.ididioma = ".$lIdIdioma.
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
	}
	else
	{
		// Martin 07/04/09. Seleccionamos las fotos de tipo "entorno".
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lCadena = " SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							" AND img.idimagen_tipo = 2". //Martin 07/04/09. Nuevo.
							" AND img.idimagen = '".fLimpiar_sql($lIdimagen_ent_actual)."'".
							" AND img.idtipo = -1".
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img_trad.ididioma = ".$lIdIdioma.
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
	}
	$rsFotoEntAct = fQuery($lCadena);
	if (mysql_num_rows($rsFotoEntAct) > 0)
	{
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lIdimagenAct = mysql_result($rsFotoEntAct,0,"idimagen");
		//$lImagenAct = mysql_result($rsFotoEntAct,0,"imagen_lista");
		$lImagenAct = mysql_result($rsFotoEntAct,0,"imagen_pase");
		$lTituloImgAct = mysql_result($rsFotoEntAct,0,"titulo");
		$lDescrImgAct = mysql_result($rsFotoEntAct,0,"descripcion");
		$lSrcImgAct = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagenAct;
		//$lSrcImgAct = "http://hotelejemplo.reservadealojamientos.com/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagenAct;
		// Martin 15/09/10. Tratamiento para redimensionar la imagen.
		try {
			// Obtenemos la informacion de la foto.
			$lMaxAnchura = 470;
			$lMaxAltura = 625;
			$laInformacionFotosAct = getimagesize($lSrcImgAct); 
			$lWidthFotoAct = $laInformacionFotosAct[0]; //Anchura de la foto.
			$lHeightFotoAct = $laInformacionFotosAct[1]; //Altura de la foto.
			$lLeftFotoAct = 0;
			if ($lWidthFotoAct > $lMaxAnchura) {
				$lHeightFotoAct = floor(($lMaxAnchura * $lHeightFotoAct) / $lWidthFotoAct);
				$lWidthFotoAct = $lMaxAnchura;
			}
			if ($lHeightFotoAct > $lMaxAltura) {
				$lWidthFotoAct = floor(($lMaxAltura * $lWidthFotoAct) / $lHeightFotoAct);
				$lHeightFotoAct = $lMaxAltura;
			}
			if ($lWidthFotoAct < $lMaxAnchura) {
				$lLeftFotoAct = floor(($lMaxAnchura - $lWidthFotoAct) / 2);
			}
		}
		catch (Exception $e) {
			$lWidthFotoAct = 0;
			$lHeightFotoAct = 0;
			$lLeftFotoAct = 0;
		}
	}
	$_SESSION["idimagen_ent_actual"] = $lIdimagenAct;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
	<!--CSS de lightwindow-->
	<link rel="stylesheet" href="lightwindow/css/lightwindow.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="lightwindow/css/default.css" type="text/css" media="screen"/>
	<!--Fin CSS de lightwindow-->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
	<!--Javascript de lightwindow-->
	<script type="text/javascript" src="lightwindow/javascript/prototype.js"></script>
	<script type="text/javascript" src="lightwindow/javascript/effects.js"></script>
	<script type="text/javascript" src="lightwindow/javascript/lightwindow.js"></script>
	<!--Fin Javascript de lightwindow-->
<script src="include/js/funciones.js"></script>
<title><?php "hoteles casas rurales en navarra cerca de pamplona dormir alojamiento ".print(fLiteral(381,$lIdIdioma)); //El Entorno ?></title>

<?php 
	// Martin 29/04/09. Creamos un array con las imagenes grandes para el pase de fotos. Sera un array de 2 dimensiones.
	//Cada indice numerico del array contendra otro array con 4 elementos (sera un array asociativo).
	//		0-idimagen
	//		1-titulo de la imagen
	//		2-descripcion de la imagen
	//		3-ruta a la imagen
	$lCadena = "SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
						", img_trad.titulo, img_trad.descripcion".
						" FROM ".__TABLA_IMAGENES__." AS img".
						", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
						" WHERE img.idimagen = img_trad.idimagen".
						" AND img.idimagen_tipo = 2". //Martin 07/04/09. Nuevo.
						" AND img.idestablecimiento = ".$lIdEstablecimiento.
						" AND img.idtipo = -1".
						" AND img_trad.ididioma = ".$lIdIdioma.
						" AND img.visible = 1".
						" AND img.borrado = 0".
						" ORDER BY img.prioridad, img.titulo";
	$rsFotosEst = fQuery($lCadena);
	$lNumFotosEst = mysql_num_rows($rsFotosEst);
	$aFotos_pase = array();
	$lIndFotoAct = 0;
	if ($lNumFotosEst > 0) {
		for ($li=0;$li<$lNumFotosEst;$li++) {
			$aFila_act = mysql_fetch_array($rsFotosEst);
			$lImagen_act = $aFila_act["imagen_lista"];
			$lSrc_img_act = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagen_act;
			$lImagen_pase_act = ( ($aFila_act["imagen_pase"] != "") ? $aFila_act["imagen_pase"] : $lImagen_act );
			$lSrc_img_pase_act = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagen_pase_act;
			$aFotos_pase[$li]["idimagen"] = $aFila_act["idimagen"];
			$aFotos_pase[$li]["titulo"] = $aFila_act["titulo"];
			$aFotos_pase[$li]["descripcion"] = $aFila_act["descripcion"];
			$aFotos_pase[$li]["ruta_imagen"] = $lSrc_img_pase_act;
			if ($aFotos_pase[$li]["idimagen"] == $lIdimagen_ent_actual) {
				$lIndFotoAct = $li;
			}
		} //fin for ($li=0;$li<$lNumFotosEst;$li++) {...
	} //fin if ($lNumFotosEst > 0) {...
?>

</head>

<body class="top" onload="igualaColumnas3();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<!--<div class="span-12 border" id="cuerpo">-->
				<div class="span-13" id="cuerpo">
					<!--<div class="span-12" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower(fLiteral(352,$lIdIdioma)))); //EL ENTORNO ?>
					</div>
					<!--<div class="span-12" id="foto">-->
					<div id="foto">
						<a class="lightwindow" caption="" author="" title="<?php print($aFotos_pase[$lIndFotoAct]["titulo"]); ?>" 
							 rel="Galer�a[El entorno]" href="<?php print($aFotos_pase[$lIndFotoAct]["ruta_imagen"]); ?>">
						<?php /*<img src="<?php print($lSrcImgAct); ?>" alt="<?php print($lTituloImgAct); ?>" />*/ ?>
						<img src="<?php print($lSrcImgAct); ?>" alt="<?php print($lTituloImgAct); ?>" 
							style="width: <?php print($lWidthFotoAct); ?>px; height: <?php print($lHeightFotoAct); ?>px; margin-left:<?php print($lLeftFotoAct); ?>px;" />
						</a>
<?php 
						if ($lNumFotosEst > 1) {
							for ($lk=($lIndFotoAct + 1);$lk<$lNumFotosEst;$lk++) {
?>
								<a class="lightwindow hidden" author="" caption="" title="<?php print($aFotos_pase[$lk]["titulo"]); ?>" 
									rel="Galer�a[El entorno]" href="<?php print($aFotos_pase[$lk]["ruta_imagen"]); ?>">imagen #<?php print($lk); ?></a>
<?php 
							}
							for ($lk=0;$lk<$lIndFotoAct;$lk++) {
?>
								<a class="lightwindow hidden" author="" caption="" title="<?php print($aFotos_pase[$lk]["titulo"]); ?>" 
									rel="Galer�a[El entorno]" href="<?php print($aFotos_pase[$lk]["ruta_imagen"]); ?>">imagen #<?php print($lk); ?></a>
<?php 
							}
						} //fin if ($lNumFotosEst > 1) {...
?>

					</div>
					<!--<div class="span-12 border" id="barra_seleccion_fotos">-->
					<div id="barra_seleccion_fotos">
	<?php
		// Martin 07/04/09. Seleccionamos las fotos del establecimiento de tipo "entorno".
		$lCadena = "SELECT img.idimagen, img.imagen_lista".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							" AND img.idimagen_tipo = 2". //Martin 07/04/09. Nuevo.
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img.idtipo = -1".
							" AND img_trad.ididioma = ".$lIdIdioma.
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
		$rsFotosEnt = fQuery($lCadena);
		$lNumFotosEnt = mysql_num_rows($rsFotosEnt);
		if ($lNumFotosEnt > 0)
		{
			// Martin 14/04/08. Guardamos en una variable cuantos numeros de fotos mostraremos en la lista de fotos
			//seleccionable. Como mucho seran 12, pero si no se han subido tantas fotos ser�n el total de fotos.
			// Martin 05/05/09. Ampliamos el numero maximo de fotos a 16.
			$lNumElementosListaFotos = min($lNumFotosEnt,1);
			for ($li=0;$li<$lNumFotosEnt;$li++)
			{
				// Martin 14/04/08. Metemos todos los identificadores de fotos del establecimiento en un array.
				$lIdimagen = mysql_result($rsFotosEnt,$li,"idimagen");
				$laListaFotosEnt[$li] = $lIdimagen;
				if ($lIdimagen == $_SESSION["idimagen_ent_actual"])
				{
					// Martin 14/04/08. Guardamos la posicion que ocupa en el array la foto actual en la variable de 
					//sesion lista_fotos_est_actual.
					$_SESSION["lista_fotos_ent_actual"] = $li;
				}
			}
			// Martin 14/04/08. En las variables de sesion lista_fotos_est_primera y lista_fotos_est_ultima guardamos 
			//el primer y ultimo indice del array que mostraremos en la barra de fotos (por ejemplo del 3 al 14, ya que 
			//solo mostraremos como mucho 12 numeros de fotos para elegir).
			if ($_SESSION["lista_fotos_ent_primera"] == "")
			{
				$_SESSION["lista_fotos_ent_primera"] = 0;
				$_SESSION["lista_fotos_ent_ultima"] = $lNumElementosListaFotos - 1;
			}
			if ($_SESSION["lista_fotos_ent_actual"] > $_SESSION["lista_fotos_ent_ultima"])
			{
				$_SESSION["lista_fotos_ent_primera"]++;
				$_SESSION["lista_fotos_ent_ultima"] = $_SESSION["lista_fotos_ent_actual"];
			}
			if ($_SESSION["lista_fotos_ent_actual"] < $_SESSION["lista_fotos_ent_primera"])
			{
				$_SESSION["lista_fotos_ent_primera"] = $_SESSION["lista_fotos_ent_actual"];
				$_SESSION["lista_fotos_ent_ultima"]--;
			} //fin if ($_SESSION["lista_fotos_est_primera"] == "")...
			// Martin 14/04/08. En las variables $lNumImagenAnterior y $lNumImagenSiguiente guardamos los indices del array 
			//de fotos anterior y posterior al indice actual. Se asignaran a los botones de navegacion que hay en los 
			//extremos de la barra de seleccion de  fotos.
			if ($_SESSION["lista_fotos_ent_actual"] > 0)
			{
				$lNumImagenAnterior = $_SESSION["lista_fotos_ent_actual"] - 1;
			}
			else
			{
				$lNumImagenAnterior = 0;
			} //fin if ($_SESSION["lista_fotos_ent_actual"] > 0)...
			if ($_SESSION["lista_fotos_ent_actual"] < ($lNumFotosEnt - 1))
			{
				$lNumImagenSiguiente = $_SESSION["lista_fotos_ent_actual"] + 1;
			}
			else
			{
				$lNumImagenSiguiente = $lNumFotosEnt - 1;
			} //if ($_SESSION["lista_fotos_ent_actual"] < $lNumFotosEnt)...
			// Martin 14/04/08. Las variables $lParametros y $lParametrosSID se establece en el fichero /inc_comun.php
	?>
						<div class="numero_foto">
							<a href="elentorno.php<?php print($lParametros."&idimg_ent=".$laListaFotosEnt[$lNumImagenAnterior].$lParametrosSID); ?>"> &lt; </a>
						</div>
	<?php
			for ($lj=$_SESSION["lista_fotos_ent_primera"];$lj<=$_SESSION["lista_fotos_ent_ultima"];$lj++)
			{
				$lNumfoto = $lj + 1;
				$lIdimagen = $laListaFotosEnt[$lj];
				if ($lj != $_SESSION["lista_fotos_ent_primera"]) {
	?>
						<div class="separacion"> | </div>
	<?php
				}
	//print("SID=".SID);
	?>
						<div class="<?php if ($lIdimagen == $_SESSION["idimagen_ent_actual"]) { ?>foto_seleccionada<?php } else { ?>numero_foto<?php } ?>">
							<a href="elentorno.php<?php print($lParametros."&idimg_ent=".$lIdimagen.$lParametrosSID); ?>"> <?php print($lNumfoto); ?> </a>
						</div>
	<?php
			} //fin for ($lj=$_SESSION["lista_fotos_ent_primera"];$lj<=$_SESSION["lista_fotos_ent_ultima"];$lj++)...
	?>
						<div class="numero_foto">
							<a href="elentorno.php<?php print($lParametros."&idimg_ent=".$laListaFotosEnt[$lNumImagenSiguiente].$lParametrosSID); ?>"> &gt; </a>
						</div>
	<?php
		} //fin if ($lNumFotosEst > 0)...
	?>
					</div>
					<?php // Martin 07/04/09. Quitamos la foto de adorno que habiamos metido porque ahora habra una galeria de fotos. ?>
					<!--<div id="foto_elentorno">
						<img src="images/foto_elentorno.gif" />
					</div>-->
	<?php
					// Martin 17/04/08. Seleccionamos la informacion del entorno del establecimiento.
					//Las variables $lIdEstablecimiento y $lIdIdioma se establecen en el script inc_comun.php. 
					$lCadena = "SELECT descripcion".
									" FROM ".__TABLA_ENTORNO_INFORMACION__.
									" WHERE idestablecimiento = ".$lIdEstablecimiento.
									" AND ididioma = ".$lIdIdioma;
					$rsEntornoInf = fQuery($lCadena);
					if (mysql_num_rows($rsEntornoInf) > 0)
					{
						$lDesEntornoInf = mysql_result($rsEntornoInf,0,"descripcion");
					}
	?>
					<div class="texto">
						<h5> <?php print(ucfirst(mb_strtolower(fLiteral(352,$lIdIdioma)))); //EL ENTORNO ?> </h5>
					<p>
					<?php print(str_replace("\n","</p><p>",$lDesEntornoInf)); ?>
					</p>
					</div>
				</div>
			</div>
			<!--<div class="span-5 append-1 last" id="columna_dcha">-->
			<div class="span-5 last" id="columna_dcha">
				<?php include("include/modulos/inc_boton_megusta_facebook_col_dcha.php"); ?>
				<h4> <?php print(ucfirst(mb_strtolower(fLiteral(382,$lIdIdioma)))); //ENLACES DE INTER�S ?> </h4>
<?php
				// Martin 17/04/08. Seleccionamos los enlaces de interes del establecimiento.
				//Las variables $lIdEstablecimiento y $lIdIdioma se establecen en el script inc_comun.php. 
				$lCadena = " SELECT link_trad.titulo, link_trad.descripcion, link_trad.url".
								" FROM ".__TABLA_LINKS__." AS link".
								", ".__TABLA_LINKS_TRADUCCIONES__." AS link_trad".
								" WHERE link.idlink = link_trad.idlink".
								" AND link.idestablecimiento = ".$lIdEstablecimiento.
								" AND link_trad.ididioma = ".$lIdIdioma.
								" AND link.visible = 1".
								" AND link.borrado = 0".
								" ORDER BY link.prioridad, link_trad.titulo";
				$rsLinks = fQuery($lCadena);
				$lNumLinks = mysql_num_rows($rsLinks);
				if ($lNumLinks > 0)
				{
?>
				<div class="agrupacion_elementos">
<?php
					for ($li=0;$li<$lNumLinks;$li++)
					{
						$lLinkTitulo = mysql_result($rsLinks,$li,"titulo");
						$lLinkDescripcion = mysql_result($rsLinks,$li,"descripcion");
						$lLinkUrl = mysql_result($rsLinks,$li,"url");
						if (substr($lLinkUrl,0,7) != "http://")
						{
							$lLinkUrl = "https://".$lLinkUrl;
						}
?>
					<div class="elemento <?php if ($li == 0) { ?>primero<?php } ?>"> &gt; <a href="<?php print($lLinkUrl); ?>" target="_blank"><?php print($lLinkTitulo); ?></a> </div>
<?php
					}
?>
				</div>
<?php
				}
?>
			</div>
			<?php include("include/modulos/inc_pie.php"); ?>
		</div>
	</div>
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
