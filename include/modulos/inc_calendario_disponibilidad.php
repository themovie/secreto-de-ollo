<div class="span-6 calendario_leyenda_primero">
	<span id="colorreserva30" class="cuadraditocolor"> &nbsp;&nbsp;&nbsp; </span>
	<span class="textocolor"> &nbsp;<?php print(fLiteral(377,$lIdIdioma)); //Reservadas ?> 0% - 30% &nbsp;&nbsp;&nbsp; </span>
</div>
<div class="span-6 last calendario_leyenda_primero">
	<span id="colorreserva99" class="cuadraditocolor"> &nbsp;&nbsp;&nbsp; </span>
	<span class="textocolor"> &nbsp;<?php print(fLiteral(377,$lIdIdioma)); //Reservadas ?> 60% - 99% &nbsp;&nbsp;&nbsp; </span>
</div>
<div class="span-6">
	<span id="colorreserva60" class="cuadraditocolor"> &nbsp;&nbsp;&nbsp; </span>
	<span class="textocolor"> &nbsp;<?php print(fLiteral(377,$lIdIdioma)); //Reservadas ?> 30% - 60% &nbsp;&nbsp;&nbsp; </span>
</div>
<div class="span-6 last">
	<span id="colorreserva100" class="cuadraditocolor"> &nbsp;&nbsp;&nbsp; </span>
	<span class="textocolor"> &nbsp;<?php print(fLiteral(378,$lIdIdioma)); //Reservadas todas ?> &nbsp;&nbsp;&nbsp; </span>
</div>
<div class="span-12 last">
					<table class="tablacalendariodisp">
<?php
// Martin 18/04/08, inicializamos las variables $lMes y $lAnio con el valor del anio actual.
$lMes = 1;
$lMes = date("n",time());
$lAnio = date("Y",time());
if ($_SESSION["idtipo"]=="1")
{
	//$lPrimerDia = strtotime("1/1/".$lAnio." 12:00:00");
	$lPrimerDia = strtotime($lMes."/1/".$lAnio." 12:00:00");
	$lUltimoMes = date("n",fSumarMeses($lPrimerDia,11));
	$lNumDiasUltimoMes = fDiasMes($lUltimoMes,($lAnio + 1));
	//$lUltimoDia = strtotime("12/31/".$lAnio." 12:00:00");
	//$lUltimoDia = strtotime($lUltimoMes."/".$lNumDiasUltimoMes."/".($lAnio + 1)." 12:00:00");
	if ($lUltimoMes < $lMes) 
	{
		// Martin 11/12/08. Estamos en el caso normal en que el ultimo mes es menor que el mes inicial, por ejemplo en 
		//un calendario de 05/2008 a 04/2009. Para calcular el ultimo dia hay que sumar 1 al anio.
		$lUltimoDia = strtotime($lUltimoMes."/".$lNumDiasUltimoMes."/".($lAnio + 1)." 12:00:00");
	}
	else
	{
		// Martin 11/12/08. Estamos en el unico caso en que el ultimo mes no es menor que el mes inicial (calendario de 
		//01/2008 a 12/2008 por ejemplo), entonces el ultimo dia hay que calcularlo para el anio actual.
		$lUltimoDia = strtotime($lUltimoMes."/".$lNumDiasUltimoMes."/".$lAnio." 12:00:00");
	}
	//  Martin 16/10/07. Si estamos en un establecimiento tipo hotel obtenemos una matriz en la que por cada d�a 
	//del a�o obtenemos una matriz con todos los tipos de habitaci�n en la que tenemos el n� de habitaciones asignadas.
	//[fecha][tipohabitacion] => n� de habitaciones asignadas.
	// Martin 17/04/08, sustituimos la variable $lIDPrincipal.
	$laHabAsignadasFecha = fCalcularHabitacionesAsignadas($lIdEstablecimiento/*$lIDPrincipal*/,$lPrimerDia,$lUltimoDia);
	//  Martin 16/10/07. Si estamos en un establecimiento tipo hotel obtenemos una matriz en la que por cada d�a 
	//del a�o obtenemos una matriz con todos los tipos de habitaci�n en la que tenemos el n� de habitaciones reservadas.
	//[fecha][tipohabitacion] => n� de habitaciones reservadas.
	// Martin 17/04/08, sustituimos la variable $lIDPrincipal.
	$laHabReservadasFecha = fCalcularHabitacionesReservadas($lIdEstablecimiento/*$lIDPrincipal*/,$lPrimerDia,$lUltimoDia);
//print_r($laHabAsignadasFecha);
//print("<br />");
//print_r($laHabReservadasFecha);
}
//Martin. Calculamos el n� de filas que necesitamos en la tabla del calendario para cada mes.
$lMesSem = $lMes;
$lAnioSem = $lAnio;
$laNumSemMes = array("");
//for ($lNumMes=0;$lNumMes<6;$lNumMes++)
for ($lNumMes=0;$lNumMes<12;$lNumMes++)
{
	if ($lNumMes > 0)
	{
		$lMesSem++;
	}
	// Martin 11/12/08. El if comentado estaba provocando que las filas que comenzaban con un mes del anio siguiente 
	//se viesen mal porque no se estaba calculando bien su numero de semanas
	//if ($lMes == 13)
	if ($lMesSem == 13)
	{
		$lMesSem = 1;
		$lAnioSem += 1;
	}
	$lNumPrimSem = date("W",strtotime($lMesSem."/1/".$lAnioSem." 12:00:00"));
	if ($lNumPrimSem >= 52 ) 
	{
		$lNumPrimSem = 0;
	}
	$lUltDiaMes = fDiasMes($lMesSem,$lAnioSem);
	$lNumUltSem = date("W",strtotime($lMesSem."/".$lUltDiaMes."/".$lAnioSem." 12:00:00"));
	if ($lNumUltSem==1) 
	{
		$lUltDiaMes -= 7;
		$lNumUltSem = date("W",strtotime($lMesSem."/".$lUltDiaMes."/".$lAnioSem." 12:00:00")) + 1;
	}
	$lNumSemMes = ($lNumUltSem - $lNumPrimSem) + 1;
//echo("lNumSemMes=".$lNumSemMes."<br />");	
	$laNumSemMes[$lNumMes] = $lNumSemMes;
}

$lNumMesInicial = 0;
//for ($lNumMes=0;$lNumMes<6;$lNumMes++)
for ($lNumMes=0;$lNumMes<12;$lNumMes++)
{
	if ($lNumMes > 0)
	{
		$lMes++;
	}
	if ($lMes == 13)
	{
		$lMes = 1;
		$lAnio += 1;
	}
//	if (($lNumMes%2)==0)
//	{
//		$lNumFilasCal = max($laNumSemMes[$lNumMes],$laNumSemMes[($lNumMes+1)]);
	//Mart�n. Calculamos el m�ximo n� de filas de los meses de una fila del calendario.
	if ((($lNumMes-$lNumMesInicial)%3)==0)
	{
		$lNumFilasCal = max($laNumSemMes[$lNumMes],$laNumSemMes[($lNumMes+1)],$laNumSemMes[($lNumMes+2)]);
//	if ((($lNumMes-$lNumMesInicial)%3)==0)
//	{
//		$lNumFilasCal = max($laNumSemMes[$lNumMes],$laNumSemMes[($lNumMes+1)],$laNumSemMes[($lNumMes+2)]);
?>
						<tr>
<?php
	}
?>

							<!--<td align="center" >-->
							<td>
							<table class="tablamescalendariodisp">

								<tr>
									<td width="100%" colspan="7" class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td class="celdacalendariodisp_nombre">
													<?php 
														//Martin 17/04/08, Sustituimos la variable de sesion que usamos en el administrador 
														//por la variable $lIdIdioma que se estable en inc_comun.php.
														// Martin 21/04/08. Anadimos el anio.
														$lNombreMes = fNombreMes($lMes,$lIdIdioma);
														print(strtoupper(substr($lNombreMes,0,1)).substr($lNombreMes,1)." ".$lAnio);
													?>
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
<?php
	//Mart�n. Ponemos los d�as de la semana de un mes.
	for ($li=1;$li<=7;$li++) 
	{
?>
									<td width="14%" class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td class="celdacalendariodisp_nombre">
										<?php 
											print(substr(fDiaSemana($li,"c"),0,1));
										?>
												</td>
											</tr>
										</table>
									</td>
<?php
	}
?>
								</tr>
<?php 
	$lDiasMes = fDiasMes($lMes,$lAnio);
	$lDiaMes = 1;
	
//echo("lNumFilasCal=".$lNumFilasCal."<br />");
	//for ($li=1;$li<=6;$li++)
	//Mart�n. Construimos un mes.
	for ($li=1;$li<=$lNumFilasCal;$li++)
	{
?>
								<tr>
<?php
		for ($lj=1;$lj<=7;$lj++)
		{
//echo("lDiaMes=".$lDiaMes."; lDiasMes=".$lDiasMes."<br />");
			if ($lDiaMes<=$lDiasMes) 
			{
				//Mart�n. Calculamos el dia de la semana del primer d�a del mes. 1-Lunes; 2-Mantes; ... 7-Domingo.
				$lDiaSemana = date("w",strtotime($lMes."/".$lDiaMes."/".$lAnio." 12:00:00"));
				$lFechaUnix = strtotime($lMes."/".$lDiaMes."/".$lAnio." 12:00:00");
				if ($lDiaSemana == 0) 
				{
					$lDiaSemana = 7;
				}
//echo("lDiaSemana=".$lDiaSemana."; lj=".$lj.".<br />");
				//Mart�n. No empezaremos a dibujar d�as en el calendario hasta que $lj coincida con el 
				//primer d�a de la semana. 
				if ($lj==$lDiaSemana) 
				{
//echo("Mensaje1. <br />");

					if ($_SESSION["idtipo"]=="1")
					{
						$esLunes = "";
						if ($lDiaSemana==1)
						{
							$esLunes = "Lunes";
						}
						if ($lIdTipoHab=="")
						{
							// Martin 12/11/07, si lIdTipoHab esta vacio entonces estamos calculando la disponibilidad 
							//del conjunto total de habitaciones
							$lEsReserva = $laHabReservadasFecha[$lFechaUnix]["total"];
							$lHabAsignadas = $laHabAsignadasFecha[$lFechaUnix]["total"];
						}
						elseif ($lIdTipoHab!="")
						{
							$lEsReserva = $laHabReservadasFecha[$lFechaUnix][$lIdTipoHab];
							$lHabAsignadas = $laHabAsignadasFecha[$lFechaUnix][$lIdTipoHab];
						}
						if ($lEsReserva > 0)
						{
							// Martin 14/01/08. Se tiene en cuenta que se ha podido cambiar el numero de 
							//habitaciones asignadas a 0.
							$lPorcenReservas = (($lHabAsignadas==0)? 100 : round((($lEsReserva / $lHabAsignadas) * 100),2));
							//$lPorcenReservas = round((($lEsReserva / $lHabAsignadas) * 100),2);
						}
						elseif ($lHabAsignadas == 0)
						{
							// Martin 07/02/08. Si un dia no hay habitacioanes asignadas se 
							//muestra como totalmente ocupado.
							$lPorcenReservas = 100;
						}
						else
						{
							$lCadena = "SELECT *".
									" FROM ".__TABLA_ESTABLECIMIENTOS_CIERRE__.
									" WHERE idestablecimiento = ".$lIdEstablecimiento.
									" AND fecha = ".$lFechaUnix;
							$rsCierre = fQuery($lCadena);
							if (mysql_num_rows($rsCierre) > 0) 
							{
								$lPorcenReservas = 100;
							}
							else
							{
								$lCadena = "SELECT *".
									" FROM ".__TABLA_BLOQUEOS__.
									" WHERE idestablecimiento = ".$lIdEstablecimiento.
									" AND fecha = ".$lFechaUnix.
									" AND bloqueado = 1";
								$rsDiaBloqueado = fQuery($lCadena);
								if (mysql_num_rows($rsDiaBloqueado) > 0)
								{
									$lPorcenReservas = 100;
								}
								//elseif ($lHabAsignadas == 0)
								//{
								//	// Martin 07/02/08. Si un dia no hay habitacioanes asignadas se 
								//	//muestra como totalmente ocupado.
								//	$lPorcenReservas = 100;
								//}
								else
								{
									$lPorcenReservas = 0;
								}
							} //fin if (mysql_num_rows($rsCierre) > 0)...
						} //fin if ($lEsReserva > 0)...

						$lColorCelda = "";

						$lClaseCelda = " class=\"celdacalendariodisp";
						//Martin 04/01/08. Los dias festivos ya no se marcaran con un color de fondo distinto.
						//if ($lFestivo) 
						//{
						//	$lClaseCelda = $lClaseCelda."festivo";
						//}
						$lClaseCelda = $lClaseCelda.$esLunes."\"";

//						if ($lEsReserva)
//						{
//							if ($_SESSION["idtipo"]=="1")
//							{
//								if ($lPorcenReservas > 0 && $lPorcenReservas <= 30)
//								{
//									$lColorCelda = " id=\"colorreserva30\"";
//								}
//								elseif ($lPorcenReservas > 30 && $lPorcenReservas <= 60)
//								{
//									$lColorCelda = " id=\"colorreserva60\"";
//								}
//								elseif ($lPorcenReservas > 60 && $lPorcenReservas < 100)
//								{
//									$lColorCelda = " id=\"colorreserva99\"";
//								}
//								else
//								{
//									$lColorCelda = " id=\"colorreserva100\"";
//								}
//							} //fin if ($_SESSION["idtipo"]=="1")...
//						} // fin  if ($lEsReserva)...

								if ($lPorcenReservas > 0 && $lPorcenReservas <= 30)
								{
									$lColorCelda = " id=\"colorreserva30\"";
								}
								elseif ($lPorcenReservas > 30 && $lPorcenReservas <= 60)
								{
									$lColorCelda = " id=\"colorreserva60\"";
								}
								elseif ($lPorcenReservas > 60 && $lPorcenReservas < 100)
								{
									$lColorCelda = " id=\"colorreserva99\"";
								}
								elseif ($lPorcenReservas == 100)
								{
									$lColorCelda = " id=\"colorreserva100\"";
								}

					}
					else
					{
						$esLunes = "";
						if ($lDiaSemana==1)
						{
							$esLunes = "Lunes";
						}
						$lColorCelda = "";

						$lClaseCelda = " class=\"celdacalendariodisp";
						//Martin 04/01/08. Los dias festivos ya no se marcaran con un color de fondo distinto.
						//if ($lFestivo) 
						//{
						//	$lClaseCelda = $lClaseCelda."festivo";
						//}
						$lClaseCelda = $lClaseCelda.$esLunes."\"";
					} //fin if ($_SESSION["idtipo"]=="1")

?>
									<td width="14%"  class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td <?php print($lClaseCelda.$lColorCelda); ?>>
<?php
					if ($_SESSION["idtipo"] == 1)
					{
						print("$lDiaMes");
					}
?>
												</td>
											</tr>
										</table>
									</td>
<?php
					$lDiaMes++;
				} // fin  if ($lj==$lDiaSemana)...
				//$lDiaMes++;
				//else
				//{
				//	break;
				//}
			} // fin  if ($lDiaMes<=$lDiasMes)...
			if (($lj!=$lDiaSemana) || 
					(($lj==$lDiaSemana) 
						&& ($lDiaMes>($lDiasMes)) 
						&& ($li==$lNumFilasCal) 
						&& ($lNumFilasCal>$laNumSemMes[$lNumMes]) ))
			{
				$esLunes = "";
				if ($lj==1)
				{
					$esLunes = "Lunes";
				}	
//echo("Mensaje2. lDiaMes=".$lDiaMes."; lDiasMes=".$lDiasMes."; lj=".$lj."; lDiaSemana=".$lDiaSemana."; <br />");
?>
									<td width="14%"  class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td class="celdacalendariodisp<?php print($esLunes); ?>">&nbsp;</td>
											</tr>
										</table>
									</td>
<?php
			} //fin  if (($lj!=$lDiaSemana) || ...
			//$lDiaMes++;
//echo("Dia en blanco. <br />");
		} // fin  for ($lj=1;$lj<=7;$lj++)...
//		if ($lDiaMes>$lDiasMes) 
//		{
//			break;
//		}
?>
								</tr>
<?php
	} // fin  for ($li=1;$li<=$lNumFilasCal;$li++)...
?>
							</table>
							</td>
<?php 
//echo("Mensaje1. lNumMes=".$lNumMes."<br />");
	if ((($lNumMes-$lNumMesInicial)%3)==2)
	{
		//Mart�n. Cerramos una fila de meses
?>
						</tr>
<?php 
	}
?>
<?php
} // fin  for ($lNumMes=0;$lNumMes<12;$lNumMes++)...
//echo("Mensaje2. lNumMes=".$lNumMes."<br />");
?>
					</table>
</div>
