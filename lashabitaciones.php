<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");

	// Martin 14/04/08. Las variables $lIdEstablecimiento y $lIdIdioma se establecen en 
	//el script inc_comun.php.
	$lIdimagen_hab_actual = $_GET["idimg_hab"];
	if ($lIdimagen_hab_actual == "")
	{
		$lIdimagen_hab_actual = $_SESSION["idimagen_hab_actual"];
	}
	if ($lIdimagen_hab_actual == "")
	{
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lCadena = " SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							//" AND img.idimagen = ".$lIdimagen_est_act.
							" AND img.idtipo != -1".
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img_trad.ididioma = ".$lIdIdioma.
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
	}
	else
	{
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lCadena = " SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							" AND img.idimagen = '".fLimpiar_sql($lIdimagen_hab_actual)."'".
							" AND img.idtipo != -1".
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img_trad.ididioma = ".$lIdIdioma.
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
	}
	$rsFotoHabAct = fQuery($lCadena);
	if (mysql_num_rows($rsFotoHabAct) > 0)
	{
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lIdimagenAct = mysql_result($rsFotoHabAct,0,"idimagen");
		//$lImagenAct = mysql_result($rsFotoHabAct,0,"imagen_lista");
		$lImagenAct = mysql_result($rsFotoHabAct,0,"imagen_pase");
		$lTituloImgAct = mysql_result($rsFotoHabAct,0,"titulo");
		$lDescrImgAct = mysql_result($rsFotoHabAct,0,"descripcion");
		$lSrcImgAct = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_TIPOS_HAB__.$lImagenAct;
		//$lSrcImgAct = "http://hotelejemplo.reservadealojamientos.com/".__DIR_UPLOADS__.__DIR_TIPOS_HAB__.$lImagenAct;
		// Martin 15/09/10. Tratamiento para redimensionar la imagen.
		try {
			// Obtenemos la informacion de la foto.
			$lMaxAnchura = 470;
			$lMaxAltura = 625;
			$laInformacionFotosAct = getimagesize($lSrcImgAct); 
			$lWidthFotoAct = $laInformacionFotosAct[0]; //Anchura de la foto.
			$lHeightFotoAct = $laInformacionFotosAct[1]; //Altura de la foto.
			$lLeftFotoAct = 0;
			if ($lWidthFotoAct > $lMaxAnchura) {
				$lHeightFotoAct = floor(($lMaxAnchura * $lHeightFotoAct) / $lWidthFotoAct);
				$lWidthFotoAct = $lMaxAnchura;
			}
			if ($lHeightFotoAct > $lMaxAltura) {
				$lWidthFotoAct = floor(($lMaxAltura * $lWidthFotoAct) / $lHeightFotoAct);
				$lHeightFotoAct = $lMaxAltura;
			}
			if ($lWidthFotoAct < $lMaxAnchura) {
				$lLeftFotoAct = floor(($lMaxAnchura - $lWidthFotoAct) / 2);
			}
		}
		catch (Exception $e) {
			$lWidthFotoAct = 0;
			$lHeightFotoAct = 0;
			$lLeftFotoAct = 0;
		}
	}
	$_SESSION["idimagen_hab_actual"] = $lIdimagenAct;
//print("lashabitaciones.php. \$_SERVER[\"HTTP_HOST\"]=".$_SERVER["HTTP_HOST"]."<br />");
//print("lashabitaciones.php. \$_SERVER[\"REQUEST_URI\"]=".$_SERVER["REQUEST_URI"]."<br />");
//print("lashabitaciones.php. \$_SERVER[\"SCRIPT_FILENAME\"]=".$_SERVER["SCRIPT_FILENAME"]."<br />");

	// Martin 29/04/09. Creamos un array con las imagenes grandes para el pase de fotos. Sera un array de 2 dimensiones.
	//Cada indice numerico del array contendra otro array con 4 elementos (sera un array asociativo).
	//		0-idimagen
	//		1-titulo de la imagen
	//		2-descripcion de la imagen
	//		3-ruta a la imagen
	$lCadena = "SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
						", img_trad.titulo, img_trad.descripcion".
						" FROM ".__TABLA_IMAGENES__." AS img".
						", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
						" WHERE img.idimagen = img_trad.idimagen".
						" AND img.idestablecimiento = ".$lIdEstablecimiento.
						" AND img.idtipo != -1".
						" AND img_trad.ididioma = ".$lIdIdioma.
						" AND img.visible = 1".
						" AND img.borrado = 0".
						" ORDER BY img.prioridad, img.titulo";
	$rsFotosHab_pase = fQuery($lCadena);
	$lNumFotosHab_pase = mysql_num_rows($rsFotosHab_pase);
	$aFotos_pase = array();
	$lIndFotoAct = 0;
	if ($lNumFotosHab_pase > 0) {
		for ($li=0;$li<$lNumFotosHab_pase;$li++) {
			$aFila_act = mysql_fetch_array($rsFotosHab_pase);
			$lImagen_act = $aFila_act["imagen_lista"];
			$lSrc_img_act = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_TIPOS_HAB__.$lImagen_act;
			$lImagen_pase_act = ( ($aFila_act["imagen_pase"] != "") ? $aFila_act["imagen_pase"] : $lImagen_act );
			$lSrc_img_pase_act = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_TIPOS_HAB__.$lImagen_pase_act;
			$aFotos_pase[$li]["idimagen"] = $aFila_act["idimagen"];
			$aFotos_pase[$li]["titulo"] = $aFila_act["titulo"];
			$aFotos_pase[$li]["descripcion"] = $aFila_act["descripcion"];
			$aFotos_pase[$li]["ruta_imagen"] = $lSrc_img_pase_act;
			if ($aFotos_pase[$li]["idimagen"] == $lIdimagen_hab_actual) {
				$lIndFotoAct = $li;
			}
		} //fin for ($li=0;$li<$lNumFotosEst;$li++) {...
	} //fin if ($lNumFotosEst > 0) {...
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
	<!--CSS de lightwindow-->
	<link rel="stylesheet" href="lightwindow/css/lightwindow.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="lightwindow/css/default.css" type="text/css" media="screen"/>
	<!--Fin CSS de lightwindow-->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
	<!--Javascript de lightwindow-->
	<script type="text/javascript" src="lightwindow/javascript/prototype.js"></script>
	<script type="text/javascript" src="lightwindow/javascript/effects.js"></script>
	<script type="text/javascript" src="lightwindow/javascript/lightwindow.js"></script>
	<!--Fin Javascript de lightwindow-->
<script src="include/js/funciones.js"></script>
<title><?php print("El Secreto de Ollo hotel rural en navarra cerca pamplona navarra agroturismos ".fLiteral(391,$lIdIdioma)); //Las habitaciones ?></title>
</head>

<body class="top" onload="igualaColumnas3();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<!--<div class="span-12 border" id="cuerpo">-->
				<div class="span-13" id="cuerpo">
					<!--<div class="span-12" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower(fLiteral(350,$lIdIdioma)))); //LAS HABITACIONES ?>
					</div>
					<!--<div class="span-12" id="foto">-->
					<div id="foto">
						<div id="foto_leyenda">
							<?php print($aFotos_pase[$lIndFotoAct]["titulo"]) ?>
						</div>
						<a class="lightwindow" caption="" author="" title="<?php print($aFotos_pase[$lIndFotoAct]["titulo"]); ?>" 
							 rel="Galer�a[Habitaciones]" href="<?php print($aFotos_pase[$lIndFotoAct]["ruta_imagen"]); ?>">
						<?php /*<img src="<?php print($lSrcImgAct); ?>" alt="<?php print($lTituloImgAct); ?>" />*/ ?>
						<img src="<?php print($lSrcImgAct); ?>" alt="<?php print($lTituloImgAct); ?>" 
							style="width: <?php print($lWidthFotoAct); ?>px; height: <?php print($lHeightFotoAct); ?>px; margin-left:<?php print($lLeftFotoAct); ?>px;" />
						</a>
<?php 
						if ($lNumFotosHab_pase > 1) {
							for ($lk=($lIndFotoAct + 1);$lk<$lNumFotosHab_pase;$lk++) {
?>
								<a class="lightwindow hidden" author="" caption="" title="<?php print($aFotos_pase[$lk]["titulo"]); ?>" 
									rel="Galer�a[Habitaciones]" href="<?php print($aFotos_pase[$lk]["ruta_imagen"]); ?>">imagen #<?php print($lk); ?></a>
<?php 
							}
							for ($lk=0;$lk<$lIndFotoAct;$lk++) {
?>
								<a class="lightwindow hidden" author="" caption="" title="<?php print($aFotos_pase[$lk]["titulo"]); ?>" 
									rel="Galer�a[Habitaciones]" href="<?php print($aFotos_pase[$lk]["ruta_imagen"]); ?>">imagen #<?php print($lk); ?></a>
<?php 
							}
						} //fin if ($lNum_img_pase > 1) {if ($lNumFotosEst > 1) {...
?>
					</div>
					<!--<div class="span-12" id="barra_seleccion_fotos">-->
					<div id="barra_seleccion_fotos">
						<!--<div id="texto_fotos">
							<?php //print(fLiteral(358,$lIdIdioma)); //Fotos ?>
						</div>-->
	<?php
		// Martin 08/04/08, Seleccionamos las fotos del establecimiento
		$lCadena = "SELECT img.idimagen, img.imagen_lista".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img.idtipo != -1".
							" AND img_trad.ididioma = ".$lIdIdioma.
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
		$rsFotosHab = fQuery($lCadena);
		$lNumFotosHab = mysql_num_rows($rsFotosHab);
		if ($lNumFotosHab > 0)
		{
			// Martin 14/04/08. Guardamos en una variable cuantos numeros de fotos mostraremos en la lista de fotos
			//seleccionable. Como mucho seran 12, pero si no se han subido tantas fotos ser�n el total de fotos.
			// Martin 05/05/09. Ampliamos el numero maximo de fotos a 16.
			$lNumElementosListaFotos = min($lNumFotosHab,1);
			for ($li=0;$li<$lNumFotosHab;$li++)
			{
				// Martin 14/04/08. Metemos todos los identificadores de fotos del establecimiento en un array.
				$lIdimagen = mysql_result($rsFotosHab,$li,"idimagen");
				$laListaFotosHab[$li] = $lIdimagen;
				if ($lIdimagen == $_SESSION["idimagen_hab_actual"])
				{
					// Martin 14/04/08. Guardamos la posicion que ocupa en el array la foto actual en la variable de 
					//sesion lista_fotos_hab_actual.
					$_SESSION["lista_fotos_hab_actual"] = $li;
				}
			}
			// Martin 14/04/08. En las variables de sesion lista_fotos_hab_primera y lista_fotos_hab_ultima guardamos 
			//el primer y ultimo indice del array que mostraremos en la barra de fotos (por ejemplo del 3 al 14, ya que 
			//solo mostraremos como mucho 12 numeros de fotos para elegir).
			if ($_SESSION["lista_fotos_hab_primera"] == "")
			{
				$_SESSION["lista_fotos_hab_primera"] = 0;
				$_SESSION["lista_fotos_hab_ultima"] = $lNumElementosListaFotos - 1;
			}
			if ($_SESSION["lista_fotos_hab_actual"] > $_SESSION["lista_fotos_hab_ultima"])
			{
				$_SESSION["lista_fotos_hab_primera"]++;
				$_SESSION["lista_fotos_hab_ultima"] = $_SESSION["lista_fotos_hab_actual"];
			}
			if ($_SESSION["lista_fotos_hab_actual"] < $_SESSION["lista_fotos_hab_primera"])
			{
				$_SESSION["lista_fotos_hab_primera"] = $_SESSION["lista_fotos_hab_actual"];
				$_SESSION["lista_fotos_hab_ultima"]--;
			} //fin if ($_SESSION["lista_fotos_hab_primera"] == "")...
			// Martin 14/04/08. En las variables $lNumImagenAnterior y $lNumImagenSiguiente guardamos los indices del array 
			//de fotos anterior y posterior al indice actual. Se asignaran a los botones de navegacion que hay en los 
			//extremos de la barra de seleccion de  fotos.
			if ($_SESSION["lista_fotos_hab_actual"] > 0)
			{
				$lNumImagenAnterior = $_SESSION["lista_fotos_hab_actual"] - 1;
			}
			else
			{
				$lNumImagenAnterior = 0;
			} //fin if ($_SESSION["lista_fotos_hab_actual"] > 0)...
			if ($_SESSION["lista_fotos_hab_actual"] < ($lNumFotosHab - 1))
			{
				$lNumImagenSiguiente = $_SESSION["lista_fotos_hab_actual"] + 1;
			}
			else
			{
				$lNumImagenSiguiente = $lNumFotosHab - 1;
			} //if ($_SESSION["lista_fotos_hab_actual"] < $lNumFotosHab)...
			// Martin 14/04/08. Las variables $lParametros y $lParametrosSID se establece en el fichero /inc_comun.php
	?>
						<div class="numero_foto">
							<a href="lashabitaciones.php<?php print($lParametros."&idimg_hab=".$laListaFotosHab[$lNumImagenAnterior].$lParametrosSID); ?>"> &lt; </a>
						</div>
	<?php
			for ($lj=$_SESSION["lista_fotos_hab_primera"];$lj<=$_SESSION["lista_fotos_hab_ultima"];$lj++)
			{
				$lNumfoto = $lj + 1;
				$lIdimagen = $laListaFotosHab[$lj];
				if ($lj != $_SESSION["lista_fotos_hab_primera"]) {
	?>
						<div class="separacion"> | </div>
	<?php
				}
	//print("SID=".SID);
	?>
						<div class="<?php if ($lIdimagen == $_SESSION["idimagen_hab_actual"]) { ?>foto_seleccionada<?php } else { ?>numero_foto<?php } ?>">
							<a href="lashabitaciones.php<?php print($lParametros."&idimg_hab=".$lIdimagen.$lParametrosSID); ?>"> <?php print($lNumfoto); ?> </a>
						</div>
	<?php
			} //fin for ($lj=$_SESSION["lista_fotos_est_primera"];$lj<=$_SESSION["lista_fotos_est_ultima"];$lj++)...
	?>
						<div class="numero_foto">
							<a href="lashabitaciones.php<?php print($lParametros."&idimg_hab=".$laListaFotosHab[$lNumImagenSiguiente].$lParametrosSID); ?>"> &gt; </a>
						</div>
	<?php
		} //fin if ($lNumFotosEst > 0)...
	?>
					</div>
					<!--<div class="span-12 texto">-->
					<div class="texto">
	<?php
		$lCadena = "SELECT idtipo, nombre, descripcion".
							", mejor_caracteristica".
							" FROM ".__TABLA_HABITACIONES_TIPOS__.
							" WHERE idestablecimiento = ".$lIdEstablecimiento.
							" AND ididioma = ".$lIdIdioma.
							" AND visible = 1".
							" AND borrado = 0".
							" ORDER BY nombre";
//print("lCadena=".$lCadena."<br />");
		$rsHabTipos = fQuery($lCadena);
		$lNumHabTipos = mysql_num_rows($rsHabTipos);
		if ($lNumHabTipos > 0)
		{
	?>
						<h5> <?php print(ucfirst(mb_strtolower(fLiteral(350,$lIdIdioma)))); //LAS HABITACIONES ?> </h5>
						<p>
	<?php //print("<br />"."Prueba de llamada a fLiteral_prueba:".fLiteral_prueba(1,1)."<br />"); ?>
	<?php print(fLiteral(392,$lIdIdioma)); //Contamos con los siguientes tipos de habitaci�n ?>:
						</p>
	<?php
			for ($li=0;$li<$lNumHabTipos;$li++)
			{
				$lIdTipoHab = mysql_result($rsHabTipos,$li,"idtipo");
				$lNombreTipoHab = mysql_result($rsHabTipos,$li,"nombre");
				$lDescTipoHab = mysql_result($rsHabTipos,$li,"descripcion");
//print("lDescTipoHab=".$lDescTipoHab."<br />");
	?>
						<h5> <?php print(ucfirst(mb_strtolower($lNombreTipoHab))) ?> </h5>
	<?php
				if ($lDescTipoHab != "")
				{
	?>
						<p>
							<?php print(fLiteral(393,$lIdIdioma)); //Descripcion ?>: <br />
							<?php print($lDescTipoHab); ?>
						</p>
	<?php
				}
				// Martin 16/04/08. Seleccionamos las caracteristicas de la habitacion en curso
				$lCadena_carac = "SELECT car.descripcion AS car_descripcion".
								", car_tipo.descripcion  AS car_tipo_descripcion".
								" FROM ".__TABLA_CARACTERISTICAS__." AS car".
								", ".__TABLA_CARACTERISTICAS_TIPOS__." AS car_tipo".
								", ".__TABLA_HABITACIONES_CARACTERISTICAS__." AS hab_car".
								" WHERE car.idtipo = car_tipo.idtipo".
								" AND car.idcaracteristica = hab_car.idcaracteristica".
								" AND car_tipo.ididioma = ".$lIdIdioma.
								" AND car_tipo.borrado = 0".
								" AND car.ididioma = ".$lIdIdioma.
								" AND car.borrado = 0".
								" AND hab_car.idtipo = '".fLimpiar_sql($lIdTipoHab)."'".
								" ORDER BY car_tipo.idtipo, car.descripcion";
				$rsCaracteristicas = fQuery($lCadena_carac);
				$lNumCaracteristicas = mysql_num_rows($rsCaracteristicas);
				if ($lNumCaracteristicas == 0)
				{
					// Martin 16/04/08. Seleccionamos las caracteristicas de la habitacion en curso en espanol
					//(por si no estan traducidas al idioma actual).
					$lCadena_carac = "SELECT car.descripcion AS car_descripcion".
								", car_tipo.descripcion  AS car_tipo_descripcion".
								" FROM ".__TABLA_CARACTERISTICAS__." AS car".
								", ".__TABLA_CARACTERISTICAS_TIPOS__." AS car_tipo".
								", ".__TABLA_HABITACIONES_CARACTERISTICAS__." AS hab_car".
								" WHERE car.idtipo = car_tipo.idtipo".
								" AND car.idcaracteristica = hab_car.idcaracteristica".
								" AND car_tipo.ididioma = 1".
								" AND car_tipo.borrado = 0".
								" AND car.ididioma = 1".
								" AND car.borrado = 0".
								" AND hab_car.idtipo = '".fLimpiar_sql($lIdTipoHab)."'".
								" ORDER BY car_tipo.idtipo, car.descripcion";
					$rsCaracteristicas = fQuery($lCadena_carac);
				}
				$lNumCaracteristicas = mysql_num_rows($rsCaracteristicas);
				if ($lNumCaracteristicas > 0)
				{
	?>
						<p>
							<?php print(fLiteral(394,$lIdIdioma)); //Caracter�sticas ?>: <br />
	<?php
					for ($lj=0;$lj<$lNumCaracteristicas;$lj++)
					{
						$lCar_descripcion = mysql_result($rsCaracteristicas,$lj,"car_descripcion");
						if ($lj>0)
						{	print(", "); }
						print($lCar_descripcion);
					} //fin for ($lj=0;$lj<$lNumCaracteristicas;$lj++)...
	?>
						</p>
	<?php
				} //fin if ($lNumCaracteristicas > 0)...
	?>
	<?php
			} //fin for ($li=0;$li<$lNumHabTipos;$li++)...
			// Martin 16/04/08. Seleccionamos los servicios opcionales del hotel.
			$lCadena = "SELECT adi_trad.nombre, adi_trad.descripcion".
						" FROM ".__TABLA_ADICIONALES_HABITACIONES__." AS adi_hab".
						", ".__TABLA_ADICIONALES_TRADUCCION__." AS adi_trad".
						" WHERE adi_hab.idservicio = adi_trad.idservicio".
						" AND adi_hab.idtipo = -1".
						" AND adi_hab.idestablecimiento = ".$lIdEstablecimiento.
						" AND adi_trad.ididioma = ".$lIdIdioma.
						" ORDER BY adi_trad.nombre";
			$rsServAdicionales = fQuery($lCadena);
			$lNumServAdicionales = mysql_num_rows($rsServAdicionales);
			if ($lNumServAdicionales > 0)
			{
	?>
						<h5> <?php print(ucfirst(mb_strtolower(fLiteral(395,$lIdIdioma)))); //SERVICIOS OPCIONALES ?> </h5>
	<?php
				for ($li=0;$li<$lNumServAdicionales;$li++)
				{
					$lNombreServAdicional = mysql_result($rsServAdicionales,$li,"nombre");
					$lDescServAdicional = mysql_result($rsServAdicionales,$li,"descripcion");
	?>
						<h5> <?php print(ucfirst(mb_strtolower($lNombreServAdicional))); ?> </h5>
						<p>
							<?php print($lDescServAdicional); ?>
						</p>
	<?php
				}
			}
		} //fin if ($lNumHabTipos > 0)...
	?>
					</div>
				</div>
			</div>
			<?php include("include/modulos/inc_instalaciones_servicios.php"); ?>
			<?php include("include/modulos/inc_pie.php"); ?>
		</div>
	</div>
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
