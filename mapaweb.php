<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
<script src="include/js/funciones.js"></script>
<title><?php print("Mapa Web".". El Secreto de Ollo hotel rural en navarra cerca pamplona hoteles casas rurales agroturismos dormir alojamiento dormir con encanto bookings hotels near iruña"); ?></title>
</head>

<body class="top" onload="igualaColumnas();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<!--<div class="span-12 border" id="cuerpo">-->
				<div class="span-13" id="cuerpo">
					<!--<div class="span-12" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower("MAPA WEB"))); //MAPA WEB ?>
					</div>
					<!--<div class="span-12 texto">-->
					<div class="texto">
						<div class="mapaweb_linea">
							<a class="mapaweb_enlace" href="index_prueba.php"> 
								<?php /*<?php print(ucfirst(mb_strtolower(fLiteral(349,$lIdIdioma)))); //PRESENTACIÓN ?>*/ ?>
								<?php print(fLiteral(357,$lIdIdioma)); //Presentación ?>
							</a>
						</div>
						<div class="mapaweb_linea">
							<a class="mapaweb_enlace" href="lashabitaciones.php"> 
								<?php print(ucfirst(mb_strtolower(fLiteral(350,$lIdIdioma)))); //LAS HABITACIONES ?>
							</a>
						</div>
						<div class="mapaweb_linea">
							<a class="mapaweb_enlace" href="localizacion.php"> 
								<?php /*<?php print(ucfirst(mb_strtolower(fLiteral(351,$lIdIdioma)))); //LOCALIZACIÓN ?>*/ ?>
								<?php print(fLiteral(351,$lIdIdioma)); //Ubicación ?>
							</a>
						</div>
						<div class="mapaweb_linea">
							<a class="mapaweb_enlace" href="elentorno.php"> 
								<?php print(ucfirst(mb_strtolower(fLiteral(352,$lIdIdioma)))); //EL ENTORNO ?>
							</a>
						</div>
<?php //Martin 15/10/09. No se mostrara a partir de ahora la pagina de disponibilidad. ?>
<!--
						<div class="mapaweb_linea">
							<a class="mapaweb_enlace" href="disponibilidad.php"> 
								<?php //print(ucfirst(mb_strtolower(fLiteral(353,$lIdIdioma)))); //DISPONIBILIDAD ?>
							</a>
						</div>
-->
						<div class="mapaweb_linea">
							<a class="mapaweb_enlace" href="tarifasyofertas.php"> 
								<?php print(ucfirst(mb_strtolower(fLiteral(354,$lIdIdioma)))); //TARIFAS Y OFERTAS ?>
							</a>
						</div>
						<div class="mapaweb_linea">
							<a class="mapaweb_enlace" href="contacto.php"> 
							<?php //print(ucfirst(mb_strtolower(fLiteral(369,$lIdIdioma)))); //FORMULARIO DE CONTACTO ?>
							<?php print(ucfirst(mb_strtolower(fLiteral(355,$lIdIdioma)))); //CONTACTO ?>
							</a>
						</div>
						<div class="mapaweb_linea mapaweb_linea_ultimo">
							<a class="mapaweb_enlace" href="http://central.reservadealojamientos.com/reservas-disponibilidad.php?id=50&i=1"> 
								<?php print(ucfirst(mb_strtolower(fLiteral(356,$lIdIdioma)))); //HACER UNA RESERVA ?>
							</a>
						</div>
					</div>
				</div>
<style type="text/css">
/* Martin 04/05/09. Cambiamos algunas propiedades para que se vea bien la columna de la izquierda */
/*#columna_dcha {
	background-color: #353535;
position: relative;
top: -515px;
*top: -502px;
_top: -507px;
left: 520px;
height: 500px;
}*/
#contenido_2 {
background-color: transparent;
background-image: none;
}
#contenido {
	background-image: url(../images/imagen_fondo_contenido_2.jpg);
	background-repeat: no-repeat;
}
#contenido_adorno {
_bottom: 90px;
}
</style>
				<!--<div class="span-5 append-1 last" id="columna_dcha">-->
				<!--<div class="span-5 last" id="columna_dcha">
					&nbsp;
				</div>-->
				<?php include("include/modulos/inc_pie.php"); ?>
			</div>
		</div>
	</div>
<?php
//print("index.php. \$_SESSION[\"lista_fotos_est_primera\"]=".$_SESSION["lista_fotos_est_primera"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_ultima\"]=".$_SESSION["lista_fotos_est_ultima"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_actual\"]=".$_SESSION["lista_fotos_est_actual"]."<br />");
//print("index.php. \$lNumImagenAnterior=".$lNumImagenAnterior."<br />");
//print("index.php. \$lNumImagenSiguiente=".$lNumImagenSiguiente."<br />");
?>
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
