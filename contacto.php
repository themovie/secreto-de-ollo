<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");


	// Martin 29/04/08. Variables para controlar los errores
	$lEnviado = $_GET["env"];
	$lError = 0;
	if ($lEnviado=="si" &&
			( $_POST["nombre"] == "" ||
				$_POST["email"] == "" ||
				$_POST["telefono"] == "" ||
				$_POST["pais"] == "" ||
				$_POST["observaciones"] == ""
			)
		)
	{
		$lError = 1;
	}//fin if ($lEnviado=="si" &&...
	if ($lError==0 && $lEnviado=="si") 
	{
		$lCadena = "SELECT est.email, est_nom.nombre".
						" FROM ".__TABLA_ESTABLECIMIENTOS__." est".
						", ".__TABLA_ESTABLECIMIENTOS_NOMBRES__." est_nom".
						" WHERE est.idestablecimiento = est_nom.idestablecimiento".
						" AND est.idestablecimiento = ".$lIdEstablecimiento.
						" AND est_nom.ididioma = ".$lIdIdioma;
		$rsEst = fQuery($lCadena);
		if (mysql_num_rows($rsEst) > 0)
		{
			$lEstMail = mysql_result($rsEst,0,"email");
			$lEstNombre = mysql_result($rsEst,0,"nombre");
		}

		$lFecha = time();
		$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
		$lIP2 = $_SERVER["REMOTE_ADDR"];
		$lNavegador = $_SERVER["HTTP_USER_AGENT"];
		$lNombre = $_POST["nombre"];
		$lEMail = $_POST["email"];
		$lTelefono = $_POST["telefono"];
		$lPais = $_POST["pais"];
		$lObservaciones = $_POST["observaciones"];

		$lUltimo = fUltimoID(__TABLA_CONTACTOS_FICHA__,"idcontacto");
		$lUltimo++;

		$lCadena = "INSERT INTO ".__TABLA_CONTACTOS_FICHA__." (".
					"idcontacto, idestablecimiento, fecha, ip1,".
					" ip2, navegador, nombre, email,".
					" telefono, pais, observaciones".
					") VALUES (".
					$lUltimo.",".
					fLimpiar_sql($lIdEstablecimiento).",". //Martin 18/07/08. Incluimos el idestablecimiento.
					$lFecha.",".
					fComillas(fLimpiar_sql($lIP1)).",".
					fComillas(fLimpiar_sql($lIP2)).",".
					fComillas(fLimpiar_sql($lNavegador)).",".
					fComillas(fLimpiar_sql($lNombre)).",".
					fComillas(fLimpiar_sql($lEMail)).",".
					fComillas(fLimpiar_sql($lTelefono)).",".
					fComillas(fLimpiar_sql($lPais)).",".
					fComillas(fLimpiar_sql($lObservaciones)).
					")";
		fQuery($lCadena);

		$lTitulo = fLiteral(365,$lIdIdioma)." ".$lEstNombre;//"Formulario de contacto de ".$lEstNombre

		$lTexto1 = fLiteral(58,$lIdIdioma).", "."<br>\n"; //"Hola, "."<br>\n";
		$lTexto1.= fLiteral(366,$lIdIdioma)."<br>\n"; //"Este es un mensaje autom�tico informando del env�o de un formulario de contacto desde la Web"."<br>\n";
		$lTexto1.= fLiteral(367,$lIdIdioma)."<br><br>\n"; //"La informaci�n introducida es la siguiente:"."<br><br>\n";
		$lTexto1.= fLiteral(0,$lIdIdioma).": ".$lNombre."<br>\n"; //"Nombre: ".$lNombre."<br>\n";
		$lTexto1.= fLiteral(4,$lIdIdioma).": ".$lEMail."<br>\n"; //"EMail: ".$lEMail."<br>\n";
		$lTexto1.= fLiteral(67,$lIdIdioma).": ".$lTelefono."<br>\n"; //"Telefono: ".$lTelefono."<br>\n";
		$lTexto1.= fLiteral(239,$lIdIdioma).": ".$lPais."<br>\n"; //"Pa�s: ".$lPais."<br>\n";
		$lTexto1.= fLiteral(368,$lIdIdioma).": ".str_replace("\n","<br>\n",$lObservaciones)."<br>\n"; //"Observaciones: ".str_replace("\n","<br>\n",$lObservaciones)."<br>\n";

		$lTexto2 = fLiteral(58,$lIdIdioma).", "."\n"; //"Hola, "."\n";
		$lTexto2.= fLiteral(366,$lIdIdioma)."\n"; //"Este es un mensaje autom�tico informando del env�o de un formulario de contacto desde la Web"."\n";
		$lTexto2.= fLiteral(367,$lIdIdioma).":"."\n\n"; //"La informaci�n introducida es la siguiente:"."\n\n";
		$lTexto2.= fLiteral(0,$lIdIdioma).": ".$lNombre."\n"; //"Nombre: ".$lNombre."\n";
		$lTexto2.= fLiteral(4,$lIdIdioma).": ".$lEMail."\n"; //"EMail: ".$lEMail."\n";
		$lTexto2.= fLiteral(67,$lIdIdioma).": ".$lTelefono."\n"; //"Telefono: ".$lTelefono."\n";
		$lTexto2.= fLiteral(239,$lIdIdioma).": ".$lPais."\n"; //"Pa�s: ".$lPais."\n";
		$lTexto2.= fLiteral(368,$lIdIdioma).": ".$lObservaciones."\n"; //"Observaciones: ".$lObservaciones."\n";

		$lRemitente = $lEMail;
		$lDestinatario = $lEstMail;
		// 20111017 MA. comentado el envio a mi cuenta de los contactos.
		//fEnviarMail($lNombre,$lRemitente,"a.martin@themovie.org",$lTitulo, $lTexto1, $lTexto2, "");
		fEnviarMail($lNombre,$lRemitente,$lDestinatario,$lTitulo, $lTexto1, $lTexto2,"");
	}//fin if ($lError==0&&$lEnviado=="si")...
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
<script src="include/js/funciones.js"></script>
<title><?php print("El Secreto de Ollo hotel rural en navarra cerca pamplona navarra agroturismos ".fLiteral(325,$lIdIdioma)); //Contacto ?></title>
</head>

<body class="top" onload="igualaColumnas();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<!--<div class="span-12 border" id="cuerpo">-->
				<div class="span-13" id="cuerpo">
					<!--<div class="span-12" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower(fLiteral(369,$lIdIdioma)))); //FORMULARIO DE CONTACTO ?>
					</div>
					<!--<div class="span-12 texto">-->
					<div class="texto">
	<?php 
					if ($lEnviado == "si" && $lError == 1)
					{
	?>
						<h5><?php print(fLiteral(370,$lIdIdioma)); //ERROR ?></h5>
						<p><?php print(fLiteral(371,$lIdIdioma).": "); //print("Los siguientes campos son incorrectos o estan vacios:"); ?> 
	<?php 
										$lMensajeError = "";
										if ($_POST["nombre"]=="") 
										{
											$lMensajeError .= fLiteral(0,$lIdIdioma); //"Nombre";
										} 
										if ($_POST["email"]=="") 
										{
											if ($lMensajeError != "")
											{ $lMensajeError .= ", "; }
											$lMensajeError .= fLiteral(4,$lIdIdioma); //"Email";
										}
										if($_POST["telefono"]=="") 
										{
											if ($lMensajeError != "")
											{ $lMensajeError .= ", "; }
											$lMensajeError .= fLiteral(67,$lIdIdioma); //"Tel�fono";
										} 
										if($_POST["pais"]=="") 
										{
											if ($lMensajeError != "")
											{ $lMensajeError .= ", "; }
											$lMensajeError .= fLiteral(239,$lIdIdioma); //"Pa�s";
										} 
										if($_POST["observaciones"]=="") 
										{
											if ($lMensajeError != "")
											{ $lMensajeError .= ", "; }
											$lMensajeError .= fLiteral(368,$lIdIdioma); //"Observaciones";
										} 
										print($lMensajeError);
	?>
						</p>
	<?php
					}
					if ($lEnviado == "si" && $lError == 0)
					{
	?>
							<h5><?php print(fLiteral(372,$lIdIdioma)); //FORMULARIO ENVIADO CORRECTAMENTE ?></h5>
							<p>
	<?php
									print(fLiteral(373,$lIdIdioma).". ".
											fLiteral(374,$lIdIdioma));
									//print("El formulario se ha enviado correctamente".". ".
									//		"Gracias por contactar con nosotros");
	?>
							</p>
	<?php
					}
					else
					{
					// Martin 29/04/08. Usamos las variables $lParametros y $lParametrosSID definidas en el 
					//script inc_comun.php.
	?>
						<form id="form_contacto" method="post" action="contacto.php<?php print($lParametros.$lParametrosSID); ?>&env=si">
							<div class="linea_form_contacto">
								<label for="nombre">*<?php print(fLiteral(0,$lIdIdioma)); //Nombre ?> </label>
								<input class="texto" name="nombre" id="nombre" value="<?php print($_POST["nombre"]); ?>" maxlength="50" />
							</div>
							<div class="linea_form_contacto">
								<label for="email">*<?php print(fLiteral(375,$lIdIdioma)); //Correo Electr�nico ?> </label>
								<input class="texto" name="email" id="email" value="<?php print($_POST["email"]); ?>" maxlength="255" />
							</div>
							<div class="linea_form_contacto">
								<label for="telefono">*<?php print(fLiteral(67,$lIdIdioma)); //Tel�fono ?> </label>
								<input class="texto" name="telefono" id="telefono" value="<?php print($_POST["telefono"]); ?>" maxlength="40" />
							</div>
							<div class="linea_form_contacto">
								<label for="pais">*<?php print(fLiteral(239,$lIdIdioma)); //Pa�s ?> </label>
								<input class="texto" name="pais" id="pais" value="<?php print($_POST["pais"]); ?>" maxlength="40" />
							</div>
							<div class="linea_form_contacto">
								<label for="observaciones">*<?php print(fLiteral(368,$lIdIdioma)); //Observaciones ?> </label>
								<textarea id="observaciones" name="observaciones" rows="7" cols="5"><?php print($_POST["observaciones"]); ?></textarea>
							</div>
							<div class="linea_form_contacto">
									<input id="form_contacto_bot_enviar" type="submit" value="Enviar" />
							</div>
							<div class="linea_form_contacto">
									<span id="form_contacto_nota"> *<?php print(fLiteral(376,$lIdIdioma)); //Campos obligatorios ?> &nbsp; </span>							
							</div>
						</form>
	<?php
					}//fin //fin if($lEnviado=="si"&&$lError==0)...
	?>
					</div>
				</div>
<style type="text/css">
/* Martin 04/05/09. Cambiamos algunas propiedades para que se vea bien la columna de la izquierda */
#/*columna_dcha {
	background-color: #353535;
position: relative;
top: -515px;
*top: -502px;
_top: -507px;
left: 520px;
height: 500px;
}*/
#contenido_2 {
background-color: transparent;
background-image: none;
}
#contenido {
	background-image: url(../images/imagen_fondo_contenido_2.jpg);
	background-repeat: no-repeat;
}
#contenido_adorno {
_bottom: 90px;
}
</style>
				<!--<div class="span-5 append-1 last" id="columna_dcha">-->
				<!--<div class="span-5 last" id="columna_dcha">
				&nbsp;
				</div>-->
				<?php include("include/modulos/inc_pie.php"); ?>
			</div>
		</div>
	</div>
<?php
//print("index.php. \$_SESSION[\"lista_fotos_est_primera\"]=".$_SESSION["lista_fotos_est_primera"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_ultima\"]=".$_SESSION["lista_fotos_est_ultima"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_actual\"]=".$_SESSION["lista_fotos_est_actual"]."<br />");
//print("index.php. \$lNumImagenAnterior=".$lNumImagenAnterior."<br />");
//print("index.php. \$lNumImagenSiguiente=".$lNumImagenSiguiente."<br />");
?>
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
