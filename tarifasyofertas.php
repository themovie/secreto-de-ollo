<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
<script src="include/js/funciones.js"></script>
<title><?php print("hoteles casas rurales en navarra cerca de pamplona dormir alojamiento ".fLiteral(405,$lIdIdioma)); //Tarifas y ofertas ?></title>
</head>

<body class="top" onload="igualaColumnas3();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<!--<div class="span-12 border" id="cuerpo">-->
				<div class="span-13" id="cuerpo">
					<!--<div class="span-12" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower(fLiteral(354,$lIdIdioma)))); //TARIFAS Y OFERTAS ?>
					</div>
					<!--<div class="span-12 calendario">-->
					<div class="span-12 last calendario">
						<?php include("include/modulos/inc_calendario_tarifasyofertas.php"); ?>
					</div>
				</div>
			</div>
			<!--<div class="span-5 append-1 last" id="columna_dcha">-->
			<div class="span-5 last" id="columna_dcha">
				<?php include("include/modulos/inc_boton_megusta_facebook_col_dcha.php"); ?>
<?php
		// Martin 28/04/08. Seleccionamos la fecha actual para seleccionar solo las ofertas que esten disponibles despues
		//de la fecha del actual.
		$lFechaActual = time();
		// Martin 28/04/08. Seleccionamos las ofertas disponibles en el establecimiento.
		//Usamos las variables $lIdEstablecimiento y $lIdIdioma, que se definen en el script inc_comun.php.
		$lCadena = "SELECT ofe_fec.idoferta".
						", MAX(ofe_trad.nombre) nombre".
						", MAX(ofe_trad.descripcion) descripcion".
						", MAX(ofe.precio) precio".
						" FROM ".__TABLA_OFERTAS__." ofe".
						", ".__TABLA_OFERTAS_TRADUCCIONES__." ofe_trad".
						", ".__TABLA_OFERTAS_FECHAS__." ofe_fec".
						" WHERE ofe.idoferta = ofe_trad.idoferta".
						" AND ofe.idoferta = ofe_fec.idoferta".
						" AND ofe.idestablecimiento = ".$lIdEstablecimiento.
						" AND ofe_trad.ididioma = ".$lIdIdioma.
						" AND ofe_fec.fecha > ".$lFechaActual.
						" AND ofe.visible = 1".
						" AND ofe.borrado = 0".
						" GROUP BY ofe_fec.idoferta";
//print("Select ofertas:"."<br />");
//print($lCadena."<br />");
		$rsOfertas = fQuery($lCadena);
		$lNumOfertas = mysql_num_rows($rsOfertas);
		for ($li=0;$li<$lNumOfertas;$li++)
		{
			$lIdOfertaAct = mysql_result($rsOfertas,$li,"idoferta");
			$lNombreOfertaAct = mysql_result($rsOfertas,$li,"nombre");
			$lDescripcionOfertaAct = mysql_result($rsOfertas,$li,"descripcion");
			$lPrecioOfertaAct = mysql_result($rsOfertas,$li,"precio");
			if ($li == 0)
			{
?>
				<!--<h5> <?php //print(fLiteral(406,$lIdIdioma)); //OFERTAS ?> </h5>-->
				<h4> <?php print(ucfirst(mb_strtolower(fLiteral(406,$lIdIdioma)))); //OFERTAS ?> </h4>
<?php
			}
?>
			<!--<h6> <?php //print($lNombreOfertaAct); ?> </h6>-->
			<h5 class="titulo_condiciones"> <?php print($lNombreOfertaAct); ?> </h5>
				<p class="agrupacion_elementos">
				<?php print($lDescripcionOfertaAct); ?> <br />
				<span id="oferta_precio"><?php print(fLiteral(411,$lIdIdioma)); //Precio de la oferta ?>: <?php print($lPrecioOfertaAct); ?> &euro;</span>
				</p>
<?php
		}
?>
			</div>
			<?php include("include/modulos/inc_pie.php"); ?>
		</div>
	</div>
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
