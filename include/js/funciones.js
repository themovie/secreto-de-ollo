function igualaColumnas()
{
	var cuerpo;
	var barra;
	var cuerpoH;
	var barraH;
	cuerpo = document.getElementById("cuerpo");
	cuerpoH = cuerpo.offsetHeight;
	barra = document.getElementById("columna_izq");
	barraH = barra.offsetHeight;
	if(cuerpoH < barraH)
	{
		cuerpo.style.height = barraH + 'px';
	}
	else if ( cuerpoH > barraH)
	{
		barra.style.height = cuerpoH + 'px';
	}
}

function igualaColumnas3()
{
	var cuerpo;
	var columna_izq;
	var columna_dcha;
	var cuerpoH;
	var columna_izqH;
	var columna_dchaH;
	var columna_izq_margin = 56; /* Margin de la columna izquierda */
	cuerpo = document.getElementById("cuerpo");
	cuerpoH = cuerpo.offsetHeight;
	columna_izq = document.getElementById("columna_izq");
	columna_izqH = columna_izq.offsetHeight;
	columna_dcha = document.getElementById("columna_dcha");
	columna_dchaH = columna_dcha.offsetHeight;
/*alert('columna_izqH='+columna_izqH+'; cuerpoH='+cuerpoH+'; columna_dchaH='+columna_dchaH);*/
	if (columna_izqH >= cuerpoH && columna_izqH >= columna_dchaH) {
		cuerpo.style.height = columna_izqH + 'px';
		columna_dcha.style.height = columna_izqH + 'px';
	}
	else if (cuerpoH >= columna_izqH && cuerpoH >= columna_dchaH) {
		columna_izq.style.height = (cuerpoH - columna_izq_margin) + 'px';
		columna_dcha.style.height = cuerpoH + 'px';
	}
	else if (columna_dchaH >= cuerpoH && columna_dchaH >= columna_izqH) {
		cuerpo.style.height = columna_dchaH + 'px';
		columna_izq.style.height = (columna_dchaH - columna_izq_margin) + 'px';
	}
}
