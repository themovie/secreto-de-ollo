<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");

	// Martin 06/05/08. Recogemos el campo "llegar desde"
	$lLocalidad_llegardesde = $_POST["localidad"];
//print("lLocalidad_llegardesde=".$lLocalidad_llegardesde."<br />");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
<script src="http://maps.google.es/maps?file=api&amp;v=2&amp;key=ABQIAAAAryNx42SihOy3ILkJOJTJxhSIH3o957nl7PMeZduMu5wYl08VARTJXkECQBMumJWK8l83qLx0Nw2ESw" type="text/javascript" charset="utf-8"></script>
<script src="include/js/funciones.js"></script>
<title><?php print("El Secreto de Ollo hotel rural en navarra cerca pamplona navarra agroturismos ".fLiteral(396,$lIdIdioma)); //Localizaci�n ?></title>
</head>

<body class="top" onload="cargar_googlemaps(); igualaColumnas3();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<!--<div class="span-12 border" id="cuerpo">-->
				<div class="span-13" id="cuerpo">
					<!--<div class="span-12" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower(fLiteral(351,$lIdIdioma)))); //LOCALIZACI�N ?>
					</div>
	<?php
					// Martin 16/04/08. Seleccionamos los datos de localizacion.
					//Las variables $lIdEstablecimiento y $lIdIdioma se establecen en el script inc_comun.php.
					// Martin 29/04/09. Seleccionamos los datos de direccion tambien.
					$lCadena = "SELECT llegar,".
									" lat, lon,".
									" lat_direccion, lon_direccion,".
									" usar_coordenadas, ".
									" calle, numero, cp, localidad, provincia, pais".
									" FROM ".__TABLA_LOCALIZACION__.
									" WHERE idestablecimiento = ".$lIdEstablecimiento.
									" AND ididioma = ".$lIdIdioma;
					$rsLocalizacion = fQuery($lCadena);
					$lNumLocalizacion = mysql_num_rows($rsLocalizacion);
					if ($lNumLocalizacion > 0)
					{
						$lLoc_llegar = mysql_result($rsLocalizacion,0,"llegar");
						$lLoc_usar_coordenadas = mysql_result($rsLocalizacion,0,"usar_coordenadas");
						if ($lLoc_usar_coordenadas == "1")
						{
							$lLoc_latitud = mysql_result($rsLocalizacion,0,"lat");
							$lLoc_longitud = mysql_result($rsLocalizacion,0,"lon");
						}
						else
						{
							$lLoc_latitud = mysql_result($rsLocalizacion,0,"lat_direccion");
							$lLoc_longitud = mysql_result($rsLocalizacion,0,"lon_direccion");
						}
						// Martin 29/04/09. Recuperamos los datos de direccion de la tabla de localizacion.
						$lLoc_calle = mysql_result($rsLocalizacion,0,"calle");
						$lLoc_numero = mysql_result($rsLocalizacion,0,"numero");
						$lLoc_cp = mysql_result($rsLocalizacion,0,"cp");
						$lLoc_localidad = mysql_result($rsLocalizacion,0,"localidad");
						$lLoc_provincia = mysql_result($rsLocalizacion,0,"provincia");
						$lLoc_pais = mysql_result($rsLocalizacion,0,"pais");
					}
	?>
					<!--<div class="span-12" id="map">-->
					<div id="map">
					</div>
					<div id="barra_seleccion_fotos">
						&nbsp;
					</div>
					<!--<div class="span-12 texto">-->
					<div class="texto">
	<?php
					// Martin 16/04/08. Seleccionamos los datos de direccion del establecimiento. 
					//La variable $lIdEstablecimiento se establece en inc_comun.php.
					// Martin 29/04/09. Seleccionamos el telefono y e-mail
					$lCadena = "SELECT est.calle est_calle, est.numero est_numero, est.cp est_cp".
								", est.nombre_poblacion est_nombre_poblacion, est.nombre_provincia est_nombre_provincia".
								", est.idpais est_idpais".
								", est.telefono est_telefono, est.email est_email".
								" FROM ".__TABLA_ESTABLECIMIENTOS__." AS est".
								" WHERE idestablecimiento = ".$lIdEstablecimiento.
								" AND est.borrado = 0";
					$rsEstDireccion = fQuery($lCadena);
					$lNumEstDireccion = mysql_num_rows($rsEstDireccion);
					if ($lNumEstDireccion > 0)
					{
						$lEst_calle = mysql_result($rsEstDireccion,0,"est_calle");
						$lEst_numero = mysql_result($rsEstDireccion,0,"est_numero");
						$lEst_cp = mysql_result($rsEstDireccion,0,"est_cp");
						$lEst_nombre_poblacion = mysql_result($rsEstDireccion,0,"est_nombre_poblacion");
						$lEst_nombre_provincia = mysql_result($rsEstDireccion,0,"est_nombre_provincia");
						$lEst_idpais = mysql_result($rsEstDireccion,0,"est_idpais");
						// Martin 29/04/09. Seleccionamos los datos de telefono y email.
						$lEst_telefono = mysql_result($rsEstDireccion,0,"est_telefono");
						$lEst_email = mysql_result($rsEstDireccion,0,"est_email");
						if ($lEst_idpais != "")
						{
							//Martin 16/04/08. Seleccionamos el nombre del pais del establecimiento. 
							//La variable $lIdIdioma se establece en el script inc_comun.php.
							$lCadena = "SELECT pai.descripcion".
											" FROM ".__TABLA_PAISES__." AS pai".
											" WHERE pai.idpais = '".fLimpiar_sql($lEst_idpais)."'".
											" AND pai.ididioma = ".$lIdIdioma.
											" AND pai.borrado = 0";
							$rsNombrePais = fQuery($lCadena);
							if (mysql_num_rows($rsNombrePais) > 0)
							{
								$lNombrePais = mysql_result($rsNombrePais,0,"descripcion");
							}
						}
	?>
	<?php 
					if ($lLocalidad_llegardesde != "")
					{
	?>
						<h5> <?php print(ucfirst(mb_strtolower(fLiteral(400,$lIdIdioma)))); //RUTA RECOMENDADA POR GOOGLE MAPS ?> </h5>
						<div id="ruta_detalle">
						</div>
	<?php				
					} 
	?>
						<?php /*<h5> <?php print(ucfirst(mb_strtolower(fLiteral(397,$lIdIdioma)))); //DIRECCI�N ?> </h5>*/ ?>
						<h5> <?php print(fLiteral(69,$lIdIdioma)); //Direcci�n ?> </h5>
						<p>
							<?php print($lLoc_calle.", ".$lLoc_numero); //print($lEst_calle.", ".$lEst_numero); ?> <br />
							<?php print($lLoc_cp." ".$lLoc_localidad." (".$lLoc_provincia.")"); //print($lEst_cp." ".$lEst_nombre_poblacion." (".$lEst_nombre_provincia.")"); ?> <br />
							<?php print($lLoc_pais); //print($lNombrePais); ?> <br />
							<?php //print(fLiteral(326,$lIdIdioma).": "); //Tel ?><?php print($lEst_telefono); ?>
						</p>
	<?php
					}
					if ($lNumLocalizacion > 0)
					{
	?>
						<h5> <?php print(ucfirst(mb_strtolower(fLiteral(399,$lIdIdioma)))); //COORDENADAS ?> </h5>
						<p>
						<?php print(fLiteral(409,$lIdIdioma)); //Latitud ?>: <?php print($lLoc_latitud); ?> &nbsp; <?php print(fLiteral(410,$lIdIdioma)); //Longitud ?>: <?php print($lLoc_longitud); ?>
						</p>
						<?php /*<h5> <?php print(ucfirst(mb_strtolower(fLiteral(398,$lIdIdioma)))); //C�MO LLEGAR ?> </h5>*/?>
						<h5> <?php print(fLiteral(471,$lIdIdioma)); //C�mo Llegar ?> </h5>
						<p>
						<?php print(nl2br($lLoc_llegar)); ?>
						</p>
	<?php
					}
	?>					
					</div>
				</div>
			</div>
			<!--<div class="span-5 append-1 last" id="columna_dcha">-->
			<div class="span-5 last" id="columna_dcha">
				<?php include("include/modulos/inc_boton_megusta_facebook_col_dcha.php"); ?>
				<h4> <?php print(ucfirst(mb_strtolower(fLiteral(401,$lIdIdioma)))); //LLEGAR DESDE ?> </h4>
				<h5>Rellenar</h5>
					<!--<p>-->
					<p id="llegardesde_texto">
<?php print(fLiteral(402,$lIdIdioma)); //Introduzca en el siguiente campo su LOCALIDAD o CODIGO POSTAL de partida para conocer el itinerario a seguir. ?>
					</p>
					<form name="llegardesde" id="llegardesde" action="localizacion.php<?php print($lParametros.$lParametrosSID); ?>" method="post">
						<input name="localidad" id="localidad" type="text" value="<?php print($lLocalidad_llegardesde); ?>" />
						<input name="boton_ok" id="boton_ok" type="submit" value="ok" />
					</form>
					<!--</p>-->
				</div>
				<?php include("include/modulos/inc_pie.php"); ?>
			</div>
			<?php //include("include/modulos/inc_pie.php"); ?>
		</div>
	<!--</div>-->
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
<script type="text/javascript">
//<![CDATA[

//	Martin 07/05/08. La variable ruta la usaremos para obtener el objeto GDirections de la API 
//de googlemaps para obtener la ruta desde la localidad del usuario hasta el hotel.
var ruta = '';
function comprobar_query_ruta()
{
	//	Martin 07/05/08. Comprobamos si se ha podido obtener una ruta para llegar al hotel.
	//Si no ha podido obtenerse la ruta (el codigo devuelto sera distinto de 200) se saca un 
	//mensaje de error.
	//	El estado de la ruta nada mas hacer la consulta siempre era 500, asi que esta funcion es 
	//llamada unos segundos despues (usando un temporizador) para tener un resultado mas fiable.
//alert('ruta.getStatus()[code]='+ruta.getStatus()['code']);
	var ruta_detalle = document.getElementById("ruta_detalle");
	var mensaje_error = ruta_detalle.firstChild;
	//if (ruta_detalle.childNodes.length == 1)
	if (ruta.getStatus()['code'] != 200)
	{
		var texto_error = document.createTextNode('<?php print(fLiteral(403,$lIdIdioma)); //No se ha podido encontrar una ruta. ?>');
		ruta_detalle.appendChild(texto_error);
	}
	igualaColumnas();
}

function cargar_googlemaps()
{	
	//	Martin 07/05/08. En esta funcion cargamos el mapa de googlemaps. Si el usuario no ha metido nada 
	//en el formulario llegardesde se saca el mapa con un punto que indica la localizacion del hotel. Si 
	//el usuario ha indicado una localidad o un Codigo Postal se sacara en el mapa la ruta para llegar 
	//al hotel y el punto que se�ala la situacion del hotel.
	if (GBrowserIsCompatible())
	{
		var markers = [];

		var map = new GMap2(document.getElementById("map"));
		var control_mapa = new GSmallMapControl();
		map.addControl(control_mapa);
		map.addControl(new GMapTypeControl());
		//var nivel_zoom = 15;
		var nivel_zoom = 10;
		// Martin 06/05/08. Inicializamos la variable query_llegar_desde que usaremos para obtener 
		//la ruta hasta el hotel.
		var llegar_desde = '';
		var query_llegar_desde = '';
		//var ruta = '';
		var ruta_detalle = '';
<?php
		if ($lLocalidad_llegardesde != "")
		{
?>
			//	Martin 06/05/08. Obtenemos el valor del campo localidad de la seccion "llegar desde". Si no se ha 
			//rellenado la seccion estara vacio.
			llegar_desde = '<?php print($lLocalidad_llegardesde); ?>';
			query_llegar_desde = 'from: '+llegar_desde+' to: ';
			ruta_detalle = document.getElementById("ruta_detalle");
			ruta = new GDirections(map,ruta_detalle);
<?php
		}
?>
			var coor_lat = <?php print($lLoc_latitud); ?>;
			var coor_lon = <?php print($lLoc_longitud); ?>;
			map.setCenter(new GLatLng(coor_lat, coor_lon), nivel_zoom);
			if (llegar_desde != '')
			{
				query_llegar_desde += coor_lat+','+coor_lon;
				//ruta.load(query_llegar_desde,{preserveViewport:true});
				ruta.load(query_llegar_desde);
				// Martin 06/05/08. Movemos un poco el punto que marca el hotel para que no se superponga
				//con el punto de la ruta.
				var point = new GPoint((coor_lon + 0.0005), (coor_lat + 0.0005));
				// Martin 07/05/08. Llamamos a la funcion comprobar_query_ruta() con un retardo de 3 segundos 
				//para asegurarnos de que el codigo de estado devuelto por la ruta sea correcto.
				window.setTimeout('comprobar_query_ruta()',3000);
				//id_setinterval = window.setInterval('comprobar_query_ruta()',500);
			} //fin if (llegar_desde != '')...
			if (!point)
			{	// Si no hemos creado el punto dentro del if (llegar_desde != '')... lo creamos aqui.
				var point = new GPoint(coor_lon, coor_lat);
			}
			var marker = new GMarker(point);
			// Add the marker to map
			map.addOverlay(marker);
			// Add address information to marker
			// Martin 05/05/08. Sacamos en un globo que se desplegara al clikar la direccion del hotel. 
			//Las variables que usamos se definen mas arriba.
			// Martin 05/05/09. Usamos los datos de la tabla de localizacion.
//			GEvent.addListener(marker,"click",
//				function(){ marker.openInfoWindowHtml("<?php //print($lEst_calle.", ".$lEst_numero); ?><br /><?php //print($lEst_cp." ".$lEst_nombre_poblacion." (".$lEst_nombre_provincia.")"); ?><br /><?php //print($lNombrePais); ?>"); });
			GEvent.addListener(marker,"click",
				function(){ marker.openInfoWindowHtml("<div style=\"color: #000000; text-align: left;\"><?php print($lLoc_calle.", ".$lLoc_numero); ?><br /><?php print($lLoc_cp." ".$lLoc_localidad." (".$lLoc_provincia.")"); ?><br /><?php print($lLoc_pais); ?><br /><?php print($lEst_telefono); ?></div>"); });

		//map.setMapType(G_SATELLITE_MAP);
		map.setMapType(G_NORMAL_MAP);
<?php
//if ($lLocalidad_llegardesde != "")
//{
?>
//alert('ruta_detalle.childNodes.length='+ruta_detalle.childNodes.length);
<?php
//}
?>
	}
	else 
	{
		// display a warning if the browser was not compatible
  	alert("<?php print(fLiteral(404,$lIdIdioma)); //Lo siento, el API de Google Maps no es compatible con tu navegador ?>");
	} //fin if (GBrowserIsCompatible())...
} //fin function cargar_googlemaps()...

// This Javascript is based on code provided by the
// Blackpool Community Church Javascript Team
// http://www.commchurch.freeserve.co.uk/   
// http://www.econym.demon.co.uk/googlemaps/

//]]>
</script>
