<?php
	// Martin 10/04/08, Obtenemos datos establecimiento. La variable $lIdEstablecimiento se 
	//establece en inc_comun.
	$lCadena = "SELECT *".
				" FROM ".__TABLA_ESTABLECIMIENTOS__.
				" WHERE idestablecimiento = ".$lIdEstablecimiento.
				" AND borrado = 0";
	$rsEstablecimiento = fQuery($lCadena); 
	if (mysql_num_rows($rsEstablecimiento) > 0)
	{
		$lEstId = mysql_result($rsEstablecimiento,0,"idestablecimiento");
		$lCadena = "SELECT *".
					" FROM ".__TABLA_ESTABLECIMIENTOS_NOMBRES__.
					" WHERE idestablecimiento = ".$lEstId.
					" AND ididioma = ".$lIdIdioma;
		$rsEstNombre = fQuery($lCadena);
		if (mysql_num_rows($rsEstNombre)>0)
		{	$lEstNombre = mysql_result($rsEstNombre,0,"nombre"); }
		else
		{	$lEstNombre = ""; }
		$lEstFichero_logotipo = mysql_result($rsEstablecimiento,0,"fichero_logotipo");
		// Martin 10/04/08. Establecemos la ruta completa hacia el logotipo.
		$lEstRuta_logotipo = __URL_RAIZ_IMAGENES__.__DIR_UPLOADS__.__DIR_LOGOTIPOS__.$lEstFichero_logotipo;

		$lEstDireccion = mysql_result($rsEstablecimiento,0,"calle").", ".mysql_result($rsEstablecimiento,0,"numero");
		$lEstPiso_mano = mysql_result($rsEstablecimiento,0,"piso_mano");
		$lEstTelefono = mysql_result($rsEstablecimiento,0,"telefono");
		$lEstURL = mysql_result($rsEstablecimiento,0,"url");
		if ( ($EstlURL != "") && (strpos($lEstURL,"https://") == false) )
		{	$lEstURL = "https://".$lEstURL; }
		$lEstResponsable = mysql_result($rsEstablecimiento,0,"responsable");
		$lEstContactoNombre = mysql_result($rsEstablecimiento,0,"contacto_nombre");
		$lEstContactoTelefono = mysql_result($rsEstablecimiento,0,"contacto_telefono");
		$lEstCP = mysql_result($rsEstablecimiento,0,"cp");
		$lEstMunicipio = mysql_result($rsEstablecimiento,0,"nombre_poblacion");
		$lEstProvincia = mysql_result($rsEstablecimiento,0,"nombre_provincia");
	}
?>
		<div class="span-22 prepend-1 append-1 last" id="cabecera">
<?php
	// Martin 14/04/08. Las variables $lParametros y $lParametrosSID se establece en el script /inc_comun.php.
?>
			<div class="span-17" id="logotipo">
				<a href="index.php<?php print($lParametros.$lParametrosSID); ?>">
				<!--<img src="<?php //print($lEstRuta_logotipo); ?>" />-->
				<img src="images/logo_secretodeollo.gif" />
				</a>
			</div>
			<div class="span-5 last">
			</div>
			<div class="span-4" id="lista_idiomas">
<?php
// 20202210 jit . Sacamos los idiomas.
	// Martin 10/04/08. Ponemos los idiomas que usa el establecimiento. Las variables $lIdEstablecimiento,
	//$lIdIdioma y $lPaginaActual se establecen en inc_comun.
$lCadena = "SELECT idi.ididioma, idi_trad.traduccion".
						" FROM ".__TABLA_IDIOMAS__." AS idi".
						", ".__TABLA_IDIOMAS_ESTABLECIMIENTOS__." AS idi_est".
						", ".__TABLA_IDIOMAS_TRADUCCION__." AS idi_trad".
						" WHERE idi.ididioma = idi_est.ididioma".
						" AND idi.ididioma = idi_trad.ididioma1".
						" AND idi_est.idestablecimiento = ".$lIdEstablecimiento.
						" AND idi_trad.ididioma2 = ".$lIdIdioma.
						" ORDER BY idi.ididioma";
	$rsIdiomas = fQuery($lCadena);
	$lNumIdiomas = mysql_num_rows($rsIdiomas);
	if ($lNumIdiomas > 0)
	{
?>
				<ul>
<?php
		$lParamIdiomaEst = "?id=".$lIdEstablecimiento;
		for ($li=0;$li<$lNumIdiomas;$li++)
		{
			$lIdiId = mysql_result($rsIdiomas,$li,"ididioma");
			$lIdiNombre = mysql_result($rsIdiomas,$li,"traduccion");
			$lParamIdiomaIdi = "&i=".$lIdiId;
			if (SID != "")
			{	$lParametrosSID = "&".SID; }
			else
			{	$lParametrosSID = ""; }
?>

					<li><a href="<?php print($lPaginaActual.$lParamIdiomaEst.$lParamIdiomaIdi.$lParametrosSID); ?>"><?php print($lIdiNombre); ?></a></li>
<?php
		} 
?>
				</ul>
<?php	
	}
?>
			</div>
		</div>