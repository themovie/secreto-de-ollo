<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");

//print("SID=".SID."<br />");
	// Martin 29/04/08. Variables para controlar los errores
	$lEnviado = $_GET["env"];
	$lUrl = $_GET["url"];
	$lError = 0;
	if ($lEnviado == "si" &&
			( $_POST["nombre"] == "" ||
				$_POST["nombre-amigo"] == "" ||
				$_POST["email"] == "" ||
				$_POST["email-amigo"] == "" ||
				$_POST["observaciones"] == ""
			)
		)
	{
		$lError = 1;
	}//fin if ($lEnviado=="si" &&...
	if ($lError == 0 && $lEnviado == "si") 
	{
		$lCadena = "SELECT est.email, est_nom.nombre".
						" FROM ".__TABLA_ESTABLECIMIENTOS__." est".
						", ".__TABLA_ESTABLECIMIENTOS_NOMBRES__." est_nom".
						" WHERE est.idestablecimiento = est_nom.idestablecimiento".
						" AND est.idestablecimiento = ".$lIdEstablecimiento.
						" AND est_nom.ididioma = ".$lIdIdioma;
		$rsEst = fQuery($lCadena);
		if (mysql_num_rows($rsEst) > 0)
		{
			$lEstMail = mysql_result($rsEst,0,"email");
			$lEstNombre = mysql_result($rsEst,0,"nombre");
		}

		$lFecha = time();
		$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
		$lIP2 = $_SERVER["REMOTE_ADDR"];
		$lNavegador = $_SERVER["HTTP_USER_AGENT"];
		$lUrl = $_POST["url"];
		$lNombre = $_POST["nombre"];
		$lNombreAmigo = $_POST["nombre-amigo"];
		$lEMail = $_POST["email"];
		$lEMailAmigo = $_POST["email-amigo"];
		$lObservaciones = $_POST["observaciones"];

		$lUltimo = fUltimoID(__TABLA_RECOMENDACIONES_FICHA__,"idrecomendacion");
		$lUltimo++;

		$lCadena = "INSERT INTO ".__TABLA_RECOMENDACIONES_FICHA__." (".
					"idrecomendacion, idestablecimiento, fecha, ip1,".
					" ip2, navegador, pagina_recomendada, nombre,".
					" `nombre-amigo`, email, `email-amigo`, observaciones".
					") VALUES (".
					$lUltimo.",".
					fLimpiar_sql($lIdEstablecimiento).",". //Martin 18/07/08. Incluimos el idestablecimiento.
					$lFecha.",".
					fComillas(fLimpiar_sql($lIP1)).",".
					fComillas(fLimpiar_sql($lIP2)).",".
					fComillas(fLimpiar_sql($lNavegador)).",".
					fComillas(fLimpiar_sql($lUrl)).",".
					fComillas(fLimpiar_sql($lNombre)).",".
					fComillas(fLimpiar_sql($lNombreAmigo)).",".
					fComillas(fLimpiar_sql($lEMail)).",".
					fComillas(fLimpiar_sql($lEMailAmigo)).",".
					fComillas(fLimpiar_sql($lObservaciones)).
					")";
		fQuery($lCadena);

		$lTitulo = fLiteral(383,$lIdIdioma)." ".$lEstNombre; //"Recomendaci�n Web ".$lEstNombre;

		$lTexto1 = fLiteral(58,$lIdIdioma).", "."<br>\n"; //"Hola, "."<br>\n";
		$lTexto1.= fLiteral(384,$lIdIdioma)." ".$lEstNombre."<br>\n"; //"Un amigo te recomienda que visites la p�gina web de ".$lEstNombre."<br>\n";
		$lTexto1.= fLiteral(385,$lIdIdioma).": ".urldecode($lUrl)."<br><br>\n"; //"En concreto �sta p�gina: ".urldecode($lUrl)."<br><br>\n";
		$lTexto1.= fLiteral(386,$lIdIdioma).": "."<br>\n"; //"La informaci�n introducida es la siguiente:"."<br>\n";
		$lTexto1.= fLiteral(0,$lIdIdioma).": ".$lNombre."<br>\n"; //"Nombre: ".$lNombre."<br>\n";
		$lTexto1.= fLiteral(4,$lIdIdioma).": ".$lEMail."<br>\n"; //"Email: ".$lEMail."<br>\n";
		$lTexto1.= fLiteral(368,$lIdIdioma).": ".str_replace("\n","<br>\n",$lObservaciones)."<br>\n"; //"Observaciones: ".str_replace("\n","<br>\n",$lObservaciones)."<br>\n";

		$lTexto2 = fLiteral(58,$lIdIdioma).", "."\n"; //"Hola, "."\n";
		$lTexto2.= fLiteral(384,$lIdIdioma)." ".$lEstNombre."\n"; //"Un amigo te recomienda que visites la p�gina web de ".$lEstNombre."\n";
		$lTexto2.= fLiteral(385,$lIdIdioma).": ".urldecode($lUrl)."\n\n"; //"En concreto �sta p�gina: ".urldecode($lUrl)."\n\n";
		$lTexto2.= fLiteral(386,$lIdIdioma).": "."\n"; //"La informaci�n introducida es la siguiente:"."\n";
		$lTexto2.= fLiteral(0,$lIdIdioma).": ".$lNombre."\n"; //"Nombre: ".$lNombre."\n";
		$lTexto2.= fLiteral(4,$lIdIdioma).": ".$lEMail."\n"; //"Email: ".$lEMail."\n";
		$lTexto2.= fLiteral(368,$lIdIdioma).": ".$lObservaciones."\n"; //"Observaciones: ".$lObservaciones."\n";

		$lRemitente = $lEMail;
		//$lDestinatario = $lEstMail;
		$lDestinatario = $lEMailAmigo;
		//fEnviarMail("FeelFreeRentals",$lRemitente,"a.martin@themovie.org",$lTitulo, $lTexto1, $lTexto2, "");
		fEnviarMail($lNombre,$lRemitente,"a.martin@themovie.org",$lTitulo, $lTexto1, $lTexto2, "");
		fEnviarMail($lNombre,$lRemitente,$lEstMail,$lTitulo, $lTexto1, $lTexto2, "");
		fEnviarMail($lNombre,$lRemitente,$lDestinatario,$lTitulo, $lTexto1, $lTexto2,"");
	}//fin if ($lError==0&&$lEnviado=="si")...
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!--CSS -->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
<script src="include/js/funciones.js"></script>
<title><?php print(fLiteral(383,$lIdIdioma)); //Recomendaci�n Web ?></title>
</head>

<body class="top" onload="igualaColumnas();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<div class="span-24 top" id="contenido">
			<img id="separacion_cabecera_contenido" src="images/separacion_cabecera_contenido.gif" />
			<?php include("include/modulos/inc_menu_navegacion.php"); ?>
			<div class="span-12 border" id="cuerpo">
				<div class="span-12" id="titulo">
					<?php print(fLiteral(388,$lIdIdioma)); //FORMULARIO DE RECOMENDACION A UN AMIGO ?>
				</div>
				<div class="span-12 texto">
<?php 
				if ($lEnviado == "si" && $lError == 1)
				{
					$lUrl = $_POST["url"];
?>
					<h5><?php print(fLiteral(370,$lIdIdioma)); //ERROR ?></h5>
					<p><?php print(fLiteral(371,$lIdIdioma).": "); //print("Los siguientes campos son incorrectos o estan vacios:"); ?> 
<?php 
									$lMensajeError = "";
									if ($_POST["nombre"]=="") 
									{
										$lMensajeError .= fLiteral(0,$lIdIdioma); //"Nombre";
									} 
									if ($_POST["nombre-amigo"]=="") 
									{
										if ($lMensajeError != "")
										{ $lMensajeError .= ", "; }
										$lMensajeError .= fLiteral(389,$lIdIdioma); //"Nombre de tu amigo";
									} 
									if ($_POST["email"]=="") 
									{
										if ($lMensajeError != "")
										{ $lMensajeError .= ", "; }
										$lMensajeError .= fLiteral(4,$lIdIdioma); //"Email";
									} 
									if ($_POST["email-amigo"]=="") 
									{
										if ($lMensajeError != "")
										{ $lMensajeError .= ", "; }
										$lMensajeError .= fLiteral(390,$lIdIdioma); //"Email de tu amigo";
									} 
									if($_POST["observaciones"]=="") 
									{
										if ($lMensajeError != "")
										{ $lMensajeError .= ", "; }
										$lMensajeError .= fLiteral(368,$lIdIdioma); //"Observaciones"; 
									} 
									print($lMensajeError);
?>
						</p>
<?php
				}
				if ($lEnviado == "si" && $lError == 0)
				{
?>
						<h5><?php print(fLiteral(372,$lIdIdioma)); //FORMULARIO ENVIADO CORRECTAMENTE ?></h5>
						<p>
<?php
								print(fLiteral(373,$lIdIdioma).". ".
										fLiteral(374,$lIdIdioma));
								//print("El formulario se ha enviado correctamente".". ".
								//		"Gracias por contactar con nosotros");
?>
						</p>
<?php
				}
				else
				{
				// Martin 29/04/08. Usamos las variables $lParametros y $lParametrosSID definidas en el 
				//script inc_comun.php.
?>
					<!--<form id="form_contacto" method="post" action="contacto.php<?php// print($lParametros.$lParametrosSID); ?>&env=si">-->
					<form id="form_contacto" method="post" action="enviar-amigo.php?env=si<?php print($lParametrosSID); ?>">
						<div class="linea_form_contacto">
							<label for="nombre">*<?php print(fLiteral(0,$lIdIdioma)); //Nombre ?> </label>
							<input class="texto" name="nombre" id="nombre" value="<?php print($_POST["nombre"]); ?>" maxlength="50" />
						</div>
						<div class="linea_form_contacto">
							<label for="nombre-amigo">*<?php print(fLiteral(389,$lIdIdioma)); //Nombre de tu amigo ?> </label>
							<input class="texto" name="nombre-amigo" id="nombre-amigo" value="<?php print($_POST["nombre-amigo"]); ?>" maxlength="50" />
						</div>
						<div class="linea_form_contacto">
							<label for="email">*<?php print(fLiteral(375,$lIdIdioma)); //Correo Electr�nico ?> </label>
							<input class="texto" name="email" id="email" value="<?php print($_POST["email"]); ?>" maxlength="255" />
						</div>
						<div class="linea_form_contacto">
							<label for="email-amigo">*<?php print(fLiteral(390,$lIdIdioma)); //Correo Electr�nico de tu amigo ?> </label>
							<input class="texto" name="email-amigo" id="email-amigo" value="<?php print($_POST["email-amigo"]); ?>" maxlength="255" />
						</div>
						<div class="linea_form_contacto">
							<label for="observaciones">*<?php print(fLiteral(368,$lIdIdioma)); //Observaciones ?> </label>
							<textarea id="observaciones" name="observaciones" rows="7" cols="5"><?php print($_POST["observaciones"]); ?></textarea>
						</div>
						<div class="linea_form_contacto">
								<input id="url" name="url" type="hidden" value="<?php print($lUrl); ?>" />
								<input id="form_contacto_bot_enviar" type="submit" value="<?php print(fLiteral(408,$lIdIdioma)); //Enviar ?>" />
						</div>
						<div class="linea_form_contacto">
								<span id="form_contacto_nota"> *<?php print(fLiteral(376,$lIdIdioma)); //Campos obligatorios ?> &nbsp; </span>
						</div>
					</form>
<?php
				}//fin //fin if($lEnviado=="si"&&$lError==0)...
?>
				</div>
			</div>
			<div class="span-5 append-1 last" id="columna_dcha">
			</div>
			<?php include("include/modulos/inc_pie.php"); ?>
		</div>
	</div>
<?php
//print("index.php. \$_SESSION[\"lista_fotos_est_primera\"]=".$_SESSION["lista_fotos_est_primera"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_ultima\"]=".$_SESSION["lista_fotos_est_ultima"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_actual\"]=".$_SESSION["lista_fotos_est_actual"]."<br />");
//print("index.php. \$lNumImagenAnterior=".$lNumImagenAnterior."<br />");
//print("index.php. \$lNumImagenSiguiente=".$lNumImagenSiguiente."<br />");
?>
	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write("\<script src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'>\<\/script>" );
	</script>
	<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-245986-35");
	pageTracker._initData();
	pageTracker._trackPageview();
	</script>

</body>
</html>
