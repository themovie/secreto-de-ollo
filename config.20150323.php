<?php
/*
	Fichero:				config.php
	Autor:					Iban Rodriguez
	Fecha creacin:			31/03/2006
	 ltima modificacin: 	-
	Editado por:			-
	Funci n:				Definicin de variables constantes para la aplicaci n.
*/
?>
<?php
// Aplicacin
	define("__TITULO_WEB__","Sistema de Reservas On-Line");
	define("__TITULO_WEBC__","Sistema de Reservas On-Line");
	define("__TITULO_WEBI__","Sistema de Reservas On-Line");
	define("__TITULO_WEBT__","Sistema de Reservas On-Line");
	define("__EMPRESA__","");
	define("__TELEFONO__","+00 34 902198547");
	define("__URL__","");
	define("__DOMINIO__","www.reservadealojamientos.com");
	define("__DOMINIO_CENTRAL__","central.reservadealojamientos.com");
	define("__DOMINIO_CENTRAL_ADMIN__","central.reservadealojamientos.com/adm");
	define("__SUBDOMINIO__","central");
	define("__LINK_PAGINA__","");
	define("__MAIL_FORMULARIOS__","");
	define("__MAIL_SOPORTE__","soporte@reservadealojamientos.com");
	define("__RAIZ__",$_SERVER["DOCUMENT_ROOT"]."/");
	define("__DIR_APP__","");
	define("__ROOT__","http://".$_SERVER["HTTP_HOST"]."/");
	define("__INC_HTML__","html/");
	define("__INC_HOME__","include/");
	define("__INC_APP__","app/");
	define("__INC_MODULOS__","modulos/");
	define("__DIR_ADM__","adm/");
	define("__DIR_IMAGES__","images/");
	define("__DIR_ESTABLECIMIENTOS__","establecimientos/");
	define("__DIR_TIPOS_HAB__","tipos_hab/");
	define("__DIR_UPLOAD__","uploads/");
	define("__DIR_UPLOADS__","uploads/");
	define("__DIR_LOGOTIPOS__","logotipos/");
	//define("__URL_RAIZ_IMAGENES__","http://hotelejemplo.reservadealojamientos.com/");
	define("__URL_RAIZ_IMAGENES__","http://central.reservadealojamientos.com/");

// Administrador
	define("__TITULO_ADMINISTRADOR__","Admin Online",true);
	define("__SEPARADOR_TITULO__"," :: ",true);
	define("__APP_VERSION__","4.0.0 :: GZIP Enabled",true);
	define("__HOJA_INICIO_ADMIN__","menu_principal.php");

// Metas
	define("__META_KEYWORDS__","");
	define("__META_DESCRIPTION__","");
	define("__META_TITLE__","");

//MySQL
	if(file_exists($_SERVER["DOCUMENT_ROOT"]."/reservas_movido"))
	{
		define("__HOST__","localhost");
	}
	else
	{
		define("__HOST__","82.223.246.129");
	}
	//define("__HOST__","82.223.209.202");
	//define("__HOST__","82.223.234.104");
	define("__BBDD__","alojamientos");
//	define("__BBDD_USER__","externores");
define("__BBDD_USER__","alojamientos");
	define("__BBDD_PWD__","1qa3eD");

//Diferencias entre tipos de establecimiento
	if ($_SESSION["idtipo"] == "1")
	{
		define("__TIPO_ESTABLECIMIENTO__","hotel");
		define("__UNIDAD_ESTABLECIMIENTO__","habitaci&oacute;n");
		define("__UNIDADES_ESTABLECIMIENTO__","habitaciones");
		define("__UNIDAD_ESTABLECIMIENTO_2__","tipo de habitaci&oacute;n");
		define("__UNIDADES_ESTABLECIMIENTO_2__","tipos de habitaci&oacute;n");
		define("__UNIDADES_ESTABLECIMIENTO_3__","tipos de habitaci&oacute;n");
		define("__COMBO_INICIAL__","Tipo de habitaci&oacute;n");
	}
	elseif ($_SESSION["idtipo"] == "2")
	{
		define("__TIPO_ESTABLECIMIENTO__","apartamento");
		define("__UNIDAD_ESTABLECIMIENTO__","apartamento");
		define("__UNIDADES_ESTABLECIMIENTO__","apartamentos");
		define("__UNIDAD_ESTABLECIMIENTO_2__","apartamento");
		define("__UNIDADES_ESTABLECIMIENTO_2__","apartamentos");
		define("__UNIDADES_ESTABLECIMIENTO_3__","mto apartamentos");
		define("__COMBO_INICIAL__","Seleccione apartamento");
	}

// Anchuras de las imagenes
	// Martin 05/12/07 Anchuras que se usan para las fotos en FeelFreeRentals
	define("__IMG_PASE__",647);
	define("__IMG_LISTA__",270);
	define("__IMG_HOME__",217);
	// Martin 06/03/09. Anchuras que se usan en las fotos del establecimiento y las habitaciones:
	define("__IMG_LISTA_ESTABLECIMIENTO__",470);
	define("__IMG_LISTA_HABITACION__",470);

	// Martin 05/12/07. Anchura y altura maxima de la imagen del logotipo que se mostrara 
	//en las pantallas del proceso de reservas.
	define("__ALTURA_MAX_LOGOTIPO__",80);
	define("__ANCHURA_MAX_LOGOTIPO__",400);
	// Martin 31/01/08. Anchura y altura maxima del logotipo que se mostrara en el administrador.
	define("__ALTURA_MAX_LOGOTIPO_ADM__",40);
	define("__ANCHURA_MAX_LOGOTIPO_ADM__",200);

// Tablas
	// phpBB
	// Resto
	// POR ORDEN --- ALFABETICO ----
	define("__TABLA_ADICIONALES__","rsv_adicionales");
	define("__TABLA_ADICIONALES_HABITACIONES__","rsv_adicionales_habitaciones");
	define("__TABLA_ADICIONALES_TEMPORADAS__","rsv_adicionales_temporadas");
	define("__TABLA_ADICIONALES_TRADUCCION__","rsv_adicionales_traduccion");
	define("__TABLA_BLOQUEOS__","rsv_bloqueos");
	define("__TABLA_CARACTERISTICAS__","rsv_caracteristicas");
	define("__TABLA_CARACTERISTICAS_TIPOS__","rsv_caracteristicas_tipos");
	define("__TABLA_COLORES_TEMPORADAS__","rsv_colores_temporadas");
	define("__TABLA_COMPROBACIONES__","rsv_comprobaciones");
	define("__TABLA_CONTACTOS_FICHA__","rsv_contactos_ficha");
	define("__TABLA_CONTRASENAS_OLVIDADAS__","rsv_contrasenas_olvidadas");
	define("__TABLA_DESCRIPCIONES__","rsv_descripciones");
	define("__TABLA_ENTORNO_INFORMACION__","rsv_entorno_informacion");
	define("__TABLA_ERRORES__","rsv_errores");
	define("__TABLA_ESTABLECIMIENTOS__","rsv_establecimientos");
	define("__TABLA_ESTABLECIMIENTOS_CIERRE__","rsv_establecimientos_cierre");
	define("__TABLA_ESTABLECIMIENTOS_INSTALACIONES__","rsv_establecimientos_instalaciones");
	define("__TABLA_ESTABLECIMIENTOS_NOMBRES__","rsv_establecimientos_nombres");
	define("__TABLA_ESTABLECIMIENTOS_TIPOS__","rsv_establecimientos_tipos");
	define("__TABLA_FORMAS_PAGO__","rsv_formas_pago");
	define("__TABLA_HABITACIONES_CARACTERISTICAS__","rsv_habitaciones_caracteristicas");
	define("__TABLA_HABITACIONES_CIERRE__","rsv_habitaciones_cierre");
	define("__TABLA_HABITACIONES_ESTABLECIMIENTOS__","rsv_habitaciones_establecimientos");
	define("__TABLA_HABITACIONES_ESTABLECIMIENTOS_EXCEPCIONES__","rsv_habitaciones_establecimientos_excepciones");
	define("__TABLA_HABITACIONES_TEMPORADAS__","rsv_habitaciones_temporadas");
	define("__TABLA_HABITACIONES_TIPOS__","rsv_habitaciones_tipos");
	define("__TABLA_IDIOMAS__","rsv_idiomas");
	define("__TABLA_IDIOMAS_ESTABLECIMIENTOS__","rsv_idiomas_establecimientos");
	define("__TABLA_IDIOMAS_TRADUCCION__","rsv_idiomas_traduccion");
	define("__TABLA_IMAGENES__","rsv_imagenes");
	define("__TABLA_IMAGENES_TRADUCCIONES__","rsv_imagenes_traducciones");
	define("__TABLA_INSTALACIONES_TIPOS__","rsv_instalaciones_tipos");
	define("__TABLA_INSTALACIONES__","rsv_instalaciones");
	define("__TABLA_LINKS__","rsv_links");
	define("__TABLA_LINKS_TRADUCCIONES__","rsv_links_traducciones");
	define("__TABLA_LITERALES_TRADUCCION__","rsv_literales_traduccion");
	define("__TABLA_LOCALIDADES__","rsv_localidades");
	define("__TABLA_LOCALIZACION__","rsv_localizacion");
	define("__TABLA_LOCALIZACION_HABITACIONES__","rsv_localizacion_habitaciones");
	define("__TABLA_LOC_PAISES__","rsv_loc_paises");
	define("__TABLA_LOC_POBLACIONES__","rsv_loc_poblaciones");
	define("__TABLA_LOC_PROVINCIAS__","rsv_loc_provincias");
	define("__TABLA_OFERTAS__","rsv_ofertas");
	define("__TABLA_OFERTAS_FECHAS__","rsv_ofertas_fechas");
	define("__TABLA_OFERTAS_TRADUCCIONES__","rsv_ofertas_traducciones");
	define("__TABLA_OPERACIONES__","rsv_operaciones");
	define("__TABLA_PAISES__","rsv_paises");
	define("__TABLA_PERMISOS__","rsv_permisos");
	define("__TABLA_POLITICAS__","rsv_politicas");
	define("__TABLA_POLITICAS_HABITACIONES__","rsv_politicas_habitaciones");
	define("__TABLA_PROVINCIAS__","rsv_provincias");
	define("__TABLA_PYD_PRECIOS__","rsv_pyd_precios");
	define("__TABLA_PYD_HABITACIONES__","rsv_pyd_habitaciones");
	define("__TABLA_PYD_CIERRE__","rsv_pyd_cierre");
	define("__TABLA_PYD_VACACIONES__","rsv_pyd_vacaciones");
	define("__TABLA_PYD_RESTRICCIONES__","rsv_pyd_restricciones");
	define("__TABLA_PYD_COMISIONES__","rsv_pyd_comisiones");
	define("__TABLA_PYD_EXCEPCIONES__","rsv_pyd_excepciones");
	define("__TABLA_RECOMENDACIONES_FICHA__","rsv_recomendaciones_ficha");
	define("__TABLA_RESERVAS__","rsv_reservas");
	define("__TABLA_REGISTRO__","rsv_registro");
	define("__TABLA_SOPORTE__","rsv_soporte");
	define("__TABLA_TARJETAS__","rsv_tarjetas");
	define("__TABLA_TARJETAS_ESTABLECIMIENTOS__","rsv_tarjetas_establecimientos");
	define("__TABLA_TEMPORADAS__","rsv_temporadas");
	define("__TABLA_TEMPORADAS_FECHAS__","rsv_temporadas_fechas");
	define("__TABLA_TEMPORADAS_DESCRIPCIONES__","rsv_temporadas_descripciones");
	define("__TABLA_TEMPORADAS_PERIODOS__","rsv_temporadas_periodos");
	define("__TABLA_USUARIOS__","rsv_usuarios");
	define("__TABLA_USUARIOS_HABITACIONES__","rsv_usuarios_habitaciones");
	define("__TABLA_VARIABLES__","rsv_variables");
	define("__TABLA_ZONAS__","rsv_zonas");
	define("__TABLA_ZONAS_LOCALIDADES__","rsv_zonas_localidades");
?>
