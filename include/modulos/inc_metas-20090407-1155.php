<?php
	if ($eIdioma == "es")
	{
?>
<meta name="DC.CREATOR" content="The Movie">
<meta name="DC.TITLE" content="Feel Free Rentals">
<!--<meta name="DC.DESCRIPTION" content="Alquiler de apartamentos, casas, villas y habitaciones con central de reservas online y consulta de disponibilidad.">-->
<meta name="DC.DESCRIPTION" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="DC.SUBJECT" content="feelfreerentals,reserva,online,alquiler,apartamentos,vacaciones,dormir,ocio,alojarse,
ciudad,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,casas,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="DC.LANGUAGE" content="es">

<meta http-equiv="Author" content="The Movie">
<meta http-equiv="Title" content="Feel Free Rentals">
<!--<meta http-equiv="Description" content="Alquiler de apartamentos, casas, villas y habitaciones con central de reservas online y consulta de disponibilidad.">-->
<meta http-equiv="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta http-equiv="Keywords" content="feelfreerentals,reserva,online,alquiler,apartamentos,vacaciones,dormir,ocio,alojarse,
ciudad,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,casas,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Language" content="es">

<meta name="Author" content="The Movie">
<meta name="Title" content="Feel Free Rentals">
<!--<meta name="Description" content="Alquiler de apartamentos, casas, villas y habitaciones con central de reservas online y consulta de disponibilidad.">-->
<meta name="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="Keywords" content="feelfreerentals,reserva,online,alquiler,apartamentos,vacaciones,dormir,ocio,alojarse,
ciudad,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,casas,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="VW96.ObjectType" content="Document">
<meta name="Revisit" content="5 days">
<meta name="Revisit-After" content="5 days">
<meta name="Resource-Type" content="Document">
<meta name="Robots" content="index,follow">
<?php
	}
	elseif ($eIdioma == "en")
	{
?>
<meta name="DC.CREATOR" content="The Movie">
<meta name="DC.TITLE" content="Feel Free Rentals">
<meta name="DC.DESCRIPTION" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="DC.SUBJECT" content="feelfreerentals,reservation,online,rental,apartments,holidays,sleep,leisure,lodge,
city,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,houses,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="DC.LANGUAGE" content="en">

<meta http-equiv="Author" content="The Movie">
<meta http-equiv="Title" content="Feel Free Rentals">
<meta http-equiv="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta http-equiv="Keywords" content="feelfreerentals,reservation,online,rental,apartments,holidays,sleep,leisure,lodge,
city,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,houses,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Language" content="en">

<meta name="Author" content="The Movie">
<meta name="Title" content="Feel Free Rentals">
<meta name="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="Keywords" content="feelfreerentals,reservation,online,rental,apartments,holidays,sleep,leisure,lodge,
city,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,houses,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="VW96.ObjectType" content="Document">
<meta name="Revisit" content="5 days">
<meta name="Revisit-After" content="5 days">
<meta name="Resource-Type" content="Document">
<meta name="Robots" content="index,follow">
<?php
	}
	elseif ($eIdioma == "de")
	{
?>
<meta name="DC.CREATOR" content="The Movie">
<meta name="DC.TITLE" content="Feel Free Rentals">
<meta name="DC.DESCRIPTION" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="DC.SUBJECT" content="feelfreerentals,reservierung,online,alquiler,wohnungen,urlaub,schlaf,freizeit,alojarse,
stadt,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,h�user,villen,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="DC.LANGUAGE" content="de">

<meta http-equiv="Author" content="The Movie">
<meta http-equiv="Title" content="Feel Free Rentals">
<meta http-equiv="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta http-equiv="Keywords" content="feelfreerentals,reservierung,online,alquiler,wohnungen,urlaub,schlaf,freizeit,alojarse,
stadt,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,h�user,villen,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Language" content="de">

<meta name="Author" content="The Movie">
<meta name="Title" content="Feel Free Rentals">
<meta name="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="Keywords" content="feelfreerentals,reservierung,online,alquiler,wohnungen,urlaub,schlaf,freizeit,alojarse,
stadt,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,h�user,villen,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="VW96.ObjectType" content="Document">
<meta name="Revisit" content="5 days">
<meta name="Revisit-After" content="5 days">
<meta name="Resource-Type" content="Document">
<meta name="Robots" content="index,follow">
<?php
	}
	elseif ($eIdioma == "it")
	{
?>
<meta name="DC.CREATOR" content="The Movie">
<meta name="DC.TITLE" content="Feel Free Rentals">
<meta name="DC.DESCRIPTION" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="DC.SUBJECT" content="feelfreerentals,prenotazione,online,affitto,appartamenti,vacanze,dormire,svago,alloggiarsi,
citt�,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,case,ville,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="DC.LANGUAGE" content="it">

<meta http-equiv="Author" content="The Movie">
<meta http-equiv="Title" content="Feel Free Rentals">
<meta http-equiv="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta http-equiv="Keywords" content="feelfreerentals,prenotazione,online,affitto,appartamenti,vacanze,dormire,svago,alloggiarsi,
citt�,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,case,ville,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Language" content="it">

<meta name="Author" content="The Movie">
<meta name="Title" content="Feel Free Rentals">
<meta name="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="Keywords" content="feelfreerentals,prenotazione,online,affitto,appartamenti,vacanze,dormire,svago,alloggiarsi,
citt�,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,case,ville,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="VW96.ObjectType" content="Document">
<meta name="Revisit" content="5 days">
<meta name="Revisit-After" content="5 days">
<meta name="Resource-Type" content="Document">
<meta name="Robots" content="index,follow">
<?php
	}
	elseif ($eIdioma == "fr")
	{
?>
<meta name="DC.CREATOR" content="The Movie">
<meta name="DC.TITLE" content="Feel Free Rentals">
<meta name="DC.DESCRIPTION" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="DC.SUBJECT" content="feelfreerentals,r�servation,online,location,appartements,vacances,dormir,loisir,loger,
ville,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,maisons,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="DC.LANGUAGE" content="fr">

<meta http-equiv="Author" content="The Movie">
<meta http-equiv="Title" content="Feel Free Rentals">
<meta http-equiv="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta http-equiv="Keywords" content="feelfreerentals,r�servation,online,location,appartements,vacances,dormir,loisir,loger,
ville,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,maisons,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Language" content="fr">

<meta name="Author" content="The Movie">
<meta name="Title" content="Feel Free Rentals">
<meta name="Description" content="Gestionamos alquileres de apartamentos y casas de alta calidad en San Sebasti�n, Ibiza, Bilbao, Baqueira Beret y Biarritz.">
<meta name="Keywords" content="feelfreerentals,r�servation,online,location,appartements,vacances,dormir,loisir,loger,
ville,ibiza,san sebastian,vizcaya,donostia,arrendamientos,rentals,apartments,maisons,villas,huesca,bilbao,jaca,short,term,
donostia,international,film,festival,guggenheim">
<meta name="VW96.ObjectType" content="Document">
<meta name="Revisit" content="5 days">
<meta name="Revisit-After" content="5 days">
<meta name="Resource-Type" content="Document">
<meta name="Robots" content="index,follow">
<?php
	}
?>
