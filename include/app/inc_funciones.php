<?php
// Image Resize
// imagen origen, X miniatura, Y miniatura, fichero salida
function createthumb($IMAGE_SOURCE,$THUMB_X,$THUMB_Y,$OUTPUT_FILE)
{
	$BACKUP_FILE = $OUTPUT_FILE . "_backup.jpg";
	copy($IMAGE_SOURCE,$BACKUP_FILE);
	$IMAGE_PROPERTIES = getimagesize($BACKUP_FILE);
	if (!$IMAGE_PROPERTIES[2] == 2)
		return(0);
	else
	{
		$SRC_IMAGE = imagecreatefromjpeg($BACKUP_FILE);
		$SRC_X = imagesx($SRC_IMAGE);
		$SRC_Y = imagesy($SRC_IMAGE);
		if (($THUMB_Y == "0") && ($THUMB_X == "0"))
			return(0);
		elseif($THUMB_Y == "0")
		{
			$SCALEX = $THUMB_X/($SRC_X-1);
			$THUMB_Y = $SRC_Y*$SCALEX;
		}
		elseif($THUMB_X == "0")
		{
			$SCALEY = $THUMB_Y/($SRC_Y-1);
			$THUMB_X = $SRC_X*$SCALEY;
		}

		$THUMB_X = (int)($THUMB_X);
				$THUMB_Y = (int)($THUMB_Y);
		$DEST_IMAGE = imagecreatetruecolor($THUMB_X, $THUMB_Y);
		unlink($BACKUP_FILE);
		if (!imagecopyresized($DEST_IMAGE, $SRC_IMAGE, 0, 0, 0, 0, $THUMB_X, $THUMB_Y, $SRC_X, $SRC_Y))
		{
			imagedestroy($SRC_IMAGE);
			imagedestroy($DEST_IMAGE);
			return(0);
		}
		else
		{
			imagedestroy($SRC_IMAGE);
			if (imagejpeg($DEST_IMAGE,$OUTPUT_FILE))
			{
				imagedestroy($DEST_IMAGE);
				return(1);
			}
			imagedestroy($DEST_IMAGE);
		}
	return(0);
	}
}

// Image Resize (tipo GIF).
// imagen origen, X miniatura, Y miniatura, fichero salida
// Martin 12/12/07. Creada para redimensionar imagenes GIF.
function createthumb_gif($IMAGE_SOURCE,$THUMB_X,$THUMB_Y,$OUTPUT_FILE)
{
	$BACKUP_FILE = $OUTPUT_FILE . "_backup.gif";
	copy($IMAGE_SOURCE,$BACKUP_FILE);
	$IMAGE_PROPERTIES = getimagesize($BACKUP_FILE);
	if (!$IMAGE_PROPERTIES[2] == 2)
		return(0);
	else
	{
		$SRC_IMAGE = imagecreatefromgif($BACKUP_FILE);
		$SRC_X = imagesx($SRC_IMAGE);
		$SRC_Y = imagesy($SRC_IMAGE);
		if (($THUMB_Y == "0") && ($THUMB_X == "0"))
			return(0);
		elseif($THUMB_Y == "0")
		{
			$SCALEX = $THUMB_X/($SRC_X-1);
			$THUMB_Y = $SRC_Y*$SCALEX;
		}
		elseif($THUMB_X == "0")
		{
			$SCALEY = $THUMB_Y/($SRC_Y-1);
			$THUMB_X = $SRC_X*$SCALEY;
		}

		$THUMB_X = (int)($THUMB_X);
				$THUMB_Y = (int)($THUMB_Y);
		$DEST_IMAGE = imagecreatetruecolor($THUMB_X, $THUMB_Y);
		unlink($BACKUP_FILE);
		if (!imagecopyresized($DEST_IMAGE, $SRC_IMAGE, 0, 0, 0, 0, $THUMB_X, $THUMB_Y, $SRC_X, $SRC_Y))
		{
			imagedestroy($SRC_IMAGE);
			imagedestroy($DEST_IMAGE);
			return(0);
		}
		else
		{
			imagedestroy($SRC_IMAGE);
			if (imagegif($DEST_IMAGE,$OUTPUT_FILE))
			{
				imagedestroy($DEST_IMAGE);
				return(1);
			}
			imagedestroy($DEST_IMAGE);
		}
	return(0);
	}
}

function fReferenciaFoto($pFoto)
{
	$lReferencia = "AGK".fPonerCeros($pFoto,5);
	
	return $lReferencia;
}

function fProcesarTexto($pTexto)
{
	$lCadena=$pTexto;
//	$lCadena = str_replace("'","\'",$lCadena);
/*
	$lCadena=str_replace('&quot;',"`",$lCadena);
	$lCadena=str_replace('&ldquo;',"",$lCadena);
	$lCadena=str_replace('&rdquo;',"`",$lCadena);
	$lCadena=str_replace("'","\'",$lCadena);
	$lCadena=str_replace("\\","",$lCadena);
*/
	$lCadena=str_replace('"',"`",$lCadena);
	$lCadena=str_replace('',"`",$lCadena);
	$lCadena=str_replace('',"",$lCadena);
	$lCadena=str_replace("'","",$lCadena);
	$lCadena=str_replace("\\","",$lCadena);

	return $lCadena;
}

function fSesionIniciada()
{
	// Se comprueba si hay sesin iniciada como administrador
	if($_SESSION["admin"])
		return true;
	else
		return false;
}

function fEnviarMail($pNombreRemitente,$pEMailRemitente,$pDestinatario,$pTitulo, $pTexto1, $pTexto2,$pAdjunto)
{
	$lContenido = "";

	// Contenido 
	$lContenido.=$pTexto1;

	$lCuerpoMail = $lContenido;
	$lContenido = $lCuerpoMail;

	$lMensajeFinal = "";
	$lCabeceras = "";

	// Abrimos boundary
	$lBoundary = "----=SEPARADOR_MAIL";

	// Cabeceras comunes
	//$lCabeceras.= "From: ".imap_8bit($pNombreRemitente)." <".$pEMailRemitente.">\n";
	$lCabeceras.= "From: "."=?ISO-8859-1?Q?".imap_8bit($pNombreRemitente)."?="." <".$pEMailRemitente.">\n";
	$lCabeceras.= "Reply-To: ".$pEMailRemitente."\n";
	$lCabeceras.= "Return-Path: ".$pEMailRemitente."\n";
	$lCabeceras.= "Errors-To: ".$pEMailRemitente."\n"; // NUEVO
	$lCabeceras.= "X-Priority: 3"."\n"; // NUEVO
	$lCabeceras.= "X-Mailer: PHP/".phpversion()."\n";
	$lCabeceras.= "MIME-Version: 1.0\n";
	$lCabeceras.= "Content-Type: multipart/alternative; charset=ISO-8859-1".";\n\tboundary=".'"'.$lBoundary.'"'."\n"; // NUEVO
	$lCabeceras.= "Content-Transfer-Encoding: 7bit"."\n"; // NUEVO
	// Fin cabecera

	// Boundary para text
	$lMensajeFinal.="\nThis is a multi-part message in MIME format.\n\n";
	$lMensajeFinal.= "--".$lBoundary."\n";
	$lMensajeFinal.= "Content-Transfer-Encoding: 8bit"."\n";
	$lMensajeFinal.= "Content-Type: text/plain;\ncharset=ISO-8859-1\n\n";
	$lMensajeFinal.= $pTexto2."\n\n";
	// Fin texto

	// Boundary para mensaje html
	$lMensajeFinal.= "--".$lBoundary."\n";
	$lMensajeFinal.= "Content-Transfer-Encoding: 7bit"."\n";
	$lMensajeFinal.= "Content-Type: text/html;\ncharset=ISO-8859-1\n\n";

	$lMensajeFinal.= $lContenido."\n\n";
	//Martin 21/05/07. Comentados estos replaces porque provocaban que no se viese el contenido del correo.
//	$lMensajeFinal = str_replace("=","=\"",$lMensajeFinal);
//	$lMensajeFinal = str_replace(">","\">",$lMensajeFinal);
//	$lMensajeFinal = str_replace("=`","=\"",$lMensajeFinal);
//	$lMensajeFinal = str_replace("`>","\">",$lMensajeFinal);

	// Cerramos el boundary
	$lMensajeFinal.= "--".$lBoundary."--"."\n";

	//$lTitulo = "=?ISO-8859-1?Q?".imap_8bit($pTitulo)."?=";
	$lTitulo = "=?ISO-8859-1?Q?".imap_8bit(substr($pTitulo,0,50)).imap_8bit(substr($pTitulo,50))."?=";
	mail($pDestinatario, $lTitulo, $lMensajeFinal,$lCabeceras) or die("Ocurri&oacute;  alg&uacute;n error en el envio");
}

function fUltimoID($pTabla, $pCampo)
{
	$lCadena = "SELECT MAX(".$pCampo.") AS ultimo FROM ".$pTabla;
	$rsSelect = fQuery($lCadena);
	if(mysql_result($rsSelect, 0,"ultimo")==NULL)
		$lUltimo = 0;
	else
		$lUltimo = mysql_result($rsSelect,0,"ultimo");
	return $lUltimo;
}

function fUltimoIDMultiple($pTabla, $pCamposFijos, $pValoresFijos, $pCampo)
//  Obtenemos el valor m ximo de un campo de la clave para una tabla con una clave 
//mltiple. Habr  unos campos de la clave que tendrn un valor predeterminado 
//y habr  que obtener el valor mximo para el campo que var a. 
//  $pCamposFijos es un array con los campos fijos de la tabla.
//  $pValoresFijos es un array con los valores que tomarn los campos fijos.
//  $pCampo es el campo para el que hay que calcular el valor m ximo.
//  En $pValoresFijos los valores que no sean numricos deber n venir con las comillas 
//incluidas.
{
	$lCadena = "SELECT MAX(".$pCampo.") AS ultimo FROM ".$pTabla;
	if ((count($pCamposFijos)==0) || (count($pCamposFijos)!=count($pValoresFijos)))
	{
		$lUltimo = "Error";
	}
	else
	{
		for ($li=0;$li<count($pCamposFijos);$li++) 
		{
			if ($li==0)
			{
				$lCadena = $lCadena." WHERE ".$pCamposFijos[$li]." = ".$pValoresFijos[$li];
			}
			else
			{
				$lCadena = $lCadena." AND ".$pCamposFijos[$li]." = ".$pValoresFijos[$li];
			}
		}
		$rsSelect = fQuery($lCadena);
		if(mysql_result($rsSelect, 0,"ultimo")==NULL)
		{
			$lUltimo = 0;
		}
		else
		{
			$lUltimo = mysql_result($rsSelect,0,"ultimo");
		}
	}
	return $lUltimo;
}

function fNombreTipo($pTipo)
{
	$lCadena = "SELECT * FROM ".__TABLA_TIPOS_SERVICIOS__." WHERE idtipo = ".$pTipo;
	$rsServicios = fQuery($lCadena);
	if(mysql_num_rows($rsServicios)>0)
		$lServicio = mysql_result($rsServicios,0,"nombrec");
	else
		$lServicio = "N/D";
	
	return $lServicio;
}

function fDiaSemana($pDia,$pIdioma)
{
	$laDias[0][0] = "Domingo";
	$laDias[0][1] = "Igandea";
	$laDias[0][2] = "Domingo";
	$laDias[0][3] = "Sunday";
	$laDias[0][4] = "Domingo";
	$laDias[0][5] = "Domingo";
	$laDias[0][6] = "Domingo";

	$laDias[1][0] = "Lunes";
	$laDias[1][1] = "Astelehena";
	$laDias[1][2] = "Lunes";
	$laDias[0][3] = "Monday";
	$laDias[0][4] = "Lunes";
	$laDias[0][5] = "Lunes";
	$laDias[0][6] = "Lunes";

	$laDias[2][0] = "Martes";
	$laDias[2][1] = "Asteartea";
	$laDias[2][2] = "Martes";
	$laDias[0][3] = "Tuesday";
	$laDias[0][4] = "Martes";
	$laDias[0][5] = "Martes";
	$laDias[0][6] = "Martes";

	$laDias[3][0] = "Mircoles";
	$laDias[3][1] = "Asteazkena";
	$laDias[3][2] = "Mircoles";
	$laDias[0][3] = "Wednesday";
	$laDias[0][4] = "Mircoles";
	$laDias[0][5] = "Mircoles";
	$laDias[0][6] = "Mircoles";

	$laDias[4][0] = "Jueves";
	$laDias[4][1] = "Osteguna";
	$laDias[4][2] = "Jueves";
	$laDias[0][3] = "Thursday";
	$laDias[0][4] = "Jueves";
	$laDias[0][5] = "Jueves";
	$laDias[0][6] = "Jueves";

	$laDias[5][0] = "Viernes";
	$laDias[5][1] = "Ostirala";
	$laDias[5][2] = "Viernes";
	$laDias[0][3] = "Friday";
	$laDias[0][4] = "Viernes";
	$laDias[0][5] = "Viernes";
	$laDias[0][6] = "Viernes";

	$laDias[6][0] = "S&aacute;bado";
	$laDias[6][1] = "Larunbata";
	$laDias[6][2] = "S&aacute;bado";
	$laDias[0][3] = "Saturday";
	$laDias[0][4] = "S&aacute;bado";
	$laDias[0][5] = "S&aacute;bado";
	$laDias[0][6] = "S&aacute;bado";

	$laDias[7][0] = "Domingo";
	$laDias[7][1] = "Igandea";
	$laDias[7][2] = "Domingo";
	$laDias[0][3] = "Sunday";
	$laDias[0][4] = "Domingo";
	$laDias[0][5] = "Domingo";
	$laDias[0][6] = "Domingo";

//	if($pIdioma=="c")
//		$lIdioma = 0;
//	if($pIdioma=="e")
//		$lIdioma = 1;
//	if($pIdioma=="i")
//		$lIdioma = 2;
	//  Martin 12/11/07, Miramos si en pIdioma nos viene un literal de idioma (como se hacia en la version vieja)
	//para poner el indice adecuado, si no restamos 1 al parametro con el idioma.
	if($pIdioma=="c")
		$lIdioma = 0;
	elseif($pIdioma=="e")
		$lIdioma = 1;
	elseif($pIdioma=="i")
		$lIdioma = 2;
	else
		$lIdioma = $pIdioma - 1; 

	$lDia = $laDias[$pDia][$lIdioma];

	return $lDia;
}

function fNombreMes($pMes, $pIdioma)
{
	// Aado 10 y quito 10 para transformar "08" en "8"
	$pMes+=10;
	$pMes-=10;
	// Resto uno por indice en base 0
	$pMes-=1;
	$laMeses[0][0] = "enero";
	$laMeses[0][1] = "urtarrila";
	$laMeses[0][2] = "janvier";
	$laMeses[0][3] = "january";
	$laMeses[0][4] = "enero";
	$laMeses[0][5] = "Januar";
	$laMeses[0][6] = "Gennaio";

	$laMeses[1][0] = "febrero";
	$laMeses[1][1] = "otsaila";
	$laMeses[1][2] = "f�vrier";
	$laMeses[1][3] = "february";
	$laMeses[1][4] = "febrero";
	$laMeses[1][5] = "Februar";
	$laMeses[1][6] = "febbraio";

	$laMeses[2][0] = "marzo";
	$laMeses[2][1] = "martxoa";
	$laMeses[2][2] = "mars";
	$laMeses[2][3] = "march";
	$laMeses[2][4] = "marzo";
	$laMeses[2][5] = "M�rz";
	$laMeses[2][6] = "marzo";

	$laMeses[3][0] = "abril";
	$laMeses[3][1] = "apirila";
	$laMeses[3][2] = "avril";
	$laMeses[3][3] = "april";
	$laMeses[3][4] = "abril";
	$laMeses[3][5] = "April";
	$laMeses[3][6] = "aprile";

	$laMeses[4][0] = "mayo";
	$laMeses[4][1] = "maiatza";
	$laMeses[4][2] = "mai";
	$laMeses[4][3] = "may";
	$laMeses[4][4] = "mayo";
	$laMeses[4][5] = "Mai";
	$laMeses[4][6] = "maggio";

	$laMeses[5][0] = "junio";
	$laMeses[5][1] = "ekaina";
	$laMeses[5][2] = "juin";
	$laMeses[5][3] = "june";
	$laMeses[5][4] = "junio";
	$laMeses[5][5] = "Juni";
	$laMeses[5][6] = "giugno";

	$laMeses[6][0] = "julio";
	$laMeses[6][1] = "uztaila";
	$laMeses[6][2] = "juillet";
	$laMeses[6][3] = "july";
	$laMeses[6][4] = "julio";
	$laMeses[6][5] = "Juli";
	$laMeses[6][6] = "luglio";

	$laMeses[7][0] = "agosto";
	$laMeses[7][1] = "abuztua";
	$laMeses[7][2] = "ao�t";
	$laMeses[7][3] = "august";
	$laMeses[7][4] = "agosto";
	$laMeses[7][5] = "August";
	$laMeses[7][6] = "Agosto";

	$laMeses[8][0] = "septiembre";
	$laMeses[8][1] = "iraila";
	$laMeses[8][2] = "septembre";
	$laMeses[8][3] = "september";
	$laMeses[8][4] = "septiembre";
	$laMeses[8][5] = "September";
	$laMeses[8][6] = "settembre";

	$laMeses[9][0] = "octubre";
	$laMeses[9][1] = "urria";
	$laMeses[9][2] = "octobre";
	$laMeses[9][3] = "october";
	$laMeses[9][4] = "octubre";
	$laMeses[9][5] = "Oktober";
	$laMeses[9][6] = "ottobre";

	$laMeses[10][0] = "noviembre";
	$laMeses[10][1] = "azaroa";
	$laMeses[10][2] = "novembre";
	$laMeses[10][3] = "november";
	$laMeses[10][4] = "noviembre";
	$laMeses[10][5] = "November";
	$laMeses[10][6] = "novembre";

	$laMeses[11][0] = "diciembre";
	$laMeses[11][1] = "abendua";
	$laMeses[11][2] = "d�cembre";
	$laMeses[11][3] = "december";
	$laMeses[11][4] = "diciembre";
	$laMeses[11][5] = "Dezember";
	$laMeses[11][6] = "dicembre";

	//Martin 21/03/07, Adaptado a la nueva gestin de idiomas.
	//if($pIdioma==1) //"c"
	//	$lIdioma = 0;
	//if($pIdioma==2) //"e"
	//	$lIdioma = 1;
	//if($pIdioma==4) //"i"
	//	$lIdioma = 2;

	$lIdioma = $pIdioma - 1; 

	$lMes = $laMeses[$pMes][$lIdioma];
	
	return $lMes;
}

function fSumarUnDia($pDiaenSeg)
{
	//	Mart n 22/03/07. Dada una fecha en formato UNIX calculamos la fecha del da siguiente 
	//en formato UNIX tambi n (a las 12:00:00). 
	//	Sumar directamente los 86400 segundos que tiene un da a la fecha deber a bastar normalmente, 
	//pero cuando entre dos das hay un cambio de hora PHP lo tiene en cuenta y la diferencia 
	//en segundos es (86400+3600) o (86400-3600)

	//Obtenemos una representaci n legible de la fecha
	$laDia = getdate($pDiaenSeg);
	$lDia = $laDia["mday"];
	$lMes = $laDia["mon"];
	$lAnio = $laDia["year"];

	$lHora = $laDia["hours"];
	$lMinuto = $laDia["minutes"];
	$lSegundo = $laDia["seconds"];

	//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
	$lDia += 1;
	if (!checkdate($lMes,$lDia,$lAnio))
	{
		//Si la fecha es incorrecta es que estabamos en el  ltimo da del mes.
		$lDia = 1;
		$lMes += 1;
		if (!checkdate($lMes,$lDia,$lAnio))
		{
			//Si la fecha es incorrecta es que estabamos en el  ltimo mes del ao.
			$lMes = 1;
			$lAnio += 1;
		}
	}
	//Calculamos la marca de tiempo.
	$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
	//$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio.
	//					" ".$lHora.":".$lMinuto.":".$lSegundo.":");
	return ($lDiaSig);
}

function fRestarUnDia($pDiaenSeg)
{
	//	Mart n 22/03/07. Dada una fecha en formato UNIX calculamos la fecha del da anterior 
	//en formato UNIX tambi n (a las 12:00:00). 
	//	Restar directamente los 86400 segundos que tiene un da a la fecha deber a bastar normalmente, 
	//pero cuando entre dos das hay un cambio de hora PHP lo tiene en cuenta y la diferencia 
	//en segundos es (86400+3600) o (86400-3600)

	//Obtenemos una representaci n legible de la fecha
	$laDia = getdate($pDiaenSeg);
	$lDia = $laDia["mday"];
	$lMes = $laDia["mon"];
	$lAnio = $laDia["year"];

	$lHora = $laDia["hours"];
	$lMinuto = $laDia["minutes"];
	$lSegundo = $laDia["seconds"];

	//Calculamos la marca de tiempo de la fecha correspondiente al da anterior.
	$lDia -= 1;
	if (!checkdate($lMes,$lDia,$lAnio))
	{
		//Si la fecha es incorrecta es que estabamos en el primer d a del mes.
		//Probaremos en un bucle con los posibles ltimos d as del mes hasta hallar 
		//la fecha correcta.
		//$lDia = 31;
		$lMes -= 1;
		if ($lMes == 0)
		{
			//Si mes es 0 es que la fecha era 1 de Enero y la fecha anterior entonces 
			//es 31 de Diciembre.
			$lMes = 12;
			$lDia = 31;
			$lAnio--;
		}
		else
		{
			//Vamos probando con los posibles ltimos d as del mes.
			for ($lDia=31;(!checkdate($lMes,$lDia,$lAnio) || $lDia<28);$lDia--);
		}
	}
	//Calculamos la marca de tiempo.
	$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
	//$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio.
	//					" ".$lHora.":".$lMinuto.":".$lSegundo.":");

	return ($lDiaSig);
}

function fPonerCeros($pNumero,$pDigitos)
{
	if($pDigitos<strlen($pNumero))
		return $pNumero;
	$lCadena = $pNumero;
	for($li=0;$li<$pDigitos-strlen($pNumero);$li++)
		$lCadena="0".$lCadena;
	
	return $lCadena;
}

function sBorrarFichero($pFichero)
{
	if(file_exists($pFichero))
		return unlink($pFichero);
	else
	{
//		print($pFichero.": Fichero no encontrado ...");
		return false;
	}
}

function fComillas($pTexto)
{
	return "'".$pTexto."'";
}

function fReportarError($pCadena)
{
	print("<br>Ocurri&oacute; un error al procesar el siguiente comando: ".$pCadena."<br><br>Es posible que la hoja no se muestre correctamente");

	$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
	$lIP2 = $_SERVER["REMOTE_ADDR"];
	$lNavegador = $_SERVER["HTTP_USER_AGENT"];
	$lFechaUnix = time();
	$lFechaLegible = date("d/m/Y H:i:s",$lFechaUnix);
	$lCodigoError = mysql_errno();
	$lMensajeError = mysql_error();

	$lTexto = "";
	$lTexto.="Error en script: ".$_SERVER["REQUEST_URI"]."\n";
	$lTexto.="Referer: ".$_SERVER["HTTP_REFERER"]."\n";
	$lTexto.="IP: ".$_SERVER["REMOTE_ADDR"]."\n";
	$lTexto.="Navegador: ".$lNavegador."\n";
	$lTexto.="Fecha de la incidencia: ".$lFechaLegible."\n";
	$lTexto.="Cadena SQL: ".$pCadena."\n";
	$lTexto.="Error devuelto por MySQL: ".$lCodigoError." - ".$lMensajeError."\n";
$lTexto.="URL del script: ".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."\n";
	$lTitulo = "Error Script. (".__ROOT__.")";


	$lUltimo = fUltimoID(__TABLA_ERRORES__,"iderror");
	$lUltimo++;
	$lCadenaError = $pCadena;
	$lCadenaError = str_replace("'","�",$lCadenaError);
	$lCadenaError = str_replace("\"","�",$lCadenaError);
	$lMensajeError = str_replace("'","�",$lMensajeError);
	$lMensajeError = str_replace("\"","�",$lMensajeError);
	// Martin 28/01/08, insertamos el error en la nueva tabla rsv_errores.
	// Martin 05/02/09, Hemos cambiado el nombre del campo sql a sql_error porque si no 
	//daba error al insertar en la Base de Datos en el nuevo servidor de Ovh.
	$lCadena = "INSERT INTO ".__TABLA_ERRORES__."(".
				" iderror, ip1, ip2, navegador,".
				" fecha, cod_error, msg_error, sql_error,".
				" pagina, URL, referer".
				" ) VALUES (".
				" ".$lUltimo.
				" ,".fComillas(fLimpiar_sql($lIP1)).
				" ,".fComillas(fLimpiar_sql($lIP2)).
				" ,".fComillas(fLimpiar_sql(substr($lNavegador,0,255))).
				" ,".$lFechaUnix.
				" ,".fComillas(fLimpiar_sql($lCodigoError)).
				" ,".fComillas(fLimpiar_sql($lMensajeError)).
				" ,".fComillas(fLimpiar_sql($lCadenaError)).
				" ,".fComillas(fLimpiar_sql(substr($_SERVER["REQUEST_URI"],0,255))).
				" ,".fComillas(fLimpiar_sql($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"])).
				" ,".fComillas(fLimpiar_sql($_SERVER["HTTP_REFERER"])).
				" )";
	mysql_query($lCadena);
//	if (mysql_errno() > 0)
//	{
//		print("Error al insertar en rsv_errores: ".mysql_errno()." ".mysql_error().". lCadena=".$lCadena);
//	}

	// Martin 28/01/08, activo el envio de errores
	//mail("irodriguez@themovie.org",$lTitulo,$lTexto);
	mail("a.martin@themovie.org",$lTitulo,$lTexto);
}

function fQuery($pCadena)
{
	$rsObjeto = mysql_query($pCadena) or fReportarError($pCadena);
	return $rsObjeto;
}

function fComillasLike($pTexto)
{
	return fComillas("%".$pTexto."%");
}

function fCargarContenido($pContenido,$pIdioma)
{
	$lCadena = "SELECT * FROM ".__TABLA_CONTENIDOS__." WHERE idcontenido = ".$pContenido;
	$rsContenidos = fQuery($lCadena);
	if(mysql_num_rows($rsContenidos)>0)
		print(mysql_result($rsContenidos,0,"contenido".$pIdioma));
	else
		print("No se pudo cargar el contenido de esta p gina");
}

function fLiteral_prueba($pLiteral,$pIdioma)
{
	//include("inc_literales.php");
	
//Martin 25/09/07. Obtenemos el nombre y otros datos del establecimiento.
$lIDEstablecimiento = $_SESSION["idestablecimiento"];
$lCadena = "SELECT *".
			" FROM ".__TABLA_ESTABLECIMIENTOS__.
			" WHERE idestablecimiento = ".$lIDEstablecimiento;
$rsEstablecimiento = fQuery($lCadena);
if (mysql_num_rows($rsEstablecimiento) > 0)
{
	$lNombreEst = mysql_result($rsEstablecimiento,0,"nombre");
	$lTelefonoEst = mysql_result($rsEstablecimiento,0,"telefono");
	$lFaxEst = mysql_result($rsEstablecimiento,0,"fax");
	$lEmailEst = mysql_result($rsEstablecimiento,0,"email");
	$lBancoEst = mysql_result($rsEstablecimiento,0,"nombre_banco");
	$lTitularcuentaEst = mysql_result($rsEstablecimiento,0,"titular_cuenta");
	$lCuentaEst = mysql_result($rsEstablecimiento,0,"ncuenta");
	$lIbanEst = mysql_result($rsEstablecimiento,0,"iban");
	$lBicEst = mysql_result($rsEstablecimiento,0,"bic");
}

	$lCadena = "SELECT literal".
					" FROM ".__TABLA_LITERALES_TRADUCCION__.
					" WHERE idliteral = ".$pLiteral.
					" AND ididioma = ".$pIdioma;
	$rsLiteral = fQuery($lCadena);
	if (mysql_num_rows($rsLiteral) > 0)
	{
		$lLiteral = mysql_result($rsLiteral,0,"literal");
	}
	// Martin 23/07/08. Hago eval para sustituir apariciones de variables por su valor. En algunas 
	//frases aparecen variables para cambiar su valor por el nombre del establecimiento o su telefono, etc. 
	eval("\$lLiteral = \"$lLiteral\";");
	return ($lLiteral);
}

function fLiteral($pLiteral,$pIdioma)
{
	//include("inc_literales.php");
	
//Martin 25/09/07. Obtenemos el nombre y otros datos del establecimiento.
$lIDEstablecimiento = $_SESSION["idestablecimiento"];
$lCadena = "SELECT *".
			" FROM ".__TABLA_ESTABLECIMIENTOS__.
			" WHERE idestablecimiento = ".$lIDEstablecimiento;
$rsEstablecimiento = fQuery($lCadena);
if (mysql_num_rows($rsEstablecimiento) > 0)
{
	$lNombreEst = mysql_result($rsEstablecimiento,0,"nombre");
	$lTelefonoEst = mysql_result($rsEstablecimiento,0,"telefono");
	$lFaxEst = mysql_result($rsEstablecimiento,0,"fax");
	$lEmailEst = mysql_result($rsEstablecimiento,0,"email");
	$lBancoEst = mysql_result($rsEstablecimiento,0,"nombre_banco");
	$lTitularcuentaEst = mysql_result($rsEstablecimiento,0,"titular_cuenta");
	$lCuentaEst = mysql_result($rsEstablecimiento,0,"ncuenta");
	$lIbanEst = mysql_result($rsEstablecimiento,0,"iban");
	$lBicEst = mysql_result($rsEstablecimiento,0,"bic");
}
	// Martin 24/07/08. Asignamos la constante __DOMINIO__ a una variable. Esta variable es la que se pondra dentro 
	//de los strings en la tabla para que se interprete al sacarla por pantalla.
	$lDominioEst = __DOMINIO__;

	// Martin 23/07/08. Ahora se guardan los literales en la tabla __TABLA_LITERALES_TRADUCCION__
	$lCadena = "SELECT literal".
					" FROM ".__TABLA_LITERALES_TRADUCCION__.
					" WHERE idliteral = ".$pLiteral.
					" AND ididioma = ".$pIdioma;
	$rsLiteral = fQuery($lCadena);
	if (mysql_num_rows($rsLiteral) > 0)
	{
		$lLiteral = mysql_result($rsLiteral,0,"literal");
	}
	// Martin 23/07/08. Hago eval para sustituir apariciones de variables por su valor. En algunas 
	//frases aparecen variables para cambiar su valor por el nombre del establecimiento o su telefono, etc. 
	// Sustituyo las apariciones " (dobles comillas) por \" para evitar que provoquen errores al interpretarse 
	//como fin del string.
	$lLiteral = str_replace("\"","\\\"",$lLiteral);
	eval("\$lLiteral = \"$lLiteral\";");
	return ($lLiteral);
}

//function fLiteral_original($pLiteral,$pIdioma)
//{
//	include("inc_literales.php");
//
//	//Martn 20/03/07. Adaptado a nueva gesti n de idiomas.
//	switch($pIdioma)
//	{
//		case 1: //c - castellano
//			return $laLiterales[$pLiteral+0];
//			break;
//		case 2: //e - euskera
//			return $laLiterales[$pLiteral+1000];
//			break;
//		case 3: //f - frances
//			return $laLiterales[$pLiteral+2000];
//			break;
//		case 4: //i - ingles
//			return $laLiterales[$pLiteral+3000];
//			break;
//		case 5: //t - catalan
//			return $laLiterales[$pLiteral+4000];
//			break;
//		case 6: //de - aleman
//			return $laLiterales[$pLiteral+5000];
//			break;
//		case 7: //it - italiano
//			return $laLiterales[$pLiteral+6000];
//			break;
//	}
//}

function fString($pString,$pIdioma)
{
	$lIdioma = $pIdioma;
	if($pIdioma=="")
		$pIdioma = "c";
	$lCadena = "SELECT string".$pIdioma." FROM ".__TABLA_STRINGS__." WHERE idstring = ".$pString;
	$rsString = fQuery($lCadena);
	if(mysql_num_rows($rsString)>0)
		$lString = mysql_result($rsString,0,"string".$pIdioma);
	else
		$lString = "N/D";
	return $lString;
}

function fDescripcionLocalidad($pLocalidad, $pIdioma)
{
	//Mart n 21/03/07. Adaptado a nueva gestin de idiomas.
	//$lCadena = "SELECT * FROM ".__TABLA_LOCALIDADES__." WHERE idlocalidad = ".$pLocalidad;
	$lCadena = "SELECT * FROM ".__TABLA_LOCALIDADES__.
			" WHERE idlocalidad = ".$pLocalidad.
			" AND ididioma = ".$pIdioma;
	$rsLocalidad = fQuery($lCadena);
	if(mysql_num_rows($rsLocalidad)>0)
		//$lLocalidad = mysql_result($rsLocalidad,0,"descripcion".$pIdioma);
		$lLocalidad = mysql_result($rsLocalidad,0,"descripcion");
	else
		$lLocalidad = "N/D";

	return $lLocalidad;
}

function fDescripcionEstablecimiento($pEstablecimiento, $pIdioma)
{
//Mart n 22/03/07. Adaptado a nueva gestin de idiomas.
	//$lCadena = "SELECT * FROM ".__TABLA_ESTABLECIMIENTOS__." WHERE idestablecimiento = ".$pEstablecimiento;
	$lCadena = "SELECT *"." FROM ".__TABLA_ESTABLECIMIENTOS_NOMBRES__.
			" WHERE idestablecimiento = ".$pEstablecimiento.
			" AND ididioma = ".$pIdioma;
	$rsEstablecimiento = fQuery($lCadena);
	if(mysql_num_rows($rsEstablecimiento)>0)
		//$lEstablecimiento = mysql_result($rsEstablecimiento,0,"descripcion".$pIdioma);
		$lEstablecimiento = mysql_result($rsEstablecimiento,0,"nombre");
	else
		$lEstablecimiento = "N/D";

	return $lEstablecimiento;
}

function fEMailEstablecimiento($pEstablecimiento)
{
	$lCadena = "SELECT * FROM ".__TABLA_ESTABLECIMIENTOS__." WHERE idestablecimiento = ".$pEstablecimiento;
	$rsEstablecimiento = fQuery($lCadena);
	if(mysql_num_rows($rsEstablecimiento)>0)
		$lMail = mysql_result($rsEstablecimiento,0,"email");
	else
		$lMail = "N/D";

	return $lMail;
}

function fDescripcionTipoEstablecimiento($pTipo,$pIdioma)
{
	$lCadena = "SELECT * FROM ".__TABLA_ESTABLECIMIENTOS_TIPOS__." WHERE idtipo = ".$pTipo;
	$rsTipo = fQuery($lCadena);
	if(mysql_num_rows($rsTipo)>0)
		$lTipo = mysql_result($rsTipo,0,"descripcion".$pIdioma);
	else
		$lTipo = "N/D";

	return $lTipo;
}

function fDescripcionTipoHabitacion($pTipo,$pIdioma)
{
	//Mart n 21/03/07. Adaptado a nueva gestin de idiomas.
	//$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_TIPOS__." WHERE idtipo = ".$pTipo." AND borrado = 0";
	$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_TIPOS__.
				" WHERE idtipo = ".$pTipo.
				" AND ididioma = ".$pIdioma.
				" AND borrado = 0";
	$rsTipos = fQuery($lCadena);
	if(mysql_num_rows($rsTipos)>0)
		//$lTipo = mysql_result($rsTipos,0,"descripcion".$pIdioma);
		$lTipo = mysql_result($rsTipos,0,"descripcion");
	else
		$lTipo = "N/D";
	return $lTipo;
}

function fNombreTipoHabitacion($pTipo,$pIdioma)
{
	//Mart n 21/03/07. Adaptado a nueva gestin de idiomas.
	//$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_TIPOS__." WHERE idtipo = ".$pTipo." AND borrado = 0";
	$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_TIPOS__.
				" WHERE idtipo = ".$pTipo.
				" AND ididioma = ".$pIdioma.
				" AND borrado = 0";
	$rsTipos = fQuery($lCadena);
	if(mysql_num_rows($rsTipos)>0)
		//$lTipo = mysql_result($rsTipos,0,"descripcion".$pIdioma);
		$lTipo = mysql_result($rsTipos,0,"nombre");
	else
		$lTipo = "N/D";
	return $lTipo;
}

function fDescripcionTipoInstalacion($pTipo,$pIdioma)
{
	$lCadena = "SELECT * FROM ".__TABLA_INSTALACIONES_TIPOS__." WHERE idtipo = ".$pTipo." AND borrado = 0";
	$rsTipos = fQuery($lCadena);
	if(mysql_num_rows($rsTipos)>0)
		$lTipo = mysql_result($rsTipos,0,"descripcion".$pIdioma);
	else
		$lTipo = "N/D";
	return $lTipo;
}

function fDescripcionInstalacion($pInstalacion,$pIdioma)
{
	$lCadena = "SELECT * FROM ".__TABLA_INSTALACIONES__." WHERE idinstalacion = ".$pInstalacion." AND borrado = 0";
	$rsInstalaciones = fQuery($lCadena);
	if(mysql_num_rows($rsInstalaciones)>0)
		$lInstalacion = mysql_result($rsInstalaciones,0,"descripcion".$pIdioma);
	else
		$lInstalacion = "N/D";
	return $lInstalacion;
}

function fDescripcionTipoTarjeta($pTipo,$pIdioma)
{
	$lCadena = "SELECT * FROM ".__TABLA_TARJETAS__." WHERE idtarjeta = ".$pTipo." AND borrado = 0";
	$rsTipos = fQuery($lCadena);
	if(mysql_num_rows($rsTipos)>0)
		$lTipo = mysql_result($rsTipos,0,"descripcion".$pIdioma);
	else
		$lTipo = "N/D";
	return $lTipo;
}

function fIDProvincia($pLocalidad)
{
	$lCadena = "SELECT idprovincia FROM ".__TABLA_LOCALIDADES__." WHERE idlocalidad = ".$pLocalidad;
	$rsProvincia = fQuery($lCadena);
	if(mysql_num_rows($rsProvincia)>0)
		$lID = mysql_result($rsProvincia,0,"idprovincia");
	else
		$lID = 0;
	
	return $lID;
}

function fDescripcionProvincia($pProvincia,$pIdioma)
{
	//Mart n 21/03/07. Adaptado a nueva gestin de idiomas.
	//$lCadena = "SELECT * FROM ".__TABLA_PROVINCIAS__." WHERE idprovincia = ".$pProvincia;
	$lCadena = "SELECT * FROM ".__TABLA_PROVINCIAS__.
			" WHERE idprovincia = ".$pProvincia.
			" AND ididioma = ".$pIdioma;
	$rsProvincia = fQuery($lCadena);
	if(mysql_num_rows($rsProvincia)>0)
		//$lDescripcion = mysql_result($rsProvincia,0,"descripcion".$pIdioma);
		$lDescripcion = mysql_result($rsProvincia,0,"descripcion");
	else
		$lDescripcion = "N/D";
	
	return $lDescripcion;
}

function fNombreTemporada($pIdTemporada,$pBorrado=0,$pVisible=1)
{
	$lCadena = "SELECT * FROM ".__TABLA_TEMPORADAS__.
				" WHERE idtemporada = ".$pIdTemporada.
				" AND borrado = ".$pBorrado.
				" AND visible = ".$pVisible;
	$rsTemporada = fQuery($lCadena);
	if (mysql_num_rows($rsTemporada)>0) 
	{
		$lNombre = mysql_result($rsTemporada,0,"nombre");
	}
	else 
	{
		$lNombre = "N/D";
	}
	return $lNombre;
}

function fDiasMes($pMes,$pAnio)
{
	//Martin 10/01/08, Nueva forma de calcular los dias de un mes
	if (checkdate($pMes,31,$pAnio))
	{
		$lDiasMes = 31;
	}
	elseif (checkdate($pMes,30,$pAnio))
	{
		$lDiasMes = 30;
	}
	elseif (checkdate($pMes,29,$pAnio))
	{
		$lDiasMes = 29;
	}
	else
	{
		$lDiasMes = 28;
	}
	return ($lDiasMes);

/*	$lMes = $pMes;
	$lMes+=10;
	$lMes-=10;

	switch ($lMes)
	{
		case 1:
			$lDiasMes = 31;
			break;
		case 2:
			if(gmp_div_r($pAnio,4)==0)
				$lDiasMes = 29;
			else
				$lDiasMes = 28;
			break;
		case 3:
			$lDiasMes = 31;
			break;
		case 4:
			$lDiasMes = 30;
			break;
		case 5:
			$lDiasMes = 31;
			break;
		case 6:
			$lDiasMes = 30;
			break;
		case 7:
			$lDiasMes = 31;
			break;
		case 8:
			$lDiasMes = 31;
			break;
		case 9:
			$lDiasMes = 30;
			break;
		case 10:
			$lDiasMes = 31;
			break;
		case 11:
			$lDiasMes = 30;
			break;
		case 12:
			$lDiasMes = 31;
			break;
	}

	return $lDiasMes;*/
}

function fSumarMeses($pMesenSeg,$lNumMeses)
{
	//	Martin 22/03/07. Dada una fecha en formato UNIX calculamos la fecha "lNumMeses" 
	//mesee despues en formato UNIX tambien ( del dia 1 a las 12:00:00). 

	//Obtenemos una representaci n legible de la fecha
	$laMes = getdate($pMesenSeg);
	$lDia = $laMes["mday"];
	$lMes = $laMes["mon"];
	$lAnio = $laMes["year"];

	$lHora = $laMes["hours"];
	$lMinuto = $laMes["minutes"];
	$lSegundo = $laMes["seconds"];

	//Calculamos la marca de tiempo.
	$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");

	if ($lNumMeses <= 0)
	{
		$lNumMeses = 0;
	}
	else
	{
		for ($li=0;$li<$lNumMeses;$li++)
		{
			//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
			$lMes += 1;
			if (!checkdate($lMes,$lDia,$lAnio))
			{
				//Si la fecha es incorrecta es que estabamos en el ultimo mes del anio.
				$lMes = 1;
				$lAnio += 1;
			}
			//Calculamos la marca de tiempo.
			$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
		}
	}
	return ($lMesSig);
}

function fRestarMeses($pMesenSeg,$lNumMeses)
{
	//	Martin 22/03/07. Dada una fecha en formato UNIX calculamos la fecha "lNumMeses" 
	//mesee despues en formato UNIX tambien ( del dia 1 a las 12:00:00). 

	//Obtenemos una representaci n legible de la fecha
	$laMes = getdate($pMesenSeg);
	$lDia = $laMes["mday"];
	$lMes = $laMes["mon"];
	$lAnio = $laMes["year"];

	$lHora = $laMes["hours"];
	$lMinuto = $laMes["minutes"];
	$lSegundo = $laMes["seconds"];

	//Calculamos la marca de tiempo.
	$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");

	if ($lNumMeses <= 0)
	{
		$lNumMeses = 0;
	}
	else
	{
		for ($li=0;$li<$lNumMeses;$li++)
		{
			//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
			$lMes -= 1;
			if (!checkdate($lMes,$lDia,$lAnio))
			{
				//Si la fecha es incorrecta es que estabamos en el primer mes del anio.
				$lMes = 12;
				$lAnio -= 1;
			}
			//Calculamos la marca de tiempo.
			$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
		}
	}
	return ($lMesSig);
}

function fSumarUnMes($pMesenSeg)
{
	//	Martin 22/03/07. Dada una fecha en formato UNIX calculamos la fecha del mes siguiente 
	//en formato UNIX tambien ( del dia 1 a las 12:00:00). 

	//Obtenemos una representaci n legible de la fecha
	$laMes = getdate($pMesenSeg);
	$lDia = $laMes["mday"];
	$lMes = $laMes["mon"];
	$lAnio = $laMes["year"];

	$lHora = $laMes["hours"];
	$lMinuto = $laMes["minutes"];
	$lSegundo = $laMes["seconds"];

	//Calculamos la marca de tiempo de la fecha correspondiente al da siguiente.
	$lMes += 1;
	if (!checkdate($lMes,$lDia,$lAnio))
	{
		//Si la fecha es incorrecta es que estabamos en el ultimo mes del anio.
		$lMes = 1;
		$lAnio += 1;
	}
	//Calculamos la marca de tiempo.
	$lMesSig = strtotime($lMes."/".$lDia."/".$lAnio." 12:00:00");
	//$lDiaSig = strtotime($lMes."/".$lDia."/".$lAnio.
	//					" ".$lHora.":".$lMinuto.":".$lSegundo.":");
	return ($lMesSig);
}

function fPermiso($pSeccion)
{
	// Martin 07/02/08, Si el _SESSION[userid] es nulo comparamos con '' para evitar que de error la select
	$lCadena = "SELECT * FROM ".__TABLA_PERMISOS__." WHERE idusuario = ".(($_SESSION["userid"]!="")?$_SESSION["userid"]:fComillas(""));
	//$lCadena = "SELECT * FROM ".__TABLA_PERMISOS__." WHERE idusuario = ".$_SESSION["userid"];
	$rsPermiso = fQuery($lCadena);
	if(mysql_num_rows($rsPermiso)>0)
		if(strstr(mysql_result($rsPermiso,0,"secciones"),",".$pSeccion.",")||mysql_result($rsPermiso,0,"secciones")=="*")
			return true;
		else
			return false;
	else
		return false;
}

function fTipoEstablecimiento ($pUsuario)
{
	//Martin 13/03/07. Esta funci n devuelve el tipo de establecimiento asignado a un usuario.
	//  El idtipo 1 ser un Hotel y el idtipo 2 ser  un Apartamento.
	$lCadena = "SELECT est.idtipo idtipo_establecimiento".
				" FROM rsv_usuarios usu, rsv_establecimientos est".
				" WHERE usu.idestablecimiento = est.idestablecimiento".
				" AND usu.idusuario = ".$pUsuario;
	$rsTipo = fQuery($lCadena);
	if (mysql_num_rows($rsTipo)>0)
	{
		$lIdTipo = mysql_result($rsTipo,0,"idtipo_establecimiento");
	}
	else 
	{
		$lIdTipo = 0;
	}
	return ($lIdTipo);
}

function fCalcularHabitacionesDisponiblesNormal($pEstablecimiento, $pFechaInicial, $pFechaFinal)
{
	//  Martin 14/01/08. Esta funcion se usara en los establecimientos de tipo "hotel" para calcular las habitaciones asignadas
	//durante un periodo determinado pero sin tener en cuenta las asignaciones que se hayan hecho en el calendario de detalle 
	//de disponibilidad.
	//  Se devolver� una matriz asociativa en la que cada elemento ser� la fecha de cada dia del periodo en formato UNIX. 
	//A cada elemento se le asignar� otra matriz asociativa con un elemento por cada tipo de habitaci�n mas un elemento 
	//"total". A cada uno de estos elementos se le asignar� el n� de habitaciones asignadas y al elemento "total" el n� 
	//total de habitaciones asignadas de todos los tipos ese dia.
	//  Para acceder al n� de habitaciones asignadas a un tipo de habitacion en una fecha: [fecha][tipohabitacion]
	//  Para acceder al n� de habitaciones total asignadas en una fecha: [fecha]["total"]
	//  Para calcular el n� de habitaciones de un tipo se tiene en cuenta si a la fecha le afecta alguna temporada y se ha 
	//modificado el n� de habitaciones asignadas en ella.

	$lIDEstablecimiento = $pEstablecimiento;

	$lFechaDesde = $pFechaInicial;
	$lFechaHasta = $pFechaFinal;
	$lError = 0;

	//Obtenemos los tipos de habitacion del establecimiento.
	//Martin 07/01/08, Solo tenemos en cuenta las habitaciones que estan marcadas como visibles.
	$lCadena = "SELECT idtipo, idestablecimiento".
			" FROM ".__TABLA_HABITACIONES_TIPOS__.
			" WHERE idestablecimiento = ".$lIDEstablecimiento.
			" AND visible = 1".
			" GROUP BY idtipo, idestablecimiento";
	$rsTiposHab = fQuery($lCadena);
	$lNumTiposHab = mysql_num_rows($rsTiposHab);
	if ($lNumTiposHab > 0)
	{
		//Creamos la matriz de tipos de habitacion, que se asignara despues a cada fecha del periodo.
		$laTiposHab = array();
		for ($li=0;$li<$lNumTiposHab;$li++)
		{
			$lIdTipoHab = mysql_result($rsTiposHab,$li,"idtipo");
			$laTiposHab[$lIdTipoHab] = "";
		}
		$laTiposHab["total"] = "";
	}

	$laAsignadosPeriodo = array();
	for ($li=$lFechaDesde;$li<=$lFechaHasta;$li=fSumarUnDia($li))
	{
		$laAsignadosPeriodo[$li] = $laTiposHab;
		$lTotalHab = 0;
		foreach ($laAsignadosPeriodo[$li] as $lIdTipoHabActual => $lNumTipoHabAct)
		{
			if ($lIdTipoHabActual != "total")
			{
					//Miramos si la fecha actual est� incluida en una temporada y si para esa temporada se 
					//ha asinado un n� de habitaciones al tipo de habitaci�n.
					$lCadena = "SELECT hab_temp.habitaciones".
						" FROM ".__TABLA_HABITACIONES_TEMPORADAS__." AS hab_temp".
						" ,".__TABLA_TEMPORADAS_FECHAS__." AS temp_fech".
						" ,".__TABLA_TEMPORADAS__." AS temp".
						" WHERE temp.idtemporada = temp_fech.idtemporada".
						" AND temp.idtemporada = hab_temp.idtemporada".
						" AND temp.idestablecimiento = ".$lIDEstablecimiento.
						" AND temp_fech.fecha = ".$li.
						" AND hab_temp.idtipo = ".$lIdTipoHabActual;
					$rsHabTemp = fQuery($lCadena);
					$lNumHabTemp = mysql_num_rows($rsHabTemp);
					if ($lNumHabTemp > 0)
					{
						$lNumHabitaciones = mysql_result($rsHabTemp,0,"habitaciones");
						//$laAsignadosPeriodo[$li][$lIdTipoHabActual] = $lNumHabitaciones;
						//$lTotalHab += $lNumHabitaciones;
					}
					else
					{
						//Si la fecha actual no estaba incluida en ninguan temporada buscamos el n� de habitaciones 
						//asignadas por defecto.
						$lCadena = "SELECT hab_est.habitaciones".
							" FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__." AS hab_est".
							" WHERE hab_est.idtipo = ".$lIdTipoHabActual;
						$rsHab = fQuery($lCadena);
						$lNumHab = mysql_num_rows($rsHab);
						if ($lNumHab > 0)
						{
							$lNumHabitaciones = mysql_result($rsHab,0,"habitaciones");
							//$laAsignadosPeriodo[$li][$lIdTipoHabActual] = $lNumHabitaciones;
							//$lTotalHab += $lNumHabitaciones;
						}
					} //if ($lNumHabTemp > 0)...
				$laAsignadosPeriodo[$li][$lIdTipoHabActual] = $lNumHabitaciones;
				$lTotalHab += $lNumHabitaciones;
			} //if ($lIdTipoHabActual != "total")...
		} //foreach ($laAsignadosPeriodo[$li] as $lIdTipoHabActual => $lNumTipoHabAct)...

		//Asignamos el total
		$laAsignadosPeriodo[$li]["total"] = $lTotalHab;

	} //for ($li=$lFechaDesde;$li<=$lFechaHasta;$li=fSumarUnDia($li))

	return($laAsignadosPeriodo);
} // fin fCalcularHabitacionesDisponiblesNormal($pEstablecimiento, $pFechaInicial, $pFechaFinal)

function fCalcularHabitacionesAsignadas($pEstablecimiento, $pFechaInicial, $pFechaFinal, $pTipo = "1", 
						$pConsiderarVisible = "1", $pIdIdioma = '1', $pConsiderarExcep = '1') {
	//  Martin 16/10/07. Esta funcion se usara en los establecimientos de tipo "hotel" para calcular cuantas habitaciones 
	//se han asignado en un periodo determinado. 
	//  Se devolver� una matriz asociativa en la que cada elemento ser� la fecha de cada dia del periodo en formato UNIX. 
	//A cada elemento se le asignar� otra matriz asociativa con un elemento por cada tipo de habitaci�n mas un elemento 
	//"total". A cada uno de estos elementos se le asignar� el n� de habitaciones asignadas y al elemento "total" el n� 
	//total de habitaciones asignadas de todos los tipos ese dia.
	//  Para acceder al n� de habitaciones asignadas a un tipo de habitacion en una fecha: [fecha][tipohabitacion]
	//  Para acceder al n� de habitaciones total asignadas en una fecha: [fecha]["total"]
	//  Para calcular el n� de habitaciones de un tipo se tiene en cuenta si a la fecha le afecta alguna temporada y se ha 
	//modificado el n� de habitaciones asignadas en ella.
	// 	Martin 24/05/10. Ponemos un parametro nuevo $pTipo para tener en cuenta solo las habitaciones del tipo adecuado 
	//segun el establecimiento sea un hotel o de Otros servicios.
	//	20120209 MA. Creamos un parametro $pConsiderarVisible. Lo usamos para ver si tenemos que tener en cuenta el campo 
	//visible de la tabla __TABLA_HABITACIONES_TIPOS__. Esta funcion se puede usar en el modulo de reservas, donde hay que 
	//tenerlo en cuenta, o en el administrador, donde no debera tenerse en cuenta.
	//	20130605 MA. Creado parametro $pIdIdioma para usarlo al seleccionar el tipo de habitacion, por defecto vale '1' (espa�ol).
	//	20130606 MA. Creado parametro $pConsiderarExcep para tener en cuenta las excepciones o no. Si vale 1 (valor por defecto), 
	//se tienen en cuenta las excepciones, si no, no.
	//
	// Estructutura del array $laAsignadosPeriodo.
	// $laAsignadosPeriodo -> array cuyos indices son fechas en formato unix (es la fecha de un dia a las 12:00). El primer 
	//				indice es $pFechaInicial y el ultimo $pFechaFinal.
	// $laAsignadosPeriodo[fechaunix] -> Contiene un array con la disponibilidad de cada tipo de habitacion. 
	// $laAsignadosPeriodo[fechaunix][idtipohab] -> Numero de habitaciones disponibles en la fecha 'fechaunix' del tipo 
	//				de habitacion 'idtipohab'. Hay un indice 'total' para poner el numero de habitaciones disponibles de todas 
	//				las habitaciones.

	$lIDEstablecimiento = $pEstablecimiento;

	$lFechaDesde = $pFechaInicial;
	$lFechaHasta = $pFechaFinal;
	$lError = 0;

	$laAsignadosPeriodo = array();

	// 20130604 MA. Obtenemos los tipos de habitacion del establecimiento y el numero de habitaciones disponibles en la temporada 
	//normal. Inicializamos el array $laAsignadosPeriodo con las habitaciones disponibles.
	$lCadena = 'SELECT hab_tipo.idtipo, hab_tipo.idestablecimiento'.
					', hab_est.habitaciones'.
					' FROM '.__TABLA_HABITACIONES_TIPOS__.' hab_tipo'.
					', '.__TABLA_HABITACIONES_ESTABLECIMIENTOS__.' hab_est'.
					' WHERE hab_tipo.idtipo = hab_est.idtipo'.
					' AND hab_tipo.idestablecimiento = '.fLimpiar_sql($lIDEstablecimiento).
					' AND hab_tipo.ididioma = '.fLimpiar_sql($pIdIdioma).
					' AND hab_tipo.tipo = '.$pTipo.
					' AND hab_tipo.borrado = 0';
	if ($pConsiderarVisible == '1') {
		$lCadena .= ' AND hab_tipo.visible = 1';
	}
	$lCadena .= ' ORDER BY hab_tipo.orden';
//print('En dis2011/inc/app/inc_funciones.php. fCalcularHabitacionesAsignadas. Lin. 1527. $lCadena='.$lCadena."\n\n");
	$rsTiposHab = fQuery($lCadena);
	if ($rsTiposHab) {
		$lNumTiposHab = mysql_num_rows($rsTiposHab);
		$laTiposHab = array();
		$lNumHabitacionesTotal = 0;
		for ($li=0;$li<$lNumTiposHab;$li++) {
			$laTiposHabAct = mysql_fetch_array($rsTiposHab);
			$lIdTipoHabAct = $laTiposHabAct['idtipo'];
			$lNumHabitacionesAct = $laTiposHabAct['habitaciones'];
			$laTiposHab[$lIdTipoHabAct] = $lNumHabitacionesAct;
			$lNumHabitacionesTotal += $lNumHabitacionesAct;
		}
		$laTiposHab['total'] = $lNumHabitacionesTotal;

		// 20130604 MA. Inicializamos $laAsignadosPeriodo con los valores que tendra por defecto cada dia.
		for ($li=$lFechaDesde;$li<=$lFechaHasta;$li=fSumarUnDia($li)) {
			$laAsignadosPeriodo[$li] = $laTiposHab;
		}
	} //fin if ($rsTiposHab) {...

	// 20130604 MA. Creamos un array $laExcepPeriodo, en el que guardamos las excepciones definidas por cada dia del periodo 
	//y tipo de habitacion. 
	// $laExcepPeriodo -> Los indices del array seran los dias del perido para los que se haya definido una excepcion.
	// $laExcepPeriodo[fechaunix] -> Contiene un array con el numero de habitaciones disponibles por cada tipo de habitacion.
	// $laExcepPeriodo[fechaunix][idtipohab] -> Numero de habitaciones definidas en 'fechaunix' para el 'idtipohab'.
	$laExcepPeriodo = array();
	if ($pConsiderarExcep == '1') {
		$lCadena = 'SELECT hab_exc.idtipo, hab_exc.fecha, hab_exc.habitaciones'.
						' FROM '.__TABLA_HABITACIONES_TIPOS__.' hab_tipo'.
						', '.__TABLA_HABITACIONES_ESTABLECIMIENTOS_EXCEPCIONES__.' hab_exc'.
						' WHERE hab_tipo.idtipo = hab_exc.idtipo'.
						' AND hab_tipo.idestablecimiento = '.fLimpiar_sql($lIDEstablecimiento).
						' AND hab_tipo.ididioma = '.fLimpiar_sql($pIdIdioma).
						' AND hab_exc.fecha BETWEEN '.$lFechaDesde.' AND '.$lFechaHasta.
						' AND hab_tipo.tipo = '.$pTipo.
						' AND hab_tipo.borrado = 0';
		if ($pConsiderarVisible == '1') {
			$lCadena .= ' AND hab_tipo.visible = 1';
		}
//print('En dis2011/inc/app/inc_funciones.php. fCalcularHabitacionesAsignadas. Lin. 1566. $lCadena='.$lCadena."\n\n");
		$rsExcep = fQuery($lCadena);
		if ($rsExcep) {
			while ($laExcepAct = mysql_fetch_array($rsExcep)) {
				$lExcepIdtipoAct = $laExcepAct['idtipo'];
				$lExcepFechaAct = $laExcepAct['fecha'];
				$lExcepHabitacionesAct = $laExcepAct['habitaciones'];
				if (!array_key_exists($lExcepFechaAct,$laExcepPeriodo)) {
					$laExcepPeriodo[$lExcepFechaAct] = array();
				}
				$laExcepPeriodo[$lExcepFechaAct][$lExcepIdtipoAct] = $lExcepHabitacionesAct;
			}
		} //fin if ($rsExcep) {...
	} //fin if ($pConsiderarExcep == '1') {...

	// 20130605 MA. Creamos un array $laTempPeriodo, en el que guardamos las habitaciones definidas en las fechas que coincidan 
	//con alguna temporada.
	// $laTempPeriodo -> Los indices del array seran los dias del periodo que coincidan con una temporada.
	// $laTempPeriodo[fechaunix] -> Contiene un array con el numero de habitaciones disponibles por cada tipo de habitacion.
	// $laTempPeriodo[fechaunix][idtipohab] -> Numero de habitaciones definidas en 'fechaunix' para el 'idtipohab', que ademas 
	//		no tengan definida una excepcion.
	$lCadena = 'SELECT hab_temp.idtipo, temp_fech.fecha, hab_temp.habitaciones'.
					' FROM '.__TABLA_HABITACIONES_TIPOS__.' hab_tipo'.
					', '.__TABLA_HABITACIONES_TEMPORADAS__.' hab_temp'.
					', '.__TABLA_TEMPORADAS_FECHAS__.' temp_fech'.
					' WHERE hab_tipo.idtipo = hab_temp.idtipo'.
					' AND hab_tipo.idestablecimiento = hab_temp.idestablecimiento'.
					' AND hab_temp.idestablecimiento = temp_fech.idestablecimiento'.
					' AND hab_temp.idtemporada = temp_fech.idtemporada'.
					' AND hab_tipo.idestablecimiento = '.fLimpiar_sql($lIDEstablecimiento).
					' AND hab_tipo.ididioma = '.fLimpiar_sql($pIdIdioma).
					' AND temp_fech.fecha BETWEEN '.$lFechaDesde.' AND '.$lFechaHasta.
					' AND hab_tipo.tipo = '.$pTipo.
					' AND hab_tipo.borrado = 0';
	if ($pConsiderarVisible == '1') {
		$lCadena .= ' AND hab_tipo.visible = 1';
	}
	if ($pConsiderarExcep == '1') {
		$lCadena .= ' AND NOT EXISTS ('.
						'    SELECT hab_exc.idtipo, hab_exc.fecha, hab_exc.habitaciones'.
						'    FROM '.__TABLA_HABITACIONES_ESTABLECIMIENTOS_EXCEPCIONES__.' hab_exc'.
						'    WHERE hab_exc.idtipo = hab_temp.idtipo'.
						'    AND hab_exc.fecha = temp_fech.fecha'.
						' )';
	} //fin if ($pConsiderarExcep == '1') {...
//print('En dis2011/inc/app/inc_funciones.php. fCalcularHabitacionesAsignadas. Lin. 1612. $lCadena='.$lCadena."\n\n");
	$rsTemp = fQuery($lCadena);
	$laTempPeriodo = array();
	if ($rsTemp) {
		while ($laTempAct = mysql_fetch_array($rsTemp)) {
			$lTempIdtipoAct = $laTempAct['idtipo'];
			$lTempFechaAct = $laTempAct['fecha'];
			$lTempHabitacionesAct = $laTempAct['habitaciones'];
			if (!array_key_exists($lTempFechaAct,$laTempPeriodo)) {
				$laTempPeriodo[$lTempFechaAct] = array();
			}
			$laTempPeriodo[$lTempFechaAct][$lTempIdtipoAct] = $lTempHabitacionesAct;
		}
	} //fin if ($rsTemp) {...

	// 20130606 MA. Recorremos los arrays laExcepPeriodo y $laTempPeriodo para sobreescribir el array $laAsignadosPeriodo 
	//con las habitaciones disponibles en las excepciones y temporadas.
	foreach ($laExcepPeriodo as $lFechaAct => $laTiposHabAct) {
		foreach ($laTiposHabAct as $lIdtipoAct => $lNumHabAct) {
			$lNumHabAnt = $laAsignadosPeriodo[$lFechaAct][$lIdtipoAct];
			$laAsignadosPeriodo[$lFechaAct][$lIdtipoAct] = $lNumHabAct;
			$laAsignadosPeriodo[$lFechaAct]['total'] = $laAsignadosPeriodo[$lFechaAct]['total'] - $lNumHabAnt + $lNumHabAct;
		}
	} //fin foreach ($laExcepPeriodo as $lFechaAct => $laTiposHabAct) {...
	foreach ($laTempPeriodo as $lFechaAct => $laTiposHabAct) {
		foreach ($laTiposHabAct as $lIdtipoAct => $lNumHabAct) {
			$lNumHabAnt = $laAsignadosPeriodo[$lFechaAct][$lIdtipoAct];
			$laAsignadosPeriodo[$lFechaAct][$lIdtipoAct] = $lNumHabAct;
			$laAsignadosPeriodo[$lFechaAct]['total'] = $laAsignadosPeriodo[$lFechaAct]['total'] - $lNumHabAnt + $lNumHabAct;
		}
	}

	return $laAsignadosPeriodo;

} // fin fCalcularHabitacionesAsignadas($pEstablecimiento, $pFechaInicial, $pFechaFinal)

function fCalcularHabitacionesReservadas($pEstablecimiento, $pFechaInicial, $pFechaFinal, $pTipo = "1", 
						$pConsiderarVisible = "1", $pIdIdioma = '1')
{
	//  Martin 16/10/07. Esta funcion se usara en los establecimientos de tipo "hotel" para calcular cuantas habitaciones 
	//se han reservado en un periodo determinado. 
	//  Se devolver� una matriz asociativa en la que cada elemento ser� la fecha de cada dia del periodo en formato UNIX. 
	//A cada elemento se le asignar� otra matriz asociativa con un elemento por cada tipo de habitaci�n mas un elemento 
	//"total". A cada uno de estos elementos se le asignar� el n� de habitaciones reservadas y al elemento "total" el n� 
	//total de habitaciones reservadas de todos los tipos ese dia.
	//  Para acceder al n� de habitaciones reservadas a un tipo de habitacion en una fecha: [fecha][tipohabitacion]
	//  Para acceder al n� de habitaciones total reservadas en una fecha: [fecha]["total"]
	// 	Martin 24/05/10. Ponemos un parametro nuevo $pTipo para tener en cuenta solo las habitaciones del tipo adecuado 
	//segun el establecimiento sea un hotel o de Otros servicios.
	//	20130605 MA. Creado parametro $pIdIdioma para usarlo al seleccionar el tipo de habitacion, por defecto vale '1' (espa�ol).
	//	20120209 MA. Creamos un parametro $pConsiderarVisible. Lo usamos para ver si tenemos que tener en cuenta el campo 
	//visible de la tabla __TABLA_HABITACIONES_TIPOS__. Esta funcion se puede usar en el modulo de reservas, donde hay que 
	//tenerlo en cuenta, o en el administrador, donde no debera tenerse en cuenta.
	//
	// Estructutura del array $laAsignadosPeriodo.
	// $laReservadosPeriodo -> array cuyos indices son fechas en formato unix (es la fecha de un dia a las 12:00). El primer 
	//				indice es $pFechaInicial y el ultimo $pFechaFinal.
	// $laReservadosPeriodo[fechaunix] -> Contiene un array con las reservas de cada tipo de habitacion. 
	// $laReservadosPeriodo[fechaunix][idtipohab] -> Numero de habitaciones reservadas en la fecha 'fechaunix' del tipo 
	//				de habitacion 'idtipohab'. Hay un indice 'total' para poner el numero de habitaciones reservadas de todas 
	//				las habitaciones.

	$lIDEstablecimiento = $pEstablecimiento;

	$lFechaDesde = $pFechaInicial;
	$lFechaHasta = $pFechaFinal;
	$lError = 0;

	$laReservadosPeriodo = array();

	// 20130604 MA. Obtenemos los tipos de habitacion del establecimiento e inicializamos el array $laReservadosPeriodo
	$lCadena = 'SELECT hab_tipo.idtipo, hab_tipo.idestablecimiento'.
					' FROM '.__TABLA_HABITACIONES_TIPOS__.' hab_tipo'.
					' WHERE hab_tipo.idestablecimiento = '.fLimpiar_sql($lIDEstablecimiento).
					' AND hab_tipo.ididioma = '.fLimpiar_sql($pIdIdioma).
					' AND hab_tipo.tipo = '.$pTipo.
					' AND hab_tipo.borrado = 0';
	if ($pConsiderarVisible == '1') {
		$lCadena .= ' AND hab_tipo.visible = 1';
	}
	$lCadena .= ' ORDER BY hab_tipo.orden';
	$rsTiposHab = fQuery($lCadena);
	if ($rsTiposHab) {
		$lNumTiposHab = mysql_num_rows($rsTiposHab);
		$laTiposHab = array();
		for ($li=0;$li<$lNumTiposHab;$li++) {
			$laTiposHabAct = mysql_fetch_array($rsTiposHab);
			$lIdTipoHabAct = $laTiposHabAct['idtipo'];
			$laTiposHab[$lIdTipoHabAct] = 0;
		}
		$laTiposHab['total'] = 0;

		for ($li=$lFechaDesde;$li<=$lFechaHasta;$li=fSumarUnDia($li)) {
			$laReservadosPeriodo[$li] = $laTiposHab;
		}
	} //fin if ($rsTiposHab) {...

	// 20130607 MA. Seleccionamos las reservas hechas en el periodo.
	$lCadena = '(SELECT rsv.idtipo, rsv.fechallegada, rsv.fechasalida'.
					', SUM(rsv.habitaciones) habitaciones'.
					' FROM '.__TABLA_RESERVAS__.' AS rsv'.
					', '.__TABLA_HABITACIONES_TIPOS__.' AS hab_tipo'.
					' WHERE rsv.idtipo = hab_tipo.idtipo'.
					' AND hab_tipo.ididioma = '.fLimpiar_sql($pIdIdioma).
					' AND hab_tipo.tipo = '.$pTipo.
					' AND hab_tipo.borrado = 0';
	if ($pConsiderarVisible == '1') {
		$lCadena .= ' AND hab_tipo.visible = 1';
	}
	$lCadena .= ' AND rsv.idestablecimiento = '.fLimpiar_sql($lIDEstablecimiento).
					' AND rsv.idtipo != -1'.
					' AND rsv.fechallegada < '.fLimpiar_sql($pFechaFinal).
					' AND rsv.fechasalida > '.fLimpiar_sql($pFechaInicial).
					' AND rsv.estado IN (0, 1)'.
					' AND rsv.borrado = 0'.
					' GROUP BY rsv.idtipo, rsv.fechallegada, rsv.fechasalida'.
					' )'.
					' UNION '.
					'(SELECT rsv_mul.idtipo, rsv.fechallegada, rsv.fechasalida'.
					', SUM(rsv_mul.habitaciones) habitaciones'.
					' FROM '.__TABLA_RESERVAS__.' AS rsv'.
					', '.__TABLA_RESERVAS_MULTIPLE__.' AS rsv_mul'.
					', '.__TABLA_HABITACIONES_TIPOS__.' AS hab_tipo'.
					' WHERE rsv_mul.idtipo = hab_tipo.idtipo'.
					' AND rsv.idreserva = rsv_mul.idreserva'.
					' AND hab_tipo.ididioma = '.fLimpiar_sql($pIdIdioma).
					' AND hab_tipo.tipo = '.$pTipo.
					' AND hab_tipo.borrado = 0';
	if ($pConsiderarVisible == '1') {
		$lCadena .= ' AND hab_tipo.visible = 1';
	}
	$lCadena .= ' AND rsv.idestablecimiento = '.fLimpiar_sql($lIDEstablecimiento).
					' AND rsv.idtipo = -1'.
					' AND rsv.fechallegada < '.fLimpiar_sql($pFechaFinal).
					' AND rsv.fechasalida > '.fLimpiar_sql($pFechaInicial).
					' AND rsv.estado IN (0, 1)'.
					' AND rsv.borrado = 0'.
					' GROUP BY rsv_mul.idtipo, rsv.fechallegada, rsv.fechasalida'.
					' )'.
					' ORDER BY idtipo, fechallegada, fechasalida';
	$rsRsv = fQuery($lCadena);
	if ($rsRsv) {
		while ($laRsvAct = mysql_fetch_array($rsRsv)) {
			for ($li=$laRsvAct['fechallegada']; $li<$laRsvAct['fechasalida']; $li=fSumarUnDia($li)) {
				if ($li>=$lFechaDesde && $li<$lFechaHasta) {
					$laReservadosPeriodo[$li][$laRsvAct['idtipo']] += $laRsvAct['habitaciones'];
					$laReservadosPeriodo[$li]['total'] += $laRsvAct['habitaciones'];
				}
			}
		}
	} //fin if ($rsRsv) {...

	return $laReservadosPeriodo;

} // fin fCalcularHabitacionesReservadas($pEstablecimiento, $pFechaInicial, $pFechaFinal)

function fCondicionesOfertaSatisfechas($pEstablecimiento, $pHabitaciones, /*$pTipo*/$pIdOferta, $pPersonas, $pFechaLlegada, $pFechaSalida)
{
	// Guardo la informacion en la tabla de comprobaciones
	// Para tener constancia de cada intento, sea fallido o no
	$lUltimo = fUltimoID(__TABLA_COMPROBACIONES__,"idcomprobacion");
	$lUltimo++;

	$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
	$lIP2 = $_SERVER["REMOTE_ADDR"];
	$lNavegador = $_SERVER["HTTP_USER_AGENT"];

//	$lCadena = "INSERT INTO ".__TABLA_COMPROBACIONES__." VALUES(".
//				$lUltimo.",".
//				time().",".
//				fComillas($lIP1).",".
//				fComillas($lIP2).",".
//				fComillas($lNavegador).",".
//				$pEstablecimiento.",".
//				$pIdOferta .",".
//				$pHabitaciones.",".
//				$pPersonas.",".
//				$pFechaLlegada.",".
//				$pFechaSalida.
//				")";
//	fQuery($lCadena);
	
	
//echo("En fEstablecimientoDisponible. Mensaje1. <br />");
	//Comprobamos que haya disponibilidad en $pEstablecimiento con los parametros indicados. 
	$lIDEstablecimiento = $pEstablecimiento;
	$lIDOferta = $pIdOferta;
	$lHabitaciones = $pHabitaciones;
	$lPersonas = $pPersonas;

	$lFechaDesde = $pFechaLlegada;
	$lFechaHasta = $pFechaSalida;
	$lError = 0;

	if ($lError==0) { 
		//Martin 27/02/08, Seleccionamos las condiciones de la oferta
		$lCadena = "SELECT * FROM ".__TABLA_OFERTAS__.
					" WHERE idoferta = ".$lIDOferta;
		$rsEstancia = fQuery($lCadena);
		$lNoches = round(($lFechaHasta - $lFechaDesde) / 86400, 0);
		//if (mysql_num_rows($rsEstancia)==1) { 
		if (mysql_num_rows($rsEstancia)>0) { 
			$lPrecioNoche = mysql_result($rsEstancia,0,"precio");
			$lEstanciaMax = mysql_result($rsEstancia,0,"estancia_max");
			$lEstanciaMin = mysql_result($rsEstancia,0,"estancia_min");
			$lHabitacionesDisponibles = mysql_result($rsEstancia,0,"habitaciones");
			$lPlazas_min = mysql_result($rsEstancia,0,"plazas_min");
			$lPlazas_max = mysql_result($rsEstancia,0,"plazas_max");
		} 
		else { 
			$lPrecioNoche = "N/D";
			$lEstanciaMax = 60;
			$lEstanciaMin = 1;
		}

		$lNochesAplicableOferta = 0;
		for ($lj=$pFechaLlegada;$lj<$pFechaSalida;$lj=fSumarUnDia($lj))
		{
			$lCadena = "SELECT *".
						" FROM ".__TABLA_OFERTAS_FECHAS__.
						" WHERE idoferta = ".$lIDOferta.
						" AND fecha = ".$lj;
			$rsOfertasFechas = fQuery($lCadena);
			if (mysql_num_rows($rsOfertasFechas) > 0)
			{
				$lNochesAplicableOferta++;
			}
		}

		// Martin 13/03/08, Se aplicara la oferta durante todos los dias que sea posible dentro 
		//del periodo elegido por el cliente. 
		// Se comprobara que se pueda aplicar durante el numero minimo de dias indiacado en la 
		//definicion de la oferta. Si la oferta se puede aplicar durante mas dias del maximo no 
		//se mostrara un error, simplemente a la hora de calcular el importe de la reserva se 
		//aplicara el precio de la oferta durante el numero maximo de dias.

//		if ($lNoches<$lEstanciaMin) { 
//			$lError = 15;
//		} 
//		elseif ($lNoches>$lEstanciaMax) { 
//			$lError = 16;
//		} 
		if ($lNochesAplicableOferta<$lEstanciaMin) { 
			$lError = 20;
		} 
		else {
			$lPersonas = intval($lPersonas);
			$lPlazas_min = intval($lPlazas_min);
			$lPlazas_max = intval($lPlazas_max);
			//Martin 13/04/07. Se tienen en cuenta el nuevo campo plazas_max para calcular cuantas 
			//habitaciones son necesarias para alojar a las personas de la reserva.
			if ($lPlazas_max != 0)
			{
				$lNecesarias = $lPersonas / $lPlazas_max;
			}
			else
			{
				$lNecesarias = 999;
			}
		} // fin if ($lNoches<$lEstanciaMin)...
	}
		// Compruebo si las habitaciones solicitadas son suficientes
		// piden mas habitaciones de las que hay disponibles?
		if($lHabitacionesDisponibles < $lHabitaciones)
		{
			$lError = 6;
		}
		// hay disponibles menos de las necesarias?
		if($lHabitacionesDisponibles < $lNecesarias)
		{
			$lError = 6;
		}
		// son necesarias mas de las que piden?
		if($lNecesarias > $lHabitaciones)
		{
			$lError = 2;
		}
//echo("En fEstablecimientoDisponible. Mensaje4. <br />");
	if ($lError==0) 
	{
		// Martin 13/03/08, Aunque la oferta no se pueda aplicar en todas las fechas del periodo
		//seleccionado por el usuario la podra elegir, los dias que se aplicable se le aplicara el 
		//precio de la oferta y el resto se le aplicara el precio normal (o de una temporada) de la 
		//habitacion.
//		$lOfertaDisponible = true;
//		for ($lj=$pFechaLlegada;$lj<$pFechaSalida;$lj=fSumarUnDia($lj))
//		{
//			$lCadena = "SELECT *".
//						" FROM ".__TABLA_OFERTAS_FECHAS__.
//						" WHERE idoferta = ".$lIDOferta.
//						" AND fecha = ".$lj;
//			$rsOfertasFechas = fQuery($lCadena);
//			if (mysql_num_rows($rsOfertasFechas) == 0)
//			{
//				$lOfertaDisponible = false;
//				break;
//			}
//		}
//		if (!($lOfertaDisponible))
//		{
//			$lError = 18;
//		}
	}
//echo("En fEstablecimientoDisponible. Mensaje5. lError=".$lError.". <br />");
	return $lError;
} //fin function fCondicionesOfertaSatisfechas

function fHabitacionesReservadasOferta($pIDEstablecimiento,$pIDOferta,$pIDTipo,$pFechaEnCurso,$pFechaLlegada,$pFechaSalida)
{
	$lTotalResDiaOferta = 0;
	$lTotalResDiaDesbloqueadasOferta = 0;
	if ($pFechaEnCurso==$pFechaLlegada) 
	{
		$lWhereFechas = " AND (res.fechallegada <= ".$pFechaEnCurso." AND res.fechasalida > ".$pFechaEnCurso.")";
		$lWhereFechas2 = " AND (res.fechallegada <= ".$pFechaEnCurso." AND res.fechasalida > ".$pFechaEnCurso.")".
						" AND ( blo.fecha = ".$pFechaEnCurso."	OR blo.fecha = ".fSumarUnDia($pFechaEnCurso)." )";
	}
	elseif ($pFechaEnCurso==$pFechaSalida) 
	{
		$lWhereFechas = " AND (res.fechallegada < ".$pFechaEnCurso." AND res.fechasalida >= ".$pFechaEnCurso.")";
		$lWhereFechas2 = " AND (res.fechallegada < ".$pFechaEnCurso." AND res.fechasalida >= ".$pFechaEnCurso.")".
						" AND ( blo.fecha = ".$pFechaEnCurso."	OR blo.fecha = ".fRestarUnDia($pFechaEnCurso)." )";
	}
	else 
	{
		//$lWhereFechas = " AND (res.fechallegada <= ".$pFechaEnCurso." AND res.fechasalida >= ".$pFechaEnCurso.")";
		//$lWhereFechas2 = " AND (res.fechallegada <= ".$pFechaEnCurso." AND res.fechasalida >= ".$pFechaEnCurso.")".
		//				" AND blo.fecha = ".$pFechaEnCurso;
		$lWhereFechas = " AND (res.fechallegada <= ".$pFechaEnCurso." AND res.fechasalida > ".$pFechaEnCurso.")";
		$lWhereFechas2 = " AND (res.fechallegada <= ".$pFechaEnCurso." AND res.fechasalida > ".$pFechaEnCurso.")".
						" AND blo.fecha = ".$pFechaEnCurso;
	}
	// Martin 13/11/07, consideramos las reservas en estado 0 (pendiente), pero tambi�n las que est�n en 
	//estado 1 (completada), por si acaso en el administrador se marca como completada una reserva antes de tiempo.
	// Martin 14/03/08, En un dia puede haber habitaciones a las que se ha aplicado un precio de oferta y a otras el 
	//precio normal, pero cuento todas como de oferta. Lo hago porque al hacer una reserva se intenta aplicar la 
	//oferta a todas las habitaciones, si no se ha hecho as� es que ya no quedaban habitaciones de oferta disponibles 
	//en ese momento
	$lCadena = "SELECT SUM(res.habitaciones) hab_total_reservadas_oferta".
		" FROM ".__TABLA_RESERVAS__." AS res".
		", ".__TABLA_OFERTAS_FECHAS__." AS ofe_fec".
		" WHERE res.idestablecimiento = ".$pIDEstablecimiento.
		" AND res.idtipo = ".$pIDTipo.
		" AND res.idoferta IS NOT NULL ".
		" AND res.idoferta != 0 ".
		" AND res.idoferta = ".$pIDOferta.
		" AND res.idoferta = ofe_fec.idoferta".
		" AND ofe_fec.fecha = ".$pFechaEnCurso.
		" AND res.habitaciones_oferta IS NOT NULL".
		$lWhereFechas.
		" AND res.borrado = 0".
		" AND (res.estado = 0 OR res.estado = 1)";
	$rsTotalHabResOferta = fQuery($lCadena);
	if (mysql_num_rows($rsTotalHabResOferta) > 0)
	{
		$lTotalResDiaOferta = mysql_result($rsTotalHabResOferta,0,"hab_total_reservadas_oferta");
		if (is_null($lTotalResDiaOferta))
		{
			$lTotalResDiaOferta = 0;
		}
	}
	// Martin 13/11/07, consideramos las reservas en estado 0 (pendiente), pero tambi�n las que est�n en 
	//estado 1 (completada), por si acaso en el administrador se marca como completada una reserva antes de tiempo.
	$lCadena = "SELECT SUM(res.habitaciones) hab_total_desbloqueadas_oferta".
		" FROM ".__TABLA_RESERVAS__." AS res".
		", ".__TABLA_BLOQUEOS__." AS blo".
		", ".__TABLA_OFERTAS_FECHAS__." AS ofe_fec".
		" WHERE res.idestablecimiento = blo.idestablecimiento".
		" AND res.idtipo = blo.idtipo".
		" AND res.idestablecimiento = ".$pIDEstablecimiento.
		" AND res.idtipo = ".$pIDTipo.
		" AND res.idoferta IS NOT NULL ".
		" AND res.idoferta != 0 ".
		" AND res.idoferta = ".$pIDOferta.
		" AND res.idoferta = ofe_fec.idoferta".
		" AND ofe_fec.fecha = ".$pFechaEnCurso.
		" AND res.habitaciones_oferta IS NOT NULL".
		$lWhereFechas2.
		//" AND blo.fecha = ".$pFechaEnCurso.
		//" AND ( blo.fecha = ".$pFechaEnCurso.
		//"		OR blo.fecha = ".fSumarUnDia($pFechaEnCurso)." )".
		" AND blo.bloqueado = 0".
		" AND res.borrado = 0".
		" AND (res.estado = 0 OR res.estado = 1)";
	$rsTotalHabResDesbloqueadasOferta = fQuery($lCadena);
	if (mysql_num_rows($rsTotalHabResDesbloqueadasOferta) > 0)
	{
		$lTotalResDiaDesbloqueadasOferta = mysql_result($rsTotalHabResDesbloqueadasOferta,0,"hab_total_desbloqueadas_oferta");
		if (is_null($lTotalResDiaDesbloqueadasOferta))
		{
			$lTotalResDiaDesbloqueadasOferta = 0;
		}
	}
	$lTotalResDiaOferta = $lTotalResDiaOferta - $lTotalResDiaDesbloqueadasOferta;
	return ($lTotalResDiaOferta);
} // fin function fHabitacionesReservadasOferta($pIDEstablecimiento,$pIDOferta,$pIDTipo,$pFechaEnCurso,$pFechaLlegada,$pFechaSalida)

function fEstHotelDisponible($pEstablecimiento, $pHabitaciones, $pTipo, $pIdOferta, $pPersonas, $pFechaLlegada, $pFechaSalida)
{
	// Guardo la informacion en la tabla de comprobaciones
	// Para tener constancia de cada intento, sea fallido o no
	$lUltimo = fUltimoID(__TABLA_COMPROBACIONES__,"idcomprobacion");
	$lUltimo++;

	$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
	$lIP2 = $_SERVER["REMOTE_ADDR"];
	$lNavegador = $_SERVER["HTTP_USER_AGENT"];

	$lCadena = "INSERT INTO ".__TABLA_COMPROBACIONES__." VALUES(".
				$lUltimo.",".
				time().",".
				fComillas($lIP1).",".
				fComillas($lIP2).",".
				fComillas($lNavegador).",".
				$pEstablecimiento.",".
				$pTipo.",".
				$pHabitaciones.",".
				$pPersonas.",".
				$pFechaLlegada.",".
				$pFechaSalida.
				")";
	fQuery($lCadena);
	
	
//echo("En fEstablecimientoDisponible. Mensaje1. <br />");
	//Comprobamos que haya disponibilidad en $pEstablecimiento con los parametros indicados. 
	$lIDEstablecimiento = $pEstablecimiento;
	$lIDTipo = $pTipo;
	$lIDOferta = $pIdOferta;
	$lHabitaciones = $pHabitaciones;
	$lPersonas = $pPersonas;

	$lFechaDesde = $pFechaLlegada;
	$lFechaHasta = $pFechaSalida;
	$lError = 0;

	// Busco si la fecha llegada/salida choca con alguna fecha de cierre.
	$lCadena = "SELECT * ".
			" FROM ".__TABLA_ESTABLECIMIENTOS_CIERRE__.
			" WHERE idestablecimiento = ".$lIDEstablecimiento.
			" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")";
	$rsExcepciones = fQuery($lCadena);
	if (mysql_num_rows($rsExcepciones)>0) {
		$lError = 5; // El establecimiento cierra la habitacion en alguno de los dias indicados 
	}

	if ($lError == 0)
	{
		// Martin 02/07/07. Tenemos en cuenta si para el periodo seleccionado hay alguna fecha bloqueada.
		// Martin 06/02/08. Los dias de bloqueo se tratan como si fuesen dias en que todas las habitaciones
		//estan reservadas, asi que si el ultimo dia de la reserva coincide con un dia bloqueado se 
		//considerara que es reservable.
		$lCadena = "SELECT *".
				" FROM ".__TABLA_BLOQUEOS__.
				" WHERE idestablecimiento = ".$lIDEstablecimiento.
				//" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")".
				" AND (fecha >= ".$lFechaDesde." AND fecha < ".$lFechaHasta.")".
				" AND bloqueado = 1";
		$rsBloqueos = fQuery($lCadena);
		if (mysql_num_rows($rsBloqueos) > 0) 
		{
			$lError = 5;
		}
	}

	if ($lError == 0)
	{
		//Martin 03/01/08, si la habitacion tiene marcado el campo visible a 0 la habitacion no est� disponible
		$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_TIPOS__.
					" WHERE idtipo = ".$lIDTipo;
		$rsHabTipos = fQuery($lCadena);
		if (mysql_num_rows($rsHabTipos)>0)
		{
			if (mysql_result($rsHabTipos,0,"visible") == 0)
			{
				$lError = 17;
			}
		}
		else
		{
			$lError = 17;
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje2. <br />");
	if ($lError==0) { 
		//Martin 23/10/07
		$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__.
					" WHERE idtipo = ".$lIDTipo;
		// Compruebo precio por noche, estancia minima y maxima, habitaciones disponibles y plazas
//		$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__." AS hab_est".
//					", ".__TABLA_HABITACIONES_TIPOS__." AS hab_tipo".
//					" WHERE hab_est.idtipo = hab_tipo.idtipo".
//					" AND hab_tipo.idestablecimiento = ".$lIDEstablecimiento.
//					" AND hab_tipo.idtipo = ".$lIDTipo;
		$rsEstancia = fQuery($lCadena);
		$lNoches = round(($lFechaHasta - $lFechaDesde) / 86400, 0);
		//if (mysql_num_rows($rsEstancia)==1) { 
		// Martin 17/05/07 Miramos que se haya devuelto mas de 1 linea, y no solo 1 como hasta ahora, porque al no tener 
		//en cuenta el idioma se pueden estar devolviendo mas de una linea con los mismos datos.
		if (mysql_num_rows($rsEstancia)>0) { 
			$lPrecioNoche = mysql_result($rsEstancia,0,"precio");
			$lEstanciaMax = mysql_result($rsEstancia,0,"estancia_max");
			$lEstanciaMin = mysql_result($rsEstancia,0,"estancia_min");
			$lHabitacionesDisponibles = mysql_result($rsEstancia,0,"habitaciones");
			//$lPlazas = mysql_result($rsEstancia,0,"plazas");
			$lPlazas_min = mysql_result($rsEstancia,0,"plazas_min");
			$lPlazas_max = mysql_result($rsEstancia,0,"plazas_max");
		} 
		else { 
			$lPrecioNoche = "N/D";
			$lEstanciaMax = 60;
			$lEstanciaMin = 1;
		}

		if ($lNoches<$lEstanciaMin) { 
			$lError = 15;
		} 
		elseif ($lNoches>$lEstanciaMax) { 
			$lError = 16;
		} 
		else {
//			$lTotal = round($lPrecioNoche * $lNoches, 2) * $lHabitaciones;
			$lPersonas = intval($lPersonas);
			//$lPlazas = intval($lPlazas);
			$lPlazas_min = intval($lPlazas_min);
			$lPlazas_max = intval($lPlazas_max);
			//Martin 13/04/07. Se tienen en cuenta el nuevo campo plazas_max para calcular cuantas 
			//habitaciones son necesarias para alojar a las personas de la reserva.
			if ($lPlazas_max != 0)
			{
				$lNecesarias = $lPersonas / $lPlazas_max;
			}
			else
			{
				$lNecesarias = 999;
			}
			//if ($lPlazas!=0) {
			//	$lNecesarias = $lPersonas / $lPlazas;
			//}
			//else { 
			//	$lNecesarias = 999;
			//}
		}
	}

	if ($lError==0 && $lIDOferta!="") 
	{
		// Martin 13/03/08, Si se ha elegido una oferta comprobamos si se cumplen sus condiciones.
		//Martin 27/02/08, Seleccionamos las condiciones de la oferta
		$lCadena = "SELECT * FROM ".__TABLA_OFERTAS__.
					" WHERE idoferta = ".$lIDOferta;
		$rsOfertas = fQuery($lCadena);
		$lNoches = round(($lFechaHasta - $lFechaDesde) / 86400, 0);
		//if (mysql_num_rows($rsEstancia)==1) { 
		if (mysql_num_rows($rsEstancia)>0) { 
			$lPrecioNocheOferta = mysql_result($rsOfertas,0,"precio");
			$lEstanciaMaxOferta = mysql_result($rsOfertas,0,"estancia_max");
			$lEstanciaMinOferta = mysql_result($rsOfertas,0,"estancia_min");
			$lHabitacionesOferta = mysql_result($rsOfertas,0,"habitaciones");
			$lPlazas_minOferta = mysql_result($rsOfertas,0,"plazas_min");
			$lPlazas_maxOferta = mysql_result($rsOfertas,0,"plazas_max");
		} 
		else { 
			$lPrecioNocheOferta = "N/D";
			$lEstanciaMaxOferta = 60;
			$lEstanciaMinOferta = 1;
		}

		// Miramos a cuantas noches se le puede aplicar la oferta.
		$lNochesAplicableOferta = 0;
		for ($lj=$pFechaLlegada;$lj<$pFechaSalida;$lj=fSumarUnDia($lj))
		{
			$lCadena = "SELECT *".
						" FROM ".__TABLA_OFERTAS_FECHAS__.
						" WHERE idoferta = ".$lIDOferta.
						" AND fecha = ".$lj;
			$rsOfertasFechas = fQuery($lCadena);
			if (mysql_num_rows($rsOfertasFechas) > 0)
			{
				$lNochesAplicableOferta++;
			}
		}

		// Martin 13/03/08, Se aplicara la oferta durante todos los dias que sea posible dentro 
		//del periodo elegido por el cliente. 
		// Se comprobara que se pueda aplicar durante el numero minimo de dias indiacado en la 
		//definicion de la oferta. Si la oferta se puede aplicar durante mas dias del maximo no 
		//se mostrara un error, simplemente a la hora de calcular el importe de la reserva se 
		//aplicara el precio de la oferta durante el numero maximo de dias.

//		if ($lNoches<$lEstanciaMin) { 
//			$lError = 15;
//		} 
//		elseif ($lNoches>$lEstanciaMax) { 
//			$lError = 16;
//		} 
		if ($lNochesAplicableOferta<$lEstanciaMinOferta) { 
			$lError = 20;
		} 
		else 
		{
			$lPlazas_minOferta = intval($lPlazas_minOferta);
			$lPlazas_maxOferta = intval($lPlazas_maxOferta);
			//Martin 13/04/07. Se tienen en cuenta el nuevo campo plazas_max para calcular cuantas 
			//habitaciones son necesarias para alojar a las personas de la reserva.
			if ($lPlazas_maxOferta != 0)
			{
				$lNecesariasOferta = $lPersonas / $lPlazas_maxOferta;
			}
			else
			{
				$lNecesariasOferta = 999;
			}
		} // fin if ($lNoches<$lEstanciaMin)...
		// Busco si en las fechas indicadas hay disponibilidad
//		if ($lHabitacionesOferta<$lNecesariasOferta) 
//		{ 
//			$lError = 6; // No hay suficientes habitaciones diponibles 
//		}
//		if ($lHabitacionesOferta<$lHabitacionesDisponibles) 
//		{ 
//			$lHabitacionesDisponibles = $lHabitacionesOferta;
//		}
	} // fin if ($lError==0 && $lIDOferta!="")...

//echo("En fEstablecimientoDisponible. Mensaje3. lError=".$lError.". <br />");
	if ($lError==0) {
		// Compruebo restricciones de dias min / max, si hay habitaciones suficientes y si hay un precio especial 
		//para algunos de los dias de la reserva.
		// Martin 21/03/07. 
		$lCadena = "SELECT * ".
				" FROM ".__TABLA_TEMPORADAS_FECHAS__." AS fec, "
				.__TABLA_HABITACIONES_TEMPORADAS__." AS hab".
				" WHERE fec.idestablecimiento = hab.idestablecimiento".
				" AND fec.idtemporada = hab.idtemporada".
				" AND fec.idestablecimiento = ".$lIDEstablecimiento.
				" AND (hab.idtipo = ".$lIDTipo." OR hab.idtipo = -1)".
				" AND (fec.fecha >= ".$lFechaDesde." AND fec.fecha <= ".$lFechaHasta.")".
				" AND hab.borrado = 0". //Martin 23/07/07
				" ORDER BY fec.fecha, hab.idtipo DESC ";
		$rsExcepciones = fQuery($lCadena);
		if(mysql_num_rows($rsExcepciones)>0) { 
			//Las variables siguientes se usan para tener en cuenta o no las lineas de tipo general (-1) 
			//en funcion de si hay una linea de tipo especifico que ya trate el caso.
			$lCompr_est_min_max_general = true;
			$lCompr_habit_general = true;
			$lCompr_precio_general = true;
			for($li=0;$li<mysql_num_rows($rsExcepciones);$li++) { 
				$lIdtipo_actual = mysql_result($rsExcepciones,$li,"idtipo");
				//$lFecha_actual = mysql_result($rsExcepciones,$li,"fecha");
				if ( (mysql_result($rsExcepciones,$li,"estancia_min")!="") &&
					 (mysql_result($rsExcepciones,$li,"estancia_max")!="") &&
					 (mysql_result($rsExcepciones,$li,"fecha")==$lFechaDesde) &&
					 (($lIdtipo_actual>0) || $lCompr_est_min_max_general) ) { 
//echo("En fEstablecimientoDisponible. Mensaje3-1. lIdtipo_actual=".$lIdtipo_actual.". lFechaDesde=".$lFechaDesde." <br />");
					// Compruebo restricciones de dias min / max
					if ($lNoches<mysql_result($rsExcepciones,$li,"estancia_min")) { 
						$lError = 15;
						break;
					}
					if ($lNoches>mysql_result($rsExcepciones,$li,"estancia_max")) { 
						$lError = 16;
						break;
					}
					if ($lIdtipo_actual>0) { 
						$lCompr_est_min_max_general = false;
					} 
					else { 
						$lCompr_est_min_max_general = true;
					}
				} 
				else { 
					$lCompr_est_min_max_general = true;
				}
				if ( mysql_result($rsExcepciones,$li,"habitaciones")!="" &&
					 (($lIdtipo_actual>0) || $lCompr_habit_general) ) { 
					// Busco si en las fechas indicadas hay disponibilidad
					if ( mysql_result($rsExcepciones,$li,"habitaciones")<$lNecesarias) { 
						$lError = 3; // No hay suficientes habitaciones diponibles en uno de los dias
						break;
					}
					if ( mysql_result($rsExcepciones,$li,"habitaciones")<$lHabitacionesDisponibles) { 
						$lHabitacionesDisponibles = mysql_result($rsExcepciones,$li,"habitaciones");
					}
					if ($lIdtipo_actual>0) { 
						$lCompr_habit_general = false;
					} 
					else { 
						$lCompr_habit_general = true;
					}
				} 
				else { 
					$lCompr_habit_general = true;
				}
			}
		}
	}

	if ($lError==0)
	{
		//Martin 14/01/08, Comprobamos si se ha modificado el numero de habitaciones asignadas para alguna fecha
		for ($lj=$pFechaLlegada;$lj<=$pFechaSalida;$lj=fSumarUnDia($lj))
		{
			$lCadena = "SELECT *".
					" FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS_EXCEPCIONES__.
					" WHERE idtipo = ".$lIDTipo.
					" AND fecha = ".$lj;
			$rsHabExcepciones = fQuery($lCadena);
			if (mysql_num_rows($rsHabExcepciones) > 0)
			{
				$lHabDisponiblesExcep = mysql_result($rsHabExcepciones,0,"habitaciones");
				if ($lHabDisponiblesExcep < $lNecesarias)
				{
					$lError = 3; // No hay suficientes habitaciones diponibles en uno de los dias
					break;
				}
				if ($lHabDisponiblesExcep < $lHabitacionesDisponibles)
				{
					$lHabitacionesDisponibles = $lHabDisponiblesExcep;
				}
			}
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje4. <br />");
	if ($lError==0) { 
//		// Busco reservas en esas mismas fechas para ese establecimiento/habitacion
//		$lCadena = "SELECT * FROM ".__TABLA_RESERVAS__.
//			" WHERE estado = 0 AND idestablecimiento = ".$lIDEstablecimiento.
//			" AND idtipo = ".$lIDTipo.
//			" AND (fechallegada < ".$pFechaSalida." AND fechasalida > ".$pFechaLlegada.")".
//			" AND borrado = 0".
//			" AND estado = 0";
//		$rsReservas = fQuery($lCadena);
//		$lReservadas = 0;
//		if(mysql_num_rows($rsReservas)>0)
//		{
//			for($lm=0;$lm<mysql_num_rows($rsReservas);$lm++)
//				$lReservadas+=mysql_result($rsReservas,$lm,"habitaciones");
//			$lHabitacionesDisponibles-=$lReservadas;
//		}

		//Martin 02/07/07. Se mira en las reservas solapadas (teniendo en cuenta las reservas desbloqueadas) 
		//para ver si hay disponibilidad.
		$lMinHabDisponibles = $lHabitacionesDisponibles;
//		if ($lIDOferta != "")
//		{
//			//Martin 13/03/08, $lMinHabDisponiblesOferta se usa mas adelante para comprobar si descontando las 
//			//reservas que se hayan hecho con la oferta que dan habitaciones suficientes para aplicar la oferta.
//			$lMinHabDisponiblesOferta = $lHabitacionesDisponibles;
//			$lHabitacionesDisponiblesOferta = $lHabitacionesDisponibles;
//		}
		for ($lj=$pFechaLlegada;$lj<=$pFechaSalida;$lj=fSumarUnDia($lj))
		{
			$lTotalResDia = 0;
			$lTotalResDiaDesbloqueadas = 0;
			if ($lj==$pFechaLlegada) 
			{
				$lWhereFechas = " AND (fechallegada <= ".$lj." AND fechasalida > ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada <= ".$lj." AND res.fechasalida > ".$lj.")".
								" AND ( blo.fecha = ".$lj."	OR blo.fecha = ".fSumarUnDia($lj)." )";
			}
			elseif ($lj==$pFechaSalida) 
			{
				$lWhereFechas = " AND (fechallegada < ".$lj." AND fechasalida >= ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada < ".$lj." AND res.fechasalida >= ".$lj.")".
								" AND ( blo.fecha = ".$lj."	OR blo.fecha = ".fRestarUnDia($lj)." )";
			}
			else 
			{
				$lWhereFechas = " AND (fechallegada <= ".$lj." AND fechasalida >= ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada <= ".$lj." AND res.fechasalida >= ".$lj.")".
								" AND blo.fecha = ".$lj;
			}
			// Martin 13/11/07, consideramos las reservas en estado 0 (pendiente), pero tambi�n las que est�n en 
			//estado 1 (completada), por si acaso en el administrador se marca como completada una reserva antes de tiempo.
			$lCadena = "SELECT SUM(habitaciones) hab_total_reservadas".
				" FROM ".__TABLA_RESERVAS__.
				" WHERE idestablecimiento = ".$lIDEstablecimiento.
				" AND idtipo = ".$lIDTipo.
				$lWhereFechas.
				" AND borrado = 0".
				" AND (estado = 0 OR estado = 1)";
			$rsTotalHabRes = fQuery($lCadena);
			if (mysql_num_rows($rsTotalHabRes) > 0)
			{
				$lTotalResDia = mysql_result($rsTotalHabRes,0,"hab_total_reservadas");
				if (is_null($lTotalResDia))
				{
					$lTotalResDia = 0;
				}
			}
			// Martin 13/11/07, consideramos las reservas en estado 0 (pendiente), pero tambi�n las que est�n en 
			//estado 1 (completada), por si acaso en el administrador se marca como completada una reserva antes de tiempo.
			$lCadena = "SELECT SUM(res.habitaciones) hab_total_desbloqueadas".
				" FROM ".__TABLA_RESERVAS__." AS res".
				", ".__TABLA_BLOQUEOS__." AS blo".
				" WHERE res.idestablecimiento = blo.idestablecimiento".
				" AND res.idtipo = blo.idtipo".
				" AND res.idestablecimiento = ".$lIDEstablecimiento.
				" AND res.idtipo = ".$lIDTipo.
				$lWhereFechas2.
				//" AND blo.fecha = ".$lj.
				//" AND ( blo.fecha = ".$lj.
				//"		OR blo.fecha = ".fSumarUnDia($lj)." )".
				" AND blo.bloqueado = 0".
				" AND res.borrado = 0".
				" AND (res.estado = 0 OR res.estado = 1)";
			$rsTotalHabResDesbloqueadas = fQuery($lCadena);
			if (mysql_num_rows($rsTotalHabResDesbloqueadas) > 0)
			{
				$lTotalResDiaDesbloqueadas = mysql_result($rsTotalHabResDesbloqueadas,0,"hab_total_desbloqueadas");
				if (is_null($lTotalResDiaDesbloqueadas))
				{
					$lTotalResDiaDesbloqueadas = 0;
				}
			}
			$lTotalResDia = $lTotalResDia - $lTotalResDiaDesbloqueadas;
			if ( ($lHabitacionesDisponibles - $lTotalResDia) < $lMinHabDisponibles)
			{
				$lMinHabDisponibles = $lHabitacionesDisponibles - $lTotalResDia;
			}
		} // fin for ($lj=$pFechaLlegada;$lj<=$pFechaSalida;$lj=fSumarUnDia($lj))...
		$lHabitacionesDisponibles = $lMinHabDisponibles;

		// Compruebo si las habitaciones solicitadas son suficientes
		// piden mas habitaciones de las que hay disponibles?
		if($lHabitacionesDisponibles < $lHabitaciones)
		{	$lError = 6;}
		// hay disponibles menos de las necesarias?
		if($lHabitacionesDisponibles < $lNecesarias)
		{	$lError = 6; }
		// son necesarias mas de las que piden?
		if($lNecesarias > $lHabitaciones)
		{	$lError = 2; }
	}

	if ($lError == 0  && $lIDOferta!="")
	{
		// Martin 13/03/08, Si se ha elegido una oferta comprobamos si quedan suficientes habitaciones 
		//de oferta en los dias en que sea aplicable la oferta
		//Martin 02/07/07. Se mira en las reservas solapadas (teniendo en cuenta las reservas desbloqueadas) 
		//para ver si hay disponibilidad.
		$lMinHabDisponiblesOferta = $lHabitacionesOferta;
		$lHabitacionesDisponiblesOferta = $lMinHabDisponiblesOferta;
		for ($lj=$pFechaLlegada;$lj<=$pFechaSalida;$lj=fSumarUnDia($lj))
		{
			$lTotalResDiaOferta = fHabitacionesReservadasOferta($lIDEstablecimiento,$lIDOferta,$lIDTipo,$lj,$pFechaLlegada,$pFechaSalida);
			if ( ($lHabitacionesDisponiblesOferta - $lTotalResDia) < $lMinHabDisponiblesOferta)
			{
				$lMinHabDisponiblesOferta = $lHabitacionesDisponiblesOferta - $lTotalResDiaOferta;
			}
		} // fin for ($lj=$pFechaLlegada;$lj<=$pFechaSalida;$lj=fSumarUnDia($lj))...
		$lHabitacionesDisponiblesOferta = $lMinHabDisponiblesOferta;

		// Compruebo si las habitaciones solicitadas son suficientes
		// piden mas habitaciones de las que hay disponibles?
		//if($lHabitacionesDisponiblesOferta < $lHabitaciones)
		//{	$lError = 6; }
		// hay disponibles menos de las necesarias?
		//if($lHabitacionesDisponiblesOferta < $lNecesariasOferta)
		//{	$lError = 6; }
		// son necesarias mas de las que piden?
		//if($lNecesariasOferta > $lHabitaciones)
		//{	$lError = 2; }
		if ($lHabitacionesDisponiblesOferta <= 0)
		{
			// Martin 14/03/08, Solo cuando las habitaciones disponibles para la oferta sean 0 (o menor que 0) 
			//se considerara que la oferta no esta disponible. Si no quedan habitaciones en oferta suficientes 
			//para cubrir la reserva se aplicara la oferta a todas las habitaciones que se pueda y al resto el 
			//precio normal o el de la temporada correspondiente.
			$lError = 6; 
		}
	} // fin if ($lError == 0  && $lIDOferta!="")...
/*
		// Busco si la fecha llegada/salida choca con alguna fecha de vacaciones.
		$lCadena = "SELECT * FROM ".__TABLA_PYD_VACACIONES__." WHERE idestablecimiento = ".$lIDEstablecimiento.
			" AND fechainicio = ".$lFechaDesde;
		$rsVacaciones = fQuery($lCadena);

		if(mysql_num_rows($rsVacaciones)>0)
			$lError = 5; // El establecimiento cierra la habitacion en alguno de los dias indicados
*/
//echo("En fEstablecimientoDisponible. Mensaje5. lError=".$lError.". <br />");
	return $lError;
} //fin function fEstHotelDisponible

function fEstApartDisponible($pEstablecimiento, $pHabitaciones, $pTipo, $pPersonas, $pFechaLlegada, $pFechaSalida)
{
	// Guardo la informacion en la tabla de comprobaciones
	// Para tener constancia de cada intento, sea fallido o no
	$lUltimo = fUltimoID(__TABLA_COMPROBACIONES__,"idcomprobacion");
	$lUltimo++;

	$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
	$lIP2 = $_SERVER["REMOTE_ADDR"];
	$lNavegador = $_SERVER["HTTP_USER_AGENT"];

	$lCadena = "INSERT INTO ".__TABLA_COMPROBACIONES__." VALUES(".
				$lUltimo.",".
				time().",".
				fComillas($lIP1).",".
				fComillas($lIP2).",".
				fComillas($lNavegador).",".
				$pEstablecimiento.",".
				$pTipo.",".
				$pHabitaciones.",".
				$pPersonas.",".
				$pFechaLlegada.",".
				$pFechaSalida.
				")";
	fQuery($lCadena);
	
	
//echo("En fEstablecimientoDisponible. Mensaje1. <br />");
	//Comprobamos que haya disponibilidad en $pEstablecimiento con los parametros indicados. 
	$lIDEstablecimiento = $pEstablecimiento;
	$lIDTipo = $pTipo;
	$lHabitaciones = $pHabitaciones;
	$lPersonas = $pPersonas;

	$lFechaDesde = $pFechaLlegada;
	$lFechaHasta = $pFechaSalida;
	$lError = 0;

	// Busco si la fecha llegada/salida choca con alguna fecha de cierre.
	$lCadena = "SELECT * ".
			" FROM ".__TABLA_HABITACIONES_CIERRE__.
			" WHERE (idtipo = ".$lIDTipo." OR idtipo = -1)".
			" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")";
	$rsExcepciones = fQuery($lCadena);
	if (mysql_num_rows($rsExcepciones)>0) {
		$lError = 5; // El establecimiento cierra la habitacion en alguno de los dias indicados 
	}

	if ($lError == 0)
	{
		// Martin 02/07/07. Tenemos en cuenta si para el periodo seleccionado hay alguna fecha bloqueada.
		// Martin 06/02/08. Los dias de bloqueo se tratan como si fuesen dias en que el apartamento
		//estan reservadas, asi que si el ultimo dia de la reserva coincide con un dia bloqueado se 
		//considerara que es reservable.
		$lCadena = "SELECT *".
				" FROM ".__TABLA_BLOQUEOS__.
				" WHERE idtipo = ".$lIDTipo.
				//" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")".
				" AND (fecha >= ".$lFechaDesde." AND fecha < ".$lFechaHasta.")".
				" AND bloqueado = 1";
		$rsBloqueos = fQuery($lCadena);
		if (mysql_num_rows($rsBloqueos) > 0) 
		{
			$lError = 5;
		}
	}

	if ($lError == 0)
	{
		//Martin 03/01/08, si la habitacion tiene marcado el campo visible a 0 la habitacion no est� disponible
		$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_TIPOS__.
					" WHERE idtipo = ".$lIDTipo;
		$rsHabTipos = fQuery($lCadena);
		if (mysql_num_rows($rsHabTipos)>0)
		{
			if (mysql_result($rsHabTipos,0,"visible") == 0)
			{
				$lError = 17;
			}
		}
		else
		{
			$lError = 17;
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje2. <br />");
	if ($lError==0) { 
		// Compruebo precio por noche, estancia minima y maxima, habitaciones disponibles y plazas
		//Martin 23/10/07
		$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__.
					" WHERE idtipo = ".$lIDTipo;
//		$lCadena = "SELECT * FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__." AS hab_est".
//					", ".__TABLA_HABITACIONES_TIPOS__." AS hab_tipo".
//					" WHERE hab_est.idtipo = hab_tipo.idtipo".
//					" AND hab_tipo.idestablecimiento = ".$lIDEstablecimiento.
//					" AND hab_tipo.idtipo = ".$lIDTipo;
		$rsEstancia = fQuery($lCadena);
		$lNoches = round(($lFechaHasta - $lFechaDesde) / 86400, 0);
		//if (mysql_num_rows($rsEstancia)==1) { 
		// Martin 17/05/07 Miramos que se haya devuelto mas de 1 linea, y no solo 1 como hasta ahora, porque al no tener 
		//en cuenta el idioma se pueden estar devolviendo mas de una linea con los mismos datos.
		if (mysql_num_rows($rsEstancia)>0) { 
			$lPrecioNoche = mysql_result($rsEstancia,0,"precio");
			$lEstanciaMax = mysql_result($rsEstancia,0,"estancia_max");
			$lEstanciaMin = mysql_result($rsEstancia,0,"estancia_min");
			$lHabitacionesDisponibles = mysql_result($rsEstancia,0,"habitaciones");
			//$lPlazas = mysql_result($rsEstancia,0,"plazas");
			$lPlazas_min = mysql_result($rsEstancia,0,"plazas_min");
			$lPlazas_max = mysql_result($rsEstancia,0,"plazas_max");
		} 
		else { 
			$lPrecioNoche = "N/D";
			$lEstanciaMax = 60;
			$lEstanciaMin = 1;
		}

		if ($lNoches<$lEstanciaMin) { 
			$lError = 15;
		} 
		elseif ($lNoches>$lEstanciaMax) { 
			$lError = 16;
		} 
		else {
//			$lTotal = round($lPrecioNoche * $lNoches, 2) * $lHabitaciones;
			$lPersonas = intval($lPersonas);
			//$lPlazas = intval($lPlazas);
			$lPlazas_min = intval($lPlazas_min);
			$lPlazas_max = intval($lPlazas_max);
			//Martn 13/04/07. Se tienen en cuenta el nuevo campo plazas_max para calcular cuantas 
			//habitaciones son necesarias para alojar a las personas de la reserva.
			if ($lPlazas_max != 0)
			{
				$lNecesarias = $lPersonas / $lPlazas_max;
			}
			else
			{
				$lNecesarias = 999;
			}
			//if ($lPlazas!=0) {
			//	$lNecesarias = $lPersonas / $lPlazas;
			//}
			//else { 
			//	$lNecesarias = 999;
			//}
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje3. lError=".$lError.". <br />");
	if ($lError==0) {
		// Compruebo restricciones de d as min / max, si hay habitaciones suficientes y si hay un precio especial 
		//para algunos de los das de la reserva.
		// Mart n 21/03/07. 
		$lCadena = "SELECT * ".
				" FROM ".__TABLA_TEMPORADAS_FECHAS__." AS fec, "
				.__TABLA_HABITACIONES_TEMPORADAS__." AS hab".
				" WHERE fec.idestablecimiento = hab.idestablecimiento".
				" AND fec.idtemporada = hab.idtemporada".
				" AND fec.idestablecimiento = ".$lIDEstablecimiento.
				" AND (hab.idtipo = ".$lIDTipo." OR hab.idtipo = -1)".
				" AND (fec.fecha >= ".$lFechaDesde." AND fec.fecha <= ".$lFechaHasta.")".
				" AND hab.borrado = 0". //Martin 23/07/07
				" ORDER BY fec.fecha, hab.idtipo DESC ";
//		$lCadena = "SELECT * ".
//				" FROM ".__TABLA_PYD_EXCEPCIONES__.
//				" WHERE idestablecimiento = ".$lIDEstablecimiento.
//				" AND (idtipo = ".$lIDTipo." OR idtipo = -1)".
//				" AND (fecha >= ".$lFechaDesde." AND fecha <= ".$lFechaHasta.")".
//				" AND estado = 0".
//				" ORDER BY fecha, idtipo DESC ";
		$rsExcepciones = fQuery($lCadena);
		if(mysql_num_rows($rsExcepciones)>0) { 
			//Las variables siguientes se usan para tener en cuenta o no las lineas de tipo general (-1) 
			//en funcion de si hay una linea de tipo especifico que ya trate el caso.
			$lCompr_est_min_max_general = true;
			$lCompr_habit_general = true;
			$lCompr_precio_general = true;
			for($li=0;$li<mysql_num_rows($rsExcepciones);$li++) { 
				$lIdtipo_actual = mysql_result($rsExcepciones,$li,"idtipo");
				//$lFecha_actual = mysql_result($rsExcepciones,$li,"fecha");
				if ( (mysql_result($rsExcepciones,$li,"estancia_min")!="") &&
					 (mysql_result($rsExcepciones,$li,"estancia_max")!="") &&
					 (mysql_result($rsExcepciones,$li,"fecha")==$lFechaDesde) &&
					 (($lIdtipo_actual>0) || $lCompr_est_min_max_general) ) { 
//echo("En fEstablecimientoDisponible. Mensaje3-1. lIdtipo_actual=".$lIdtipo_actual.". lFechaDesde=".$lFechaDesde." <br />");
					// Compruebo restricciones de dias min / max
					if ($lNoches<mysql_result($rsExcepciones,$li,"estancia_min")) { 
						$lError = 15;
						break;
					}
					if ($lNoches>mysql_result($rsExcepciones,$li,"estancia_max")) { 
						$lError = 16;
						break;
					}
					if ($lIdtipo_actual>0) { 
						$lCompr_est_min_max_general = false;
					} 
					else { 
						$lCompr_est_min_max_general = true;
					}
				} 
				else { 
					$lCompr_est_min_max_general = true;
				}
				if ( mysql_result($rsExcepciones,$li,"habitaciones")!="" &&
					 (($lIdtipo_actual>0) || $lCompr_habit_general) ) { 
					// Busco si en las fechas indicadas hay disponibilidad
					if ( mysql_result($rsExcepciones,$li,"habitaciones")<$lNecesarias) { 
						$lError = 3; // No hay suficientes habitaciones diponibles en uno de los dias
						break;
					}
					if ( mysql_result($rsExcepciones,$li,"habitaciones")<$lHabitacionesDisponibles) { 
						$lHabitacionesDisponibles = mysql_result($rsExcepciones,$li,"habitaciones");
					}
					if ($lIdtipo_actual>0) { 
						$lCompr_habit_general = false;
					} 
					else { 
						$lCompr_habit_general = true;
					}
				} 
				else { 
					$lCompr_habit_general = true;
				}
			}
		}
	}
//echo("En fEstablecimientoDisponible. Mensaje4. <br />");
	if ($lError==0) { 
//		// Busco reservas en esas mismas fechas para ese establecimiento/habitacion
//		$lCadena = "SELECT * FROM ".__TABLA_RESERVAS__.
//			" WHERE estado = 0 AND idestablecimiento = ".$lIDEstablecimiento.
//			" AND idtipo = ".$lIDTipo.
//			" AND (fechallegada < ".$pFechaSalida." AND fechasalida > ".$pFechaLlegada.")".
//			" AND borrado = 0".
//			" AND estado = 0";
//		$rsReservas = fQuery($lCadena);
//		$lReservadas = 0;
//		if(mysql_num_rows($rsReservas)>0)
//		{
//			for($lm=0;$lm<mysql_num_rows($rsReservas);$lm++)
//				$lReservadas+=mysql_result($rsReservas,$lm,"habitaciones");
//			$lHabitacionesDisponibles-=$lReservadas;
//		}

		//Martin 02/07/07. Se mira en las reservas solapadas (teniendo en cuenta las reservas desbloqueadas) 
		//para ver si hay disponibilidad.
		$lMinHabDisponibles = $lHabitacionesDisponibles;
		for ($lj=$pFechaLlegada;$lj<=$pFechaSalida;$lj=fSumarUnDia($lj))
		{
			$lTotalResDia = 0;
			$lTotalResDiaDesbloqueadas = 0;
			if ($lj==$pFechaLlegada) 
			{
				$lWhereFechas = " AND (fechallegada <= ".$lj." AND fechasalida > ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada <= ".$lj." AND res.fechasalida > ".$lj.")".
								" AND ( blo.fecha = ".$lj."	OR blo.fecha = ".fSumarUnDia($lj)." )";
			}
			elseif ($lj==$pFechaSalida) 
			{
				$lWhereFechas = " AND (fechallegada < ".$lj." AND fechasalida >= ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada < ".$lj." AND res.fechasalida >= ".$lj.")".
								" AND ( blo.fecha = ".$lj."	OR blo.fecha = ".fRestarUnDia($lj)." )";
			}
			else 
			{
				$lWhereFechas = " AND (fechallegada <= ".$lj." AND fechasalida >= ".$lj.")";
				$lWhereFechas2 = " AND (res.fechallegada <= ".$lj." AND res.fechasalida >= ".$lj.")".
								" AND blo.fecha = ".$lj;
			}
			// Martin 13/11/07, consideramos las reservas en estado 0 (pendiente), pero tambi�n las que est�n en 
			//estado 1 (completada), por si acaso en el administrador se marca como completada una reserva antes de tiempo.
			$lCadena = "SELECT SUM(habitaciones) hab_total_reservadas".
				" FROM ".__TABLA_RESERVAS__.
				" WHERE idestablecimiento = ".$lIDEstablecimiento.
				" AND idtipo = ".$lIDTipo.
				$lWhereFechas.
				" AND borrado = 0".
				" AND (estado = 0 OR estado = 1)";
			$rsTotalHabRes = fQuery($lCadena);
			if (mysql_num_rows($rsTotalHabRes) > 0)
			{
				$lTotalResDia = mysql_result($rsTotalHabRes,0,"hab_total_reservadas");
				if (is_null($lTotalResDia))
				{
					$lTotalResDia = 0;
				}
			}
			// Martin 13/11/07, consideramos las reservas en estado 0 (pendiente), pero tambi�n las que est�n en 
			//estado 1 (completada), por si acaso en el administrador se marca como completada una reserva antes de tiempo.
			$lCadena = "SELECT SUM(res.habitaciones) hab_total_desbloqueadas".
				" FROM ".__TABLA_RESERVAS__." AS res".
				", ".__TABLA_BLOQUEOS__." AS blo".
				" WHERE res.idestablecimiento = blo.idestablecimiento".
				" AND res.idtipo = blo.idtipo".
				" AND res.idestablecimiento = ".$lIDEstablecimiento.
				" AND res.idtipo = ".$lIDTipo.
				$lWhereFechas2.
				//" AND blo.fecha = ".$lj.
				//" AND ( blo.fecha = ".$lj.
				//"		OR blo.fecha = ".fSumarUnDia($lj)." )".
				" AND blo.bloqueado = 0".
				" AND res.borrado = 0".
				" AND (res.estado = 0 OR res.estado = 1)";
			$rsTotalHabResDesbloqueadas = fQuery($lCadena);
			if (mysql_num_rows($rsTotalHabResDesbloqueadas) > 0)
			{
				$lTotalResDiaDesbloqueadas = mysql_result($rsTotalHabResDesbloqueadas,0,"hab_total_desbloqueadas");
				if (is_null($lTotalResDiaDesbloqueadas))
				{
					$lTotalResDiaDesbloqueadas = 0;
				}
			}
			$lTotalResDia = $lTotalResDia - $lTotalResDiaDesbloqueadas;
			if ( ($lHabitacionesDisponibles - $lTotalResDia) < $lMinHabDisponibles)
			{
				$lMinHabDisponibles = $lHabitacionesDisponibles - $lTotalResDia;
			}
		}
		$lHabitacionesDisponibles = $lMinHabDisponibles;

		// Compruebo si las habitaciones solicitadas son suficientes
		// piden mas habitaciones de las que hay disponibles?
		if($lHabitacionesDisponibles < $lHabitaciones)
			$lError = 6;
		// hay disponibles menos de las necesarias?
		if($lHabitacionesDisponibles < $lNecesarias)
			$lError = 6;
		// son necesarias mas de las que piden?
		if($lNecesarias > $lHabitaciones)
			$lError = 2;
	}
/*
		// Busco si la fecha llegada/salida choca con alguna fecha de vacaciones.
		$lCadena = "SELECT * FROM ".__TABLA_PYD_VACACIONES__." WHERE idestablecimiento = ".$lIDEstablecimiento.
			" AND fechainicio = ".$lFechaDesde;
		$rsVacaciones = fQuery($lCadena);

		if(mysql_num_rows($rsVacaciones)>0)
			$lError = 5; // El establecimiento cierra la habitacion en alguno de los dias indicados
*/
//echo("En fEstablecimientoDisponible. Mensaje5. lError=".$lError.". <br />");
	return $lError;
} //fin function fEstApartDisponible

function fFormatearTexto($pTexto)
{
	$lTexto = str_replace("\n","<br />",$pTexto);
	$lTexto = str_replace("[b]","<span style=\"font-weight: bold;\">",$lTexto);
	$lTexto = str_replace("[/b]","</span>",$lTexto);
	$lTexto = str_replace("[i]","<span style=\"font-style: italic;\">",$lTexto);
	$lTexto = str_replace("[/i]","</span>",$lTexto);
	$lTexto = str_replace("[u]","<span style=\"text-decoration: underline;\">",$lTexto);
	$lTexto = str_replace("[/u]","</span>",$lTexto);
	
	return $lTexto;
}
function fFormatearTexto_correo_html($pTexto)
{
	$lTexto = str_replace("\n","<br>\n",$pTexto);
	$lTexto = str_replace("[b]","<span style=\"font-weight: bold;\">",$lTexto);
	$lTexto = str_replace("[/b]","</span>",$lTexto);
	$lTexto = str_replace("[i]","<span style=\"font-style: italic;\">",$lTexto);
	$lTexto = str_replace("[/i]","</span>",$lTexto);
	$lTexto = str_replace("[u]","<span style=\"text-decoration: underline;\">",$lTexto);
	$lTexto = str_replace("[/u]","</span>",$lTexto);
	
	return $lTexto;
}
function fFormatearTexto_plano($pTexto)
{
	//$lTexto = str_replace("\n","<br />",$pTexto);
	$lTexto = str_replace("[b]","",$pTexto);
	$lTexto = str_replace("[/b]","",$lTexto);
	$lTexto = str_replace("[i]","",$lTexto);
	$lTexto = str_replace("[/i]","",$lTexto);
	$lTexto = str_replace("[u]","",$lTexto);
	$lTexto = str_replace("[/u]","",$lTexto);
	// Martin 25/03/08, reemplazamos los simbolos HTML para las vocales acentuadas, el simbolo del euro y 
	// la etiqueta del salto de linea.
	$lTexto = str_replace("&aacute;","�",$lTexto);
	$lTexto = str_replace("&eacute;","�",$lTexto);
	$lTexto = str_replace("&iacute;","�",$lTexto);
	$lTexto = str_replace("&oacute;","�",$lTexto);
	$lTexto = str_replace("&uacute;","�",$lTexto);
	$lTexto = str_replace("&euro;","�",$lTexto);
	$lTexto = str_replace("<br />","\n",$pTexto);
	$lTexto = str_replace("<br/>","\n",$pTexto);
	$lTexto = str_replace("<br>","\n",$pTexto);
	
	return $lTexto;
}

function fObtenerCampo($pTabla,$pCampo,$pID,$pValor)
{
	if($pTabla!=""&&$pCampo!=""&&pID!=""&&$pValor!="")
	{
		$lCadena = "SELECT ".$pCampo." FROM ".$pTabla." WHERE ".$pID." = ".$pValor;
		$rsCampo = fQuery($lCadena);
		if(mysql_num_rows($rsCampo)>0)
			$lCampo = mysql_result($rsCampo,0,$pCampo);
		else
			$lCampo = "N/D";
	}
	else
		$lCampo = "N/D";

	return $lCampo;
}
function fObtenerCampoTexto($pTabla,$pCampo,$pID,$pValor)
{
	if($pTabla!=""&&$pCampo!=""&&pID!=""&&$pValor!="")
	{
		$lCadena = "SELECT ".$pCampo." FROM ".$pTabla." WHERE ".$pID." LIKE ".fComillas($pValor);
		$rsCampo = fQuery($lCadena);
		if(mysql_num_rows($rsCampo)>0)
			$lCampo = mysql_result($rsCampo,0,$pCampo);
		else
			$lCampo = "N/D";
	}
	else
		$lCampo = "N/D";

	return $lCampo;
}
function fObtenerCampoDoble($pTabla,$pCampo,$pID1, $pID2,$pValor1,$pValor2)
{
	if($pTabla!=""&&$pCampo!=""&&pID1!=""&&$pID2!=""&&$pValor1!=""&&$pValor2!="")
	{
		$lCadena = "SELECT ".$pCampo." FROM ".$pTabla." WHERE ".$pID1." = ".$pValor1." AND ".$pID2." = ".$pValor2;
		$rsCampo = fQuery($lCadena);
		if(mysql_num_rows($rsCampo)>0)
			$lCampo = mysql_result($rsCampo,0,$pCampo);
		else
			$lCampo = "N/D";
	}
	else
		$lCampo = "N/D";

	return $lCampo;
}

function fGenerarRegistro($pUsuario, $pAccion, $pTabla, $pID)
{
	$lUltimo = fUltimoID(__TABLA_REGISTRO__,"idregistro");
	$lUltimo++;

	$lIP1 = $_SERVER["HTTP_CLIENT_IP"];
	$lIP2 = $_SERVER["REMOTE_ADDR"];
	$lNavegador = $_SERVER["HTTP_USER_AGENT"];
	$lFecha = time();

	$lCadena = "INSERT INTO ".__TABLA_REGISTRO__." VALUES(".
			$lUltimo.",".
			$lFecha.",".
			fComillas($lIP1).",".
			fComillas($lIP2).",".
			fComillas($lNavegador).",".
			$pUsuario.",".
			fComillas($pAccion).",".
			fComillas($pTabla).",".
			$pID.
			")";
	fQuery($lCadena);
}

function fIdiomasEstablecimiento($pEstablecimiento)
{
	$lCadena = "SELECT * FROM ".__TABLA_IDIOMAS_ESTABLECIMIENTOS__." WHERE idestablecimiento = ".$pEstablecimiento;
	$rsIdiomas = fQuery($lCadena);

	$lIdiomas = mysql_num_rows($rsIdiomas);

	return $lIdiomas;
}

function fGetDescriptivo($pCampo,$pEstablecimiento,$pIdioma)
{
	$lCadena = "SELECT ".$pCampo." FROM ".__TABLA_DESCRIPCIONES__." WHERE idestablecimiento = ".$pEstablecimiento." AND ididioma = ".$pIdioma;
	$rsTexto = fQuery($lCadena);
	
	if(mysql_num_rows($rsTexto)>0)
		$lTexto = mysql_result($rsTexto,0,$pCampo);
	else
		$lTexto = "";
	
	return $lTexto;
}

function fGetDescrTipoHabitacion ($pCampo,$pIdTipo,$pIdidioma)
//Martin 12/03/07. Esta funcin accede a la tabla de Tipos de Habitaci n para devolver el campo que se le pase 
// en el idioma indicado.
{
	$lCadena = "SELECT ".$pCampo.
				" FROM ".__TABLA_HABITACIONES_TIPOS__.
				" WHERE idtipo = ".$pIdTipo.
				" AND ididioma = ".$pIdidioma;
	$rsTexto = fQuery($lCadena);
	
	if(mysql_num_rows($rsTexto)>0)
		$lTexto = mysql_result($rsTexto,0,$pCampo);
	else
		$lTexto = "";
	
	return $lTexto;
}

function fEsNumeroCorrecto($pNumero)
{
	// Martin 12/12/07. En esta funcion comprobamos que la cadena que se pase en pNumero sea un n�mero.
	//Se considerar� que la cadena es un n�mero si tiene un caracter punto(.) como mucho y el resto de 
	//los caracteres son cifras.
	$lLongitudCadena = strlen($pNumero);
	$lSeparadorDecimal = ".";
	$lNumSeparadorDecimal = 0;
	$lNumValido = true;
	for ($li=0;$li<$lLongitudCadena;$li++)
	{
		$lCaracterComp = substr($pNumero,$li,1);
		if ($lCaracterComp == $lSeparadorDecimal)
		{
			$lNumSeparadorDecimal++;
		}
		elseif ( (ord($lCaracterComp) < 48) || (ord($lCaracterComp) > 57) )
		{
			$lNumValido = false;
			break;
		}
	}
	if ($lNumSeparadorDecimal > 1)
	{
		$lNumValido = false;
	}
	return($lNumValido);
}

function fLimpiar_sql($pValor) {
	// Martin 24/05/10. Funcion para mejorar la seguridad de las consultas SQL. Lo que hace es escapar caracteres 
	//especiales y evitar que se interpreten comillas simples en los valores que se pasen a las queries.
	if (get_magic_quotes_gpc()) {
		$pValor = stripslashes($pValor);
	}
	$pValor = mysql_real_escape_string($pValor);
	return ($pValor);
}

function fReemplazarCaracteresXML($pTexto) {
	// Llamamos a esta funcion desde los scripts del directorio /scriptsxml para sustituir en los campos de descripcion, 
	//nombres, etc. caracteres reservados de XML por su representacion de entidades de caracter de HTML.
	$lTexto = $pTexto;
	$lTexto = str_replace('&','&amp;',$lTexto);
	$lTexto = str_replace('<','&lt;',$lTexto);
	$lTexto = str_replace('>','&gt;',$lTexto);
	$lTexto = str_replace('\'','&apos;',$lTexto);
	$lTexto = str_replace('"','&quot;',$lTexto);
	return $lTexto;
}

?>