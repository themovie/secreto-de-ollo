<?php
// Aplicacin
	define("__TITULO_WEB__","Reserva de Alojamientos");
	define("__EMPRESA__","Reserva de Alojamientos");
	define("__URL__","http://www.reservadealojamientos.com/");
	define("__LINK_PAGINA__","");
	define("__LINK_EXTRANET__","");
	define("__MAIL_FORMULARIOS__","irodriguez@themovie.org");
	define("__MAIL_RESERVAS__","irodriguez@themovie.org");
	define("__TELEFONO__","");
	define("__RAIZ__",$_SERVER["DOCUMENT_ROOT"]."/");
	define("__DIR_APP__","");
	define("__ROOT__","http://".$_SERVER["HTTP_HOST"]."/");
	define("__INC_HTML__","html/");
	define("__INC_HOME__","include/");
	define("__INC_APP__","app/");
	define("__INC_MODULOS__","modulos/");
	//define("__DIR_ADM__","adm/");
	define("__DIR_IMAGES__","images/");
	define("__DIR_UPLOAD__","uploads/");

if ($eIdioma == "es")
{
	define("__KEYS_EXTRA__"," ");
}
elseif ($eIdioma == "en")
{
	define("__KEYS_EXTRA__"," ");
}
elseif ($eIdioma == "de")
{
	define("__KEYS_EXTRA__"," ");
}
elseif ($eIdioma == "it")
{
	define("__KEYS_EXTRA__"," ");
}
elseif ($eIdioma == "fr")
{
	define("__KEYS_EXTRA__"," ");
}

// Administrador
	define("__TITULO_ADMINISTRADOR__","Admin Online",true);
	define("__SEPARADOR_TITULO__"," :: ",true);
	define("__APP_VERSION__","3.0.0 :: GZIP Enabled",true);
	define("__HOJA_INICIO_ADMIN__","add_fotos.php");

// Metas
	define("__META_KEYWORDS__","");
	define("__META_DESCRIPTION__","");
	define("__META_TITLE__","");

//MySQL
	define("__HOST__","localhost");
	define("__BBDD__","alojamientos");
	define("__BBDD_USER__","alojamientos");
	define("__BBDD_PWD__","movie666");

// Anchuras de las im genes
	define("__IMG_NOTICIA__",116);
	define("__IMG_PASE__",647);
	define("__IMG_LISTA__",270);
	define("__IMG_HOME__",217);

// Tablas
	// phpBB
	// Resto
	//define("__TABLA_ADMINISTRADOR__","ex_usuarios");
	define("__TABLA_LITERALES__","literales");
	define("__TABLA_STRINGS__","strings");
	define("__TABLA_USUARIOS__","usuarios");
	define("__TABLA_REGISTRO__","registro");
	define("__TABLA_CONTACTOS__","contactos");
	define("__TABLA_HOME__","home");

	// Tablas de la BD de Reservas
//	define("__TABLA_USUARIOS__","rsv_usuarios");
	define("__TABLA_ESTABLECIMIENTOS__","rsv_establecimientos");
	define("__TABLA_LOCALIDADES__","rsv_localidades");
	define("__TABLA_PROVINCIAS__","rsv_provincias");
	define("__TABLA_PAISES__","rsv_paises");

	define("__ABR_ALEMAN__","de");
	define("__ABR_FRANCES__","fr");
	define("__ABR_ESPANIOL__","es");
	define("__ABR_INGLES__","en");
	define("__ABR_ITALIANO__","it");
?>
