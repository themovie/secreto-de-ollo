<!--
	Martin 27/11/07. Estos estilos se usan en la pagina inc_calendario_temporada_fechas.php. 
-->
<style type="text/css">
<!--
/*body
{
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}*/
<?php
//	$lCadena = "SELECT * FROM ".__TABLA_COLORES_TEMPORADAS__.
//					" WHERE ididioma = ".$lIdIdioma;
	// Martin 21/04/08. Seleccionamos las temporadas del establecimiento para mostrar la 
	//leyenda. 
	// En temp.idtipo se guarda el apartamento al que se asocia la temporada, en establecimientos 
	//de tipo hotel ponemos el valor -1 porque las temporadas van asociadas al establecimiento 
	$lCadena = "SELECT temp.idtemporada, temp.idcolor".
					", col_temp.background, col_temp.color".
					" FROM ".__TABLA_TEMPORADAS__." temp".
					", ".__TABLA_COLORES_TEMPORADAS__." col_temp".
					" WHERE temp.idcolor = col_temp.idcolor".
					" AND temp.idestablecimiento = ".$lIdEstablecimiento.
					" AND temp.idtipo = -1".
					" AND temp.visible = 1".
					" AND temp.borrado = 0";
	$rsTemporadas = fQuery($lCadena);
	$lNumTemporadas = mysql_num_rows($rsTemporadas);

	for ($li=0;$li<$lNumTemporadas;$li++)
	{
?>
#color<?php print(mysql_result($rsTemporadas,$li,"idcolor")); ?>
{
	background-color: <?php print(mysql_result($rsTemporadas,$li,"background")); ?>;
	color: <?php print(mysql_result($rsTemporadas,$li,"color")); ?>;
}
/*td#color<?php //print(mysql_result($rsTemporadas,$li,"idcolor")); ?> a
{
	color: <?php //print(mysql_result($rsTemporadas,$li,"color")); ?>;
}*/
<?php
	}
?>
-->
</style>
<?php
	// Martin 22/04/08. Guardamos en una matriz las temporadas asociadas al establecimiento.
	//Los indices de la matriz seran los idtemporada y en cada posicion guardaremos el color 
	//de fondo que se le ha asignado.
	$laTempColores = array();
	for ($li=0;$li<$lNumTemporadas;$li++)
	{
		$laTempColores[mysql_result($rsTemporadas,$li,"idtemporada")] = mysql_result($rsTemporadas,$li,"idcolor");
	}

	//  Martin 22/04/08. 
	//  Guardaremos los precios de las habitaciones en una matriz llamada laTablaHabPreciosTemp. 
	//  Los indices de la matriz seran de la forma "idtipo-personas", donde idtipo es el 
	//identificador de la habitacion y personas sera 0 si la hab tiene plazas_max = plazas_min. 
	//Si la hab. tiene plazas_max > plazas_min habra una entrada por cada posible ocupacion de la 
	//hab. (si tenemos una hab. con idtipo=8; plazas_min=2; plazas_max=3; en la matriz 
	//tendremos 2 entradas para la hab.: "8-2","8-3"). 
	//  Para crear los indices se tendran en cuenta los datos de todas las temporadas para abarcar 
	//el maximo rango de ocupacion de cada hab. 
	//  En cada entrada de la matriz se creara otra matriz cuyos indices seran los idtemporada del 
	//establecimiento y cuyo valor sera el precio correspondiente a la temporada para la habitacion 
	//y ocupacion indicada por el indice correspondiente. El precio de la temporada normal se 
	//metera con indice 0. Habra una entrada con indice -1 en la que guardaremos el nombre de la 
	//habitacion y entre parentesis para que ocupacion si corresponde.
	$lCadena = "SELECT hab_tipo.idtipo, hab_tipo.nombre".
					", hab_est.plazas_min, hab_est.plazas_max".
					" FROM ".__TABLA_HABITACIONES_TIPOS__." hab_tipo".
					", ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__." hab_est".
					" WHERE hab_tipo.idtipo = hab_est.idtipo".
					" AND hab_tipo.idestablecimiento = ".$lIdEstablecimiento.
					" AND hab_tipo.ididioma = ".$lIdIdioma.
					" AND hab_tipo.visible = 1".
					" AND hab_tipo.borrado = 0".
					" ORDER BY hab_tipo.nombre";
	$rsHab = fQuery($lCadena);
	$lNumHab = mysql_num_rows($rsHab);
	$laTablaHabPreciosTemp = array();
	for ($li=0;$li<$lNumHab;$li++)
	{
		$lIdtipo = mysql_result($rsHab,$li,"idtipo");
		$lNombre = mb_strtoupper(mysql_result($rsHab,$li,"nombre"));
		$lPlazas_min = mysql_result($rsHab,$li,"plazas_min");
		$lPlazas_max = mysql_result($rsHab,$li,"plazas_max");

		// Martin 15/01/10. Tenemos en cuenta que los registros no esten borrados.
		$lCadena = "SELECT MIN(IFNULL(plazas_min,0)) plazas_min_temp, MAX(IFNULL(plazas_max,0)) plazas_max_temp".
						" FROM ".__TABLA_HABITACIONES_TEMPORADAS__.
						" WHERE idtipo = ".$lIdtipo.
						" AND idestablecimiento = ".$lIdEstablecimiento.
						" AND borrado = 0".
						" GROUP BY idtipo, idestablecimiento";
		$rsPlazasTemp = fQuery($lCadena);
		if (mysql_num_rows($rsPlazasTemp) > 0)
		{
			$lPlazas_min_temp = mysql_result($rsPlazasTemp,0,"plazas_min_temp");
			$lPlazas_max_temp = mysql_result($rsPlazasTemp,0,"plazas_max_temp");
			if ($lPlazas_min_temp < $lPlazas_min)
			{
				$lPlazas_min = $lPlazas_min_temp;
			}
			if ($lPlazas_max_temp > $lPlazas_max)
			{
				$lPlazas_max = $lPlazas_max_temp;
			}
		} //fin if (mysql_num_rows($rsPlazasTemp) > 0)...
		if ($lPlazas_min == $lPlazas_max)
		{
			// La habitacion solo tiene una posible ocupacion y ponemos 0 en el lugar del indice 
			//correspondiente a personas.
			$laTablaHabPreciosTemp[$lIdtipo."-"."0"] = array( -1 => mb_strtoupper("Habitaci�n")." ".$lNombre);
		}
		elseif ($lPlazas_min < $lPlazas_max)
		{
			for ($lj=$lPlazas_min;$lj<=$lPlazas_max;$lj++)
			{
				// La habitacion tiene varias posibles ocupaciones, asi que insertamos una entrada en el array 
				//por cada posibilidad de ocupacion, indicando la ocupacion en el lugar del indice 
				//correspondiente a personas.
				$laTablaHabPreciosTemp[$lIdtipo."-".$lj] = array( -1 => mb_strtoupper("Habitaci�n")." ".$lNombre." (".$lj." ".mb_strtoupper("personas").")");
			}
		} //fin if ($lPlazas_min == $lPlazas_max)...
	} //fin for ($li=0;$li<$lNumHab;$li++)...
	foreach ($laTablaHabPreciosTemp as $lIndiceAct => $laPreciosAct)
	{
		// Martin 22/04/08. En $laIndiceActPartes[0] tenemos el idtipo y en $laIndiceActParte[1] tenemos 
		//el numero de personas.
		$laIndiceActPartes = explode("-",$lIndiceAct);
		if ($laIndiceActPartes[1] == 0)
		{
			// Martin 22/04/08. La habitacion tiene ocupacion fija, no se aplica un precio diferente segun 
			//el numero de personas que vayan a la habitacion. 
			$lCadena = "SELECT precio".
							" FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__.
							" WHERE idtipo = ".$laIndiceActPartes[0];
			$rsPrecioTempNormal = fQuery($lCadena);
			if (mysql_num_rows($rsPrecioTempNormal) > 0)
			{
				$laTablaHabPreciosTemp[$lIndiceAct][0] =  number_format(mysql_result($rsPrecioTempNormal,0,"precio"),2,",","");
			}
			for ($li=0;$li<$lNumTemporadas;$li++)
			{
				$lIdTemp = mysql_result($rsTemporadas,$li,"idtemporada");
				$lCadena = "SELECT precio".
								" FROM ".__TABLA_HABITACIONES_TEMPORADAS__.
								" WHERE idtipo = ".$laIndiceActPartes[0].
								" AND idtemporada = ".$lIdTemp;
				$rsPrecioTemp = fQuery($lCadena);
				$lNumPrecioTemp = mysql_num_rows($rsPrecioTemp);
				if ($lNumPrecioTemp > 0)
				{
					$lPrecioTemp = mysql_result($rsPrecioTemp,0,"precio");
					$laTablaHabPreciosTemp[$lIndiceAct][$lIdTemp] = number_format($lPrecioTemp,2,",","");
				}
				else
				{
					$laTablaHabPreciosTemp[$lIndiceAct][$lIdTemp] = "";
				}
			}//fin for ($li=0;$li<$lNumTemporadas;$li++)
		}//fin for ($li=0;$li<$lNumTemporadas;$li++)...
		else
		{
			// Martin 23/04/08. La habitacion aplica precios diferentes segun la ocupacion. 
			//En $laIndiceActPartes[1] tenemos las personas para las que deberemos calcular el precio.
			$lCadena = "SELECT precio, precio_personas_extra".
							", plazas_min, plazas_max".
							" FROM ".__TABLA_HABITACIONES_ESTABLECIMIENTOS__.
							" WHERE idtipo = ".$laIndiceActPartes[0];
			$rsPrecioTempNormal = fQuery($lCadena);
			if (mysql_num_rows($rsPrecioTempNormal) > 0)
			{
				$lPrecioTempNormal = mysql_result($rsPrecioTempNormal,0,"precio");
				$laPrecio_pers_extra_TempNormal = explode(";",mysql_result($rsPrecioTempNormal,0,"precio_personas_extra"));
				$lPlazas_min_TempNormal = mysql_result($rsPrecioTempNormal,0,"plazas_min");
				$lPlazas_max_TempNormal = mysql_result($rsPrecioTempNormal,0,"plazas_max");
				$lPrecio_act = $lPrecioTempNormal;
				$lPlazas_act = $lPlazas_min_TempNormal;
				$lPosicion_precio_pers_extra_act = 0;
				while ($lPlazas_act + 1 <= $laIndiceActPartes[1])
				{
					if ($laPrecio_pers_extra_TempNormal[$lPosicion_precio_pers_extra_act] == "")
					{ $laPrecio_pers_extra_TempNormal[$lPosicion_precio_pers_extra_act] = 0; }
					$lPrecio_act += $laPrecio_pers_extra_TempNormal[$lPosicion_precio_pers_extra_act];
					$lPlazas_act++;
					$lPosicion_precio_pers_extra_act++;
				}
				$laTablaHabPreciosTemp[$lIndiceAct][0] = number_format($lPrecio_act,2,",","");
			} //fin if (mysql_num_rows($rsPrecioTempNormal) > 0)...
			for ($li=0;$li<$lNumTemporadas;$li++)
			{
				$lIdTemp = mysql_result($rsTemporadas,$li,"idtemporada");
				$lCadena = "SELECT precio, precio_personas_extra".
								", plazas_min, plazas_max".
								" FROM ".__TABLA_HABITACIONES_TEMPORADAS__.
								" WHERE idtipo = ".$laIndiceActPartes[0].
								" AND idtemporada = ".$lIdTemp;
				$rsPrecioTemp = fQuery($lCadena);
				$lNumPrecioTemp = mysql_num_rows($rsPrecioTemp);
				if ($lNumPrecioTemp > 0)
				{
					$lPrecioTemp = mysql_result($rsPrecioTemp,0,"precio");
					$laPrecio_pers_extra_Temp = explode(";",mysql_result($rsPrecioTemp,0,"precio_personas_extra"));
					$lPlazas_min_Temp = mysql_result($rsPrecioTemp,0,"plazas_min");
					$lPlazas_max_Temp = mysql_result($rsPrecioTemp,0,"plazas_max");
					$lPrecio_act = $lPrecioTemp;
					$lPlazas_act = $lPlazas_min_Temp;
					$lPosicion_precio_pers_extra_act = 0;
					while (( $lPlazas_act + 1) <= $laIndiceActPartes[1])
					{
						if ($laPrecio_pers_extra_Temp[$lPosicion_precio_pers_extra_act] == "")
						{ $laPrecio_pers_extra_Temp[$lPosicion_precio_pers_extra_act] = 0; }
						$lPrecio_act += $laPrecio_pers_extra_Temp[$lPosicion_precio_pers_extra_act];
						$lPlazas_act++;
						$lPosicion_precio_pers_extra_act++;
					}
					$laTablaHabPreciosTemp[$lIndiceAct][$lIdTemp] = number_format($lPrecio_act,2,",","");
				}
				else
				{
					$laTablaHabPreciosTemp[$lIndiceAct][$lIdTemp] = "";
				}
			} //fin for ($li=0;$li<$lNumTemporadas;$li++)...
		} //fin if ($laIndiceActPartes[1] == 0)...
	} //fin foreach ($laTablaHabPreciosTemp as $lIndiceAct => $laPreciosAct)...

	//  Martin 23/04/08. 
	//  Guardaremos los precios de los servicios opcionales en la matriz laTablaAdiTemp.
	//  Los indices de la matriz seran los identificadores de los serv. opcionales. Solo 
	//utilizamos serv. opcionales a nivel de establecimiento (son los que en la tabla 
	//tienen un -1 en el campo idtipo).
	//  A cada entrada de la matriz se le asignara una matriz. En esta segunda matriz en el 
	//indice -1 se guardara el nombre del serv. opcional, en el indice 0 se guardara el 
	//precio de la temporada normal y por cada temporada del establecimiento se guardara 
	//otra vez el precio del servicio (hay una tabla en la BD para poder guardar precios 
	//distintos segun la temporada pero no la estamos utilizando.).
	$lCadena = "SELECT adi_hab.idservicio, adi_hab.precio".
					", adi_trad.nombre".
					" FROM ".__TABLA_ADICIONALES_HABITACIONES__." adi_hab".
					", ".__TABLA_ADICIONALES_TRADUCCION__." adi_trad".
					" WHERE adi_hab.idservicio = adi_trad.idservicio".
					" AND adi_hab.idtipo = -1".
					" AND adi_hab.idestablecimiento = ".$lIdEstablecimiento.
					" AND adi_trad.ididioma = ".$lIdIdioma.
					" AND adi_hab.visible = 1".
					" AND adi_hab.borrado = 0";
	$rsServAdi = fQuery($lCadena);
	$lNumServAdi = mysql_num_rows($rsServAdi);
	$laTablaAdiTemp = array();
//print("lCadena=".$lCadena."<br />");
//print("lNumServAdi=".$lNumServAdi."<br />");
	for ($li=0;$li<$lNumServAdi;$li++)
	{
		$lIdServicioAct = mysql_result($rsServAdi,$li,"idservicio");
		$lPrecioServAct = mysql_result($rsServAdi,$li,"precio");
		$lNombreServAct = mb_strtoupper(mysql_result($rsServAdi,$li,"nombre"));
//print("lIdServicioAct=".$lIdServicioAct."; ");
//print("lNombreServAct=".$lNombreServAct."; ");
//print("lPrecioServAct=".$lPrecioServAct."; <br />");
		$laTablaAdiTemp[$lIdServicioAct] = array();
		$laTablaAdiTemp[$lIdServicioAct][-1] = $lNombreServAct;
		$laTablaAdiTemp[$lIdServicioAct][0] = $lPrecioServAct;
		for ($lj=0;$lj<$lNumTemporadas;$lj++)
		{
//print("  laTablaAdiTemp[".$lIdServicioAct."][".mysql_result($rsTemporadas,$lj,"idtemporada")."]=".$lPrecioServAct."; <br />");
			$laTablaAdiTemp[$lIdServicioAct][mysql_result($rsTemporadas,$lj,"idtemporada")] = $lPrecioServAct;
		}
	}//fin for ($li=0;$li<$lNumServAdi;$li++)...
?>
<div class="span-10 prepend-1 append-1 last">
	<table id="tablatarifasyofertas">
<?php
	foreach ($laTablaHabPreciosTemp as $lHabOcupacionActual => $laDescPreciosActual)
	{
?>
		<tr>
<?php
		$lColorCeldaPrecios = "";
		foreach ($laDescPreciosActual as $lIdTemporada => $lValor)
		{
			if ($lIdTemporada > 0)
			{
				$lColorCeldaPrecios = "color".$laTempColores[$lIdTemporada];
			}
?>
			<td id="<?php print($lColorCeldaPrecios); ?>">
				<?php print($lValor); ?>
			</td>
<?php
			$lColorCeldaPrecios = "";
			//print($lIndice." => ".$lValor."  ");
		}
		//print("<br />");
?>
		</tr>
<?php
	}
?>
<?php
	foreach ($laTablaAdiTemp as $lAdiActual => $laAdiDescPreciosActual)
	{
?>
		<tr>
<?php
		$lColorCeldaPrecios = "";
		foreach ($laAdiDescPreciosActual as $lAdiIdTemporada => $lAdiValor)
		{
			if ($lAdiIdTemporada > 0)
			{
				$lColorCeldaPrecios = "color".$laTempColores[$lAdiIdTemporada];
			}
?>
			<td id="<?php print($lColorCeldaPrecios); ?>">
				<?php print($lAdiValor); ?>
			</td>
<?php
			$lColorCeldaPrecios = "";
		}
?>
		</tr>
<?php
	}
?>
	</table>
</div>
<div class="span-12 last">
					<table class="tablacalendariodisp">
<?php
// Martin 18/04/08, inicializamos las variables $lMes y $lAnio con el valor del anio actual.
$lMes = 1;
$lMes = date("n",time());
$lAnio = date("Y",time());
//$lfechaunixprueba = mktime(0,0,0,7,1,2008);
//$lMes = date("n",$lfechaunixprueba);
//$lAnio = date("Y",$lfechaunixprueba);
if ($_SESSION["idtipo"]=="1")
{
	//$lPrimerDia = strtotime("1/1/".$lAnio." 12:00:00");
	$lPrimerDia = strtotime($lMes."/1/".$lAnio." 12:00:00");
	$lUltimoMes = date("n",fSumarMeses($lPrimerDia,11));
	$lNumDiasUltimoMes = fDiasMes($lUltimoMes,($lAnio + 1));
	//$lUltimoDia = strtotime("12/31/".$lAnio." 12:00:00");
	if ($lUltimoMes < $lMes) 
	{
		// Martin 11/12/08. Estamos en el caso normal en que el ultimo mes es menor que el mes inicial, por ejemplo en 
		//un calendario de 05/2008 a 04/2009. Para calcular el ultimo dia hay que sumar 1 al anio.
		$lUltimoDia = strtotime($lUltimoMes."/".$lNumDiasUltimoMes."/".($lAnio + 1)." 12:00:00");
	}
	else
	{
		// Martin 11/12/08. Estamos en el unico caso en que el ultimo mes no es menor que el mes inicial (calendario de 
		//01/2008 a 12/2008 por ejemplo), entonces el ultimo dia hay que calcularlo para el anio actual.
		$lUltimoDia = strtotime($lUltimoMes."/".$lNumDiasUltimoMes."/".$lAnio." 12:00:00");
	}
}
//Martin. Calculamos el n� de filas que necesitamos en la tabla del calendario para cada mes.
$lMesSem = $lMes;
$lAnioSem = $lAnio;
$laNumSemMes = array("");
//for ($lNumMes=0;$lNumMes<6;$lNumMes++)
for ($lNumMes=0;$lNumMes<12;$lNumMes++)
{
	if ($lNumMes > 0)
	{
		$lMesSem++;
	}
	// Martin 11/12/08. El if comentado estaba provocando que las filas que comenzaban con un mes del anio siguiente 
	//se viesen mal porque no se estaba calculando bien su numero de semanas
	//if ($lMes == 13)
	if ($lMesSem == 13)
	{
		$lMesSem = 1;
		$lAnioSem += 1;
	}
	$lNumPrimSem = date("W",strtotime($lMesSem."/1/".$lAnioSem." 12:00:00"));
	if ($lNumPrimSem >= 52 ) 
	{
		$lNumPrimSem = 0;
	}
	$lUltDiaMes = fDiasMes($lMesSem,$lAnioSem);
	$lNumUltSem = date("W",strtotime($lMesSem."/".$lUltDiaMes."/".$lAnioSem." 12:00:00"));
	if ($lNumUltSem==1) 
	{
		$lUltDiaMes -= 7;
		$lNumUltSem = date("W",strtotime($lMesSem."/".$lUltDiaMes."/".$lAnioSem." 12:00:00")) + 1;
	}
	$lNumSemMes = ($lNumUltSem - $lNumPrimSem) + 1;
//echo("lNumSemMes=".$lNumSemMes."<br />");	
	$laNumSemMes[$lNumMes] = $lNumSemMes;
}
//print_r($laNumSemMes);print('<br />');

$lNumMesInicial = 0;
//for ($lNumMes=0;$lNumMes<6;$lNumMes++)
for ($lNumMes=0;$lNumMes<12;$lNumMes++)
{
	if ($lNumMes > 0)
	{
		$lMes++;
	}
	if ($lMes == 13)
	{
		$lMes = 1;
		$lAnio += 1;
	}
//	if (($lNumMes%2)==0)
//	{
//		$lNumFilasCal = max($laNumSemMes[$lNumMes],$laNumSemMes[($lNumMes+1)]);
	//Mart�n. Calculamos el m�ximo n� de filas de los meses de una fila del calendario.
	if ((($lNumMes-$lNumMesInicial)%3)==0)
	{
		$lNumFilasCal = max($laNumSemMes[$lNumMes],$laNumSemMes[($lNumMes+1)],$laNumSemMes[($lNumMes+2)]);
//	if ((($lNumMes-$lNumMesInicial)%3)==0)
//	{
//		$lNumFilasCal = max($laNumSemMes[$lNumMes],$laNumSemMes[($lNumMes+1)],$laNumSemMes[($lNumMes+2)]);
?>
						<tr>
<?php
	}
?>

							<!--<td align="center" >-->
							<td>
							<table class="tablamescalendariodisp">

								<tr>
									<td width="100%" colspan="7" class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td class="celdacalendariodisp_nombre">
													<?php 
														//Martin 17/04/08, Sustituimos la variable de sesion que usamos en el administrador 
														//por la variable $lIdIdioma que se estable en inc_comun.php.
														// Martin 21/04/08. Anadimos el anio.
														$lNombreMes = fNombreMes($lMes,$lIdIdioma);
														print(strtoupper(substr($lNombreMes,0,1)).substr($lNombreMes,1)." ".$lAnio);
													?>
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
<?php
	//Mart�n. Ponemos los d�as de la semana de un mes.
	for ($li=1;$li<=7;$li++) 
	{
?>
									<td width="14%" class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td class="celdacalendariodisp_nombre">
										<?php 
											print(substr(fDiaSemana($li,"c"),0,1));
										?>
												</td>
											</tr>
										</table>
									</td>
<?php
	}
?>
								</tr>
<?php 
	$lDiasMes = fDiasMes($lMes,$lAnio);
	$lDiaMes = 1;
	
//echo("lNumFilasCal=".$lNumFilasCal."<br />");
	//for ($li=1;$li<=6;$li++)
	//Mart�n. Construimos un mes.
	for ($li=1;$li<=$lNumFilasCal;$li++)
	{
?>
								<tr>
<?php
		for ($lj=1;$lj<=7;$lj++)
		{
//echo("lDiaMes=".$lDiaMes."; lDiasMes=".$lDiasMes."<br />");
			if ($lDiaMes<=$lDiasMes) 
			{
				//Mart�n. Calculamos el dia de la semana del primer d�a del mes. 1-Lunes; 2-Mantes; ... 7-Domingo.
				$lDiaSemana = date("w",strtotime($lMes."/".$lDiaMes."/".$lAnio." 12:00:00"));
				$lFechaUnix = strtotime($lMes."/".$lDiaMes."/".$lAnio." 12:00:00");
				if ($lDiaSemana == 0) 
				{
					$lDiaSemana = 7;
				}
//echo("lDiaSemana=".$lDiaSemana."; lj=".$lj.".<br />");
				//Mart�n. No empezaremos a dibujar d�as en el calendario hasta que $lj coincida con el 
				//primer d�a de la semana. 
				if ($lj==$lDiaSemana) 
				{
//echo("Mensaje1. <br />");

					if ($_SESSION["idtipo"]=="1")
					{
						$lCadena = "SELECT temp.idcolor".
								" FROM ".__TABLA_TEMPORADAS__." AS temp,".
										__TABLA_TEMPORADAS_FECHAS__." AS temp_fecha".
								" WHERE temp.idestablecimiento = temp_fecha.idestablecimiento".
								" AND temp.idtemporada = temp_fecha.idtemporada".
								" AND temp.idestablecimiento = ".$lIdEstablecimiento.
								" AND temp_fecha.fecha = ".$lFechaUnix.
								" AND temp.visible = 1".
								" AND temp.borrado = 0";
						$rsColorTemporada = fQuery($lCadena);

						$esLunes = "";
						if ($lDiaSemana==1)
						{
							$esLunes = "Lunes";
						}

						$lColorCelda = "";

						$lClaseCelda = " class=\"celdacalendariodisp";
						$lClaseCelda = $lClaseCelda.$esLunes."\"";

						if (mysql_num_rows($rsColorTemporada)>0) 
						{
							$lColorTemporada = mysql_result($rsColorTemporada,0,"idcolor");
							//$lClase = "color".$lColorCelda;
							$lColorCelda = " id=\"color".$lColorTemporada."\"";
						}
						else
						{
							$lColorCelda = "";
						}

					}
					else
					{
						$esLunes = "";
						if ($lDiaSemana==1)
						{
							$esLunes = "Lunes";
						}
						$lColorCelda = "";

						$lClaseCelda = " class=\"celdacalendariodisp";
						$lClaseCelda = $lClaseCelda.$esLunes."\"";
					} //fin if ($_SESSION["idtipo"]=="1")

?>
									<td width="14%"  class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td <?php print($lClaseCelda.$lColorCelda); ?>>
<?php
					if ($_SESSION["idtipo"] == 1)
					{
						print("$lDiaMes");
					}
?>
												</td>
											</tr>
										</table>
									</td>
<?php
					$lDiaMes++;
				} // fin  if ($lj==$lDiaSemana)...
				//$lDiaMes++;
				//else
				//{
				//	break;
				//}
			} // fin  if ($lDiaMes<=$lDiasMes)...
			if (($lj!=$lDiaSemana) || 
					(($lj==$lDiaSemana) 
						&& ($lDiaMes>($lDiasMes)) 
						&& ($li==$lNumFilasCal) 
						&& ($lNumFilasCal>$laNumSemMes[$lNumMes]) ))
			{
				$esLunes = "";
				if ($lj==1)
				{
					$esLunes = "Lunes";
				}	
//echo("Mensaje2. lDiaMes=".$lDiaMes."; lDiasMes=".$lDiasMes."; lj=".$lj."; lDiaSemana=".$lDiaSemana."; <br />");
?>
									<td width="14%"  class="celdamaestradisp">
										<table class="tabladiacalendariodisp">
											<tr>
												<td class="celdacalendariodisp<?php print($esLunes); ?>">&nbsp;</td>
											</tr>
										</table>
									</td>
<?php
			} //fin  if (($lj!=$lDiaSemana) || ...
			//$lDiaMes++;
//echo("Dia en blanco. <br />");
		} // fin  for ($lj=1;$lj<=7;$lj++)...
//		if ($lDiaMes>$lDiasMes) 
//		{
//			break;
//		}
?>
								</tr>
<?php
	} // fin  for ($li=1;$li<=$lNumFilasCal;$li++)...
?>
							</table>
							</td>
<?php 
//echo("Mensaje1. lNumMes=".$lNumMes."<br />");
	if ((($lNumMes-$lNumMesInicial)%3)==2)
	{
		//Mart�n. Cerramos una fila de meses
?>
						</tr>
<?php 
	}
?>
<?php
} // fin  for ($lNumMes=0;$lNumMes<12;$lNumMes++)...
//echo("Mensaje2. lNumMes=".$lNumMes."<br />");
?>
					</table>
</div>
