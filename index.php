<?php
	ob_start("ob_gzhandler"); // Comprimir el HTML antes de enviarlo al navegador
	include("inc_comun.php");


	// Martin 11/04/08. Las variables $lIdEstablecimiento y $lIdIdioma se establecen en 
	//el script inc_comun.php.
	$lIdimagen_est_actual = $_GET["idimg_est"];
	if ($lIdimagen_est_actual == "")
	{
		$lIdimagen_est_actual = $_SESSION["idimagen_est_actual"];
	}
	if ($lIdimagen_est_actual == "")
	{
		// Martin 07/04/09. Seleccionamos las fotos de tipo "establecimiento".
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lCadena = " SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							//" AND img.idimagen = ".$lIdimagen_est_act.
							" AND img.idimagen_tipo = 1". //Martin 07/04/09. Nuevo.
							" AND img.idtipo = -1".
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img_trad.ididioma = '".fLimpiar_sql($lIdIdioma)."'".
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
	}
	else
	{
		// Martin 07/04/09. Seleccionamos las fotos de tipo "establecimiento".
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lCadena = " SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							" AND img.idimagen_tipo = 1". //Martin 07/04/09. Nuevo.
							" AND img.idimagen = '".fLimpiar_sql($lIdimagen_est_actual)."'".
							" AND img.idtipo = -1".
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img_trad.ididioma = '".fLimpiar_sql($lIdIdioma)."'".
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
	}
	$rsFotoEstAct = fQuery($lCadena);
	if (mysql_num_rows($rsFotoEstAct) > 0)
	{
		// Martin 15/09/10. Ahora usamos el campo imagen_pase.
		$lIdimagenAct = mysql_result($rsFotoEstAct,0,"idimagen");
		//$lImagenAct = mysql_result($rsFotoEstAct,0,"imagen_lista");
		$lImagenAct = mysql_result($rsFotoEstAct,0,"imagen_pase");
		$lTituloImgAct = mysql_result($rsFotoEstAct,0,"titulo");
		$lDescrImgAct = mysql_result($rsFotoEstAct,0,"descripcion");
		$lSrcImgAct = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagenAct;
		//$lSrcImgAct = "http://hotelejemplo.reservadealojamientos.com/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagenAct;
		// Martin 15/09/10. Tratamiento para redimensionar la imagen.
		try {
			// Obtenemos la informacion de la foto.
			$lMaxAnchura = 470;
			$lMaxAltura = 625;
			$laInformacionFotosAct = getimagesize($lSrcImgAct); 
			$lWidthFotoAct = $laInformacionFotosAct[0]; //Anchura de la foto.
			$lHeightFotoAct = $laInformacionFotosAct[1]; //Altura de la foto.
			$lLeftFotoAct = 0;
			if ($lWidthFotoAct > $lMaxAnchura) {
				$lHeightFotoAct = floor(($lMaxAnchura * $lHeightFotoAct) / $lWidthFotoAct);
				$lWidthFotoAct = $lMaxAnchura;
			}
			if ($lHeightFotoAct > $lMaxAltura) {
				$lWidthFotoAct = floor(($lMaxAltura * $lWidthFotoAct) / $lHeightFotoAct);
				$lHeightFotoAct = $lMaxAltura;
			}
			if ($lWidthFotoAct < $lMaxAnchura) {
				$lLeftFotoAct = floor(($lMaxAnchura - $lWidthFotoAct) / 2);
			}
		}
		catch (Exception $e) {
			$lWidthFotoAct = 0;
			$lHeightFotoAct = 0;
			$lLeftFotoAct = 0;
		}
	}
	$_SESSION["idimagen_est_actual"] = $lIdimagenAct;

	// Martin 05/05/08. Seleccionamos la descripcion del establecimiento.
	// Las variables $lIdEstablecimiento y $lIdIdioma.
	$lCadena = " SELECT descripcion".
						" FROM ".__TABLA_ESTABLECIMIENTOS_NOMBRES__.
						" WHERE idestablecimiento = ".$lIdEstablecimiento.
						" AND ididioma = ".$lIdIdioma;
	$rsEstDesc = fQuery($lCadena);
	if (mysql_num_rows($rsEstDesc) > 0)
	{
		$lEstDescripcion = mysql_result($rsEstDesc,0,"descripcion");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include("include/modulos/inc_metas.php"); ?>
<!--CSS -->
	<!--CSS de lightwindow-->
	<link rel="stylesheet" href="lightwindow/css/lightwindow.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="lightwindow/css/default.css" type="text/css" media="screen"/>
	<!--Fin CSS de lightwindow-->
<link rel="stylesheet" href="css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/blueprint/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
<link rel="stylesheet" href="css/ficha_reservas.css" type="text/css" media="screen, projection" />
	<!--Javascript de lightwindow-->
	<script type="text/javascript" src="lightwindow/javascript/prototype.js"></script>
	<script type="text/javascript" src="lightwindow/javascript/effects.js"></script>
	<script type="text/javascript" src="lightwindow/javascript/lightwindow.js"></script>
	<!--Fin Javascript de lightwindow-->
<script src="include/js/funciones.js"></script>
<title><?php print("El Secreto de Ollo hotel rural en navarra cerca pamplona navarra agroturismos ".fLiteral(357,$lIdIdioma)); //Presentaci�n ?></title>

<?php 
	// Martin 29/04/09. Recuperamos todas las fotos del establecimiento e introducimos las rutas a cada 
	//foto en un array de javascript. 
	// El array javascript tendra 2 dimensiones. Cada indice del array sera otro array con 5 elementos:
	//		0-idimagen
	//		1-titulo de la imagen
	//		2-descripcion de la imagen
	//		3-ruta a la imagen
	//		4-objeto Image
	// Creamos tambien un array con las imagenes grandes para el pase de fotos. Sera un array de 2 dimensiones.
	//Cada indice numerico del array contendra otro array con 4 elementos (sera un array asociativo).
	//		0-idimagen
	//		1-titulo de la imagen
	//		2-descripcion de la imagen
	//		3-ruta a la imagen
	$lCadena = "SELECT img.idimagen, img.imagen_lista, img.imagen_pase".
						", img_trad.titulo, img_trad.descripcion".
						" FROM ".__TABLA_IMAGENES__." AS img".
						", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
						" WHERE img.idimagen = img_trad.idimagen".
						" AND img.idimagen_tipo = 1". //Martin 07/04/09. Nuevo.
						" AND img.idestablecimiento = ".$lIdEstablecimiento.
						" AND img.idtipo = -1".
						" AND img_trad.ididioma = ".$lIdIdioma.
						" AND img.visible = 1".
						" AND img.borrado = 0".
						" ORDER BY img.prioridad, img.titulo";
	$rsFotosEst = fQuery($lCadena);
	$lNumFotosEst = mysql_num_rows($rsFotosEst);
	$aFotos_pase = array();
	$lIndFotoAct = 0;
?>
<script type="text/javascript">
<?php /*
//	var lista_fotos_cab = new Array();
//	var v_foto_cab_act = 0;
//	var v_num_fotos_cab = <?php //print($lNumFotosEst); ?>;
*/ ?>
<?php 
	if ($lNumFotosEst > 0) {
		for ($li=0;$li<$lNumFotosEst;$li++) {
			$aFila_act = mysql_fetch_array($rsFotosEst);
			$lImagen_act = $aFila_act["imagen_lista"];
			$lSrc_img_act = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagen_act;
			$lImagen_pase_act = ( ($aFila_act["imagen_pase"] != "") ? $aFila_act["imagen_pase"] : $lImagen_act );
			$lSrc_img_pase_act = "https://".__DOMINIO_CENTRAL__."/".__DIR_UPLOADS__.__DIR_ESTABLECIMIENTOS__.$lImagen_pase_act;
			$aFotos_pase[$li]["idimagen"] = $aFila_act["idimagen"];
			$aFotos_pase[$li]["titulo"] = $aFila_act["titulo"];
			$aFotos_pase[$li]["descripcion"] = $aFila_act["descripcion"];
			$aFotos_pase[$li]["ruta_imagen"] = $lSrc_img_pase_act;
			if ($aFotos_pase[$li]["idimagen"] == $lIdimagen_est_actual) {
				$lIndFotoAct = $li;
			}
?>
<?php /*
//			lista_fotos_cab[<?php //print($li); ?>] = new Array();
//			lista_fotos_cab[<?php //print($li); ?>][0] = <?php //print($aFila_act["idimagen"]); ?>;
//			lista_fotos_cab[<?php //print($li); ?>][1] = '<?php //print($aFila_act["titulo"]); ?>';
//			lista_fotos_cab[<?php //print($li); ?>][2] = '<?php //($aFila_act["descripcion"]); ?>';
//			lista_fotos_cab[<?php //print($li); ?>][3] = '<?php //print($lSrc_img_act); ?>';
//			lista_fotos_cab[<?php //print($li); ?>][4] = new Image();
//			lista_fotos_cab[<?php //print($li); ?>][4].src = '<?php //print($lSrc_img_act); ?>';
*/ ?>
<?php 
		} //fin for ($li=0;$li<$lNumFotosEst;$li++) {...
	} //fin if ($lNumFotosEst > 0) {...
?>
<?php /*
//	function cambiar_foto() {
//		document.getElementById('foto_pequena').src = lista_fotos_cab[v_foto_cab_act][3];
//		document.getElementById('foto_pequena').alt = lista_fotos_cab[v_foto_cab_act][1];
//		v_foto_cab_act++;
//		if (v_foto_cab_act >= v_num_fotos_cab) {
//			v_foto_cab_act = 0;
//		}
//		igualaColumnas3();
//	} //fin function cambiar_foto() {...

	// Martin 29/04/09. NO SE HARA LO DEL PASE DE FOTOS DE MOMENTO PORQUE AL CAMBIAR DE TAMANO LAS FOTOS SE DESCOLOCABA
	//EL DISENO.
	//window.setInterval("cambiar_foto()",2000);
*/ ?>
</script>

</head>
<body class="top" onload="igualaColumnas3();">
	<div class="container showgrid">
		<?php include("include/modulos/inc_cabecera.php"); ?>
		<!--<div class="span-23 top" id="contenido">-->
		<div class="span-22 push-1 top" id="contenido">
			<div id="contenido_adorno">
			</div>
			<div id="contenido_2">
				<?php include("include/modulos/inc_menu_navegacion.php"); ?>
				<div class="span-13" id="cuerpo">
					<!--<div class="span-13" id="titulo">-->
					<div id="titulo">
						<span class="hotel_rural">HOTEL RURAL&nbsp;</span>
						<?php print(ucfirst(mb_strtolower(fLiteral(349,$lIdIdioma)))); //PRESENTACION ?>
					</div>
					<!--<div class="span-13" id="foto">-->
					<div id="foto">
						<div id="foto_leyenda">
							<?php print($aFotos_pase[$lIndFotoAct]["titulo"]) ?>
						</div>
						<a class="lightwindow" caption="" author="" title="<?php print($aFotos_pase[$lIndFotoAct]["titulo"]); ?>" 
							 rel="Galer�a[Establecimiento]" href="<?php print($aFotos_pase[$lIndFotoAct]["ruta_imagen"]); ?>">
						<?php /*<img id="foto_pequena" src="<?php print($lSrcImgAct); ?>" alt="<?php print($lTituloImgAct); ?>" />*/ ?>
						<img id="foto_pequena" src="<?php print($lSrcImgAct); ?>" alt="<?php print($lTituloImgAct); ?>" 
							style="width: <?php print($lWidthFotoAct); ?>px; height: <?php print($lHeightFotoAct); ?>px; margin-left:<?php print($lLeftFotoAct); ?>px;" />
						</a>
<?php 
						if ($lNumFotosEst > 1) {
							for ($lk=($lIndFotoAct + 1);$lk<$lNumFotosEst;$lk++) {
?>
								<a class="lightwindow hidden" author="" caption="" title="<?php print($aFotos_pase[$lk]["titulo"]); ?>" 
									rel="Galer�a[Establecimiento]" href="<?php print($aFotos_pase[$lk]["ruta_imagen"]); ?>">imagen #<?php print($lk); ?></a>
<?php 
							}
							for ($lk=0;$lk<$lIndFotoAct;$lk++) {
?>
								<a class="lightwindow hidden" author="" caption="" title="<?php print($aFotos_pase[$lk]["titulo"]); ?>" 
									rel="Galer�a[Establecimiento]" href="<?php print($aFotos_pase[$lk]["ruta_imagen"]); ?>">imagen #<?php print($lk); ?></a>
<?php 
							}
						} //fin if ($lNumFotosEst > 1) {...
?>
					</div><!--foto-->
					<!--<div class="span-13" id="barra_seleccion_fotos">-->
					<div id="barra_seleccion_fotos">
						<!--<div id="texto_fotos">
							<?php //print(fLiteral(358,$lIdIdioma)); //Fotos ?>
						</div>-->
	<?php
		// Martin 08/04/08, Seleccionamos las fotos del establecimiento.
		// Martin 07/04/09. Seleccionamos las fotos de tipo "establecimiento".
		$lCadena = "SELECT img.idimagen, img.imagen_lista".
							", img_trad.titulo, img_trad.descripcion".
							" FROM ".__TABLA_IMAGENES__." AS img".
							", ".__TABLA_IMAGENES_TRADUCCIONES__." AS img_trad".
							" WHERE img.idimagen = img_trad.idimagen".
							" AND img.idimagen_tipo = 1". //Martin 07/04/09. Nuevo.
							" AND img.idestablecimiento = ".$lIdEstablecimiento.
							" AND img.idtipo = -1".
							" AND img_trad.ididioma = ".$lIdIdioma.
							" AND img.visible = 1".
							" AND img.borrado = 0".
							" ORDER BY img.prioridad, img.titulo";
		$rsFotosEst = fQuery($lCadena);
		$lNumFotosEst = mysql_num_rows($rsFotosEst);
		if ($lNumFotosEst > 0)
		{
			// Martin 14/04/08. Guardamos en una variable cuantos numeros de fotos mostraremos en la lista de fotos
			//seleccionable. Como mucho seran 12, pero si no se han subido tantas fotos ser�n el total de fotos.
			// Martin 05/05/09. Ampliamos el numero maximo de fotos a 16.
			$lNumElementosListaFotos = min($lNumFotosEst,1);
			for ($li=0;$li<$lNumFotosEst;$li++)
			{
				// Martin 14/04/08. Metemos todos los identificadores de fotos del establecimiento en un array.
				$lIdimagen = mysql_result($rsFotosEst,$li,"idimagen");
				$laListaFotosEst[$li] = $lIdimagen;
				if ($lIdimagen == $_SESSION["idimagen_est_actual"])
				{
					// Martin 14/04/08. Guardamos la posicion que ocupa en el array la foto actual en la variable de 
					//sesion lista_fotos_est_actual.
					$_SESSION["lista_fotos_est_actual"] = $li;
				}
			}
			// Martin 14/04/08. En las variables de sesion lista_fotos_est_primera y lista_fotos_est_ultima guardamos 
			//el primer y ultimo indice del array que mostraremos en la barra de fotos (por ejemplo del 3 al 14, ya que 
			//solo mostraremos como mucho 12 numeros de fotos para elegir).
			if ($_SESSION["lista_fotos_est_primera"] == "")
			{
				$_SESSION["lista_fotos_est_primera"] = 0;
				$_SESSION["lista_fotos_est_ultima"] = $lNumElementosListaFotos - 1;
			}
			if ($_SESSION["lista_fotos_est_actual"] > $_SESSION["lista_fotos_est_ultima"])
			{
				$_SESSION["lista_fotos_est_primera"]++;
				$_SESSION["lista_fotos_est_ultima"] = $_SESSION["lista_fotos_est_actual"];
			}
			if ($_SESSION["lista_fotos_est_actual"] < $_SESSION["lista_fotos_est_primera"])
			{
				$_SESSION["lista_fotos_est_primera"] = $_SESSION["lista_fotos_est_actual"];
				$_SESSION["lista_fotos_est_ultima"]--;
			} //fin if ($_SESSION["lista_fotos_est_primera"] == "")...
			// Martin 14/04/08. En las variables $lNumImagenAnterior y $lNumImagenSiguiente guardamos los indices del array 
			//de fotos anterior y posterior al indice actual. Se asignaran a los botones de navegacion que hay en los 
			//extremos de la barra de seleccion de  fotos.
			if ($_SESSION["lista_fotos_est_actual"] > 0)
			{
				$lNumImagenAnterior = $_SESSION["lista_fotos_est_actual"] - 1;
			}
			else
			{
				$lNumImagenAnterior = 0;
			} //fin if ($_SESSION["lista_fotos_est_actual"] > 0)...
			if ($_SESSION["lista_fotos_est_actual"] < ($lNumFotosEst - 1))
			{
				$lNumImagenSiguiente = $_SESSION["lista_fotos_est_actual"] + 1;
			}
			else
			{
				$lNumImagenSiguiente = $lNumFotosEst - 1;
			} //if ($_SESSION["lista_fotos_est_actual"] < $lNumFotosEst)...
			// Martin 14/04/08. Las variables $lParametros y $lParametrosSID se establece en el fichero /inc_comun.php
	?>
						<div class="numero_foto">
							<a href="index.php<?php print($lParametros."&idimg_est=".$laListaFotosEst[$lNumImagenAnterior].$lParametrosSID); ?>"> &lt; </a>
						</div>
	<?php
			for ($lj=$_SESSION["lista_fotos_est_primera"];$lj<=$_SESSION["lista_fotos_est_ultima"];$lj++)
			{
				$lNumfoto = $lj + 1;
				$lIdimagen = $laListaFotosEst[$lj];
				if ($lj != $_SESSION["lista_fotos_est_primera"]) {
	?>
						<div class="separacion"> | </div>
	<?php
				}
	//print("SID=".SID);
	?>
						<div class="<?php if ($lIdimagen == $_SESSION["idimagen_est_actual"]) { ?>foto_seleccionada<?php } else { ?>numero_foto<?php } ?>">
							<a href="index.php<?php print($lParametros."&idimg_est=".$lIdimagen.$lParametrosSID); ?>"> <?php print($lNumfoto); ?> </a>						</div> 
	<?php
			} //fin for ($lj=$_SESSION["lista_fotos_est_primera"];$lj<=$_SESSION["lista_fotos_est_ultima"];$lj++)...
	?>
						<div class="numero_foto">
							<a href="index.php<?php print($lParametros."&idimg_est=".$laListaFotosEst[$lNumImagenSiguiente].$lParametrosSID); ?>"> &gt; </a>
						</div>
	<?php
		} //fin if ($lNumFotosEst > 0)...
	?>
					</div>
	
					<div class="texto">
						<?php /*<h5> <?php print(ucfirst(mb_strtolower(fLiteral(359,$lIdIdioma)))); //DESCRIPCI�N ?> </h5>*/ ?>
						<h5> <?php print(fLiteral(393,$lIdIdioma)); //Descripci�n ?> </h5>
						<p>
							<?php print(nl2br($lEstDescripcion)); ?>
						</p>
						<h5> <?php print(ucfirst(mb_strtolower(fLiteral(6,$lIdIdioma)))); //DATOS DE CONTACTO ?> </h5>
							<!--<h6> <?php //print(strtoupper($lEstNombre)); ?> </h6>-->
						<p>
	<?php
		//Las variables que usamos para sacar los datos de contacto se establecen en /include/modulos/inc_cabecera.php
	?>
							<span id="nombre_hotel"><?php print(strtoupper($lEstNombre)); ?> </span> <br />
							<?php print($lEstDireccion); ?> <br />
							<?php print($lEstCP." ".$lEstMunicipio." - ".$lEstProvincia); ?> <br />
							<?php print($lEstTelefono); ?> <br />
							<a href="https://<?php print($lEstURL); ?>"><?php print($lEstURL); ?></a> <br />
							<!--<span class="negrita"><?php //print(fLiteral(360,$lIdIdioma)); //Responsable ?></span>: <?php //print($lEstResponsable); ?> <br />-->
							<span class="negrita"><?php print(fLiteral(361,$lIdIdioma)); //Persona de contacto ?></span>: <?php print($lEstContactoNombre); ?> <br />
							<span class="negrita"><?php print(fLiteral(362,$lIdIdioma)); //Tel�fono de contacto ?></span>: <?php print($lEstContactoTelefono); ?> <br />
						</p>
					</div>
				</div>
			</div>
			<?php include("include/modulos/inc_instalaciones_servicios.php"); ?>
			<?php include("include/modulos/inc_pie.php"); ?>
		</div>
	</div>
<?php
//print("index.php. \$_SESSION[\"lista_fotos_est_primera\"]=".$_SESSION["lista_fotos_est_primera"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_ultima\"]=".$_SESSION["lista_fotos_est_ultima"]."<br />");
//print("index.php. \$_SESSION[\"lista_fotos_est_actual\"]=".$_SESSION["lista_fotos_est_actual"]."<br />");
//print("index.php. \$lNumImagenAnterior=".$lNumImagenAnterior."<br />");
//print("index.php. \$lNumImagenSiguiente=".$lNumImagenSiguiente."<br />");
?>
<?php include("include/modulos/inc_google_analytics.php"); ?>
</body>
</html>
